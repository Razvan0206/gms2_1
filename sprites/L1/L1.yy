{
    "id": "8dd6f657-540d-48bb-8b71-62ba391a340a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "L1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 60,
    "bbox_left": 9,
    "bbox_right": 55,
    "bbox_top": 10,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d3b11fc4-e2f8-4ddc-bd61-6e78c4ea40db",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8dd6f657-540d-48bb-8b71-62ba391a340a",
            "compositeImage": {
                "id": "a0b0138d-9d7f-44a4-881a-0e309d141f37",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d3b11fc4-e2f8-4ddc-bd61-6e78c4ea40db",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2b8cce3e-b83c-41fb-9814-fecac8e3bb52",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d3b11fc4-e2f8-4ddc-bd61-6e78c4ea40db",
                    "LayerId": "4c3d3990-858d-49e9-b60c-fb502cc7acc8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "4c3d3990-858d-49e9-b60c-fb502cc7acc8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8dd6f657-540d-48bb-8b71-62ba391a340a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}