{
    "id": "942b73d4-0008-4c1a-a453-8a22ab7d283f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "L2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 60,
    "bbox_left": 9,
    "bbox_right": 55,
    "bbox_top": 10,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b0313077-a6ba-43d9-b176-e5886a9df1f5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "942b73d4-0008-4c1a-a453-8a22ab7d283f",
            "compositeImage": {
                "id": "027de70c-e673-403d-a9c5-cc4d5fffabf6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b0313077-a6ba-43d9-b176-e5886a9df1f5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9c2fe393-59d2-4d4d-9866-791b6200c8ef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b0313077-a6ba-43d9-b176-e5886a9df1f5",
                    "LayerId": "ede414f7-3164-40f1-b803-742d40a21ee8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "ede414f7-3164-40f1-b803-742d40a21ee8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "942b73d4-0008-4c1a-a453-8a22ab7d283f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}