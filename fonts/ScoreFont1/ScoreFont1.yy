{
    "id": "5cd595a0-a90f-42f7-80b5-53145f432995",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "ScoreFont1",
    "AntiAlias": 0,
    "TTFName": "",
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Lato",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "fe9f2c23-ed3d-4aac-9a18-1c2854b0f5ac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 320,
                "offset": 0,
                "shift": 52,
                "w": 52,
                "x": 612,
                "y": 1612
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "cad7f9b5-8b65-4542-b076-911a5863eadc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 320,
                "offset": 29,
                "shift": 92,
                "w": 34,
                "x": 970,
                "y": 1612
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "8232fb77-803d-49c5-969f-c62ddc18c386",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 320,
                "offset": 20,
                "shift": 106,
                "w": 66,
                "x": 240,
                "y": 1612
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "511df2a3-c1d2-4484-8d4e-5881c48d8580",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 320,
                "offset": 7,
                "shift": 155,
                "w": 141,
                "x": 294,
                "y": 646
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "ce527d17-7285-4227-8c6d-c07a8a81a159",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 320,
                "offset": 14,
                "shift": 155,
                "w": 128,
                "x": 132,
                "y": 968
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "13f87c9e-9d8f-421b-99f9-cdb6f31aef03",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 320,
                "offset": 10,
                "shift": 210,
                "w": 192,
                "x": 1080,
                "y": 2
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "c993048c-fe0e-4498-ba49-db9734ebaf80",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 320,
                "offset": 11,
                "shift": 188,
                "w": 177,
                "x": 2,
                "y": 324
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "3712520c-2dbc-410a-ab11-ddee320ea29a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 320,
                "offset": 20,
                "shift": 61,
                "w": 21,
                "x": 1095,
                "y": 1612
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "436ac1cd-a73d-4abb-86e4-63c81fc47bd3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 320,
                "offset": 18,
                "shift": 80,
                "w": 52,
                "x": 558,
                "y": 1612
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "a43e2c02-b2e6-4cd9-99de-111061770a32",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 320,
                "offset": 10,
                "shift": 80,
                "w": 52,
                "x": 666,
                "y": 1612
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "e0350679-2de5-46fb-943e-b66f68b21e76",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 320,
                "offset": 13,
                "shift": 107,
                "w": 82,
                "x": 87,
                "y": 1612
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "2e267197-aa6c-452e-aaed-5006b91da48a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 320,
                "offset": 13,
                "shift": 155,
                "w": 129,
                "x": 1516,
                "y": 646
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "5d4bb446-28e8-4a70-9cce-ee27379114a0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 320,
                "offset": 13,
                "shift": 57,
                "w": 33,
                "x": 1006,
                "y": 1612
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "b5925387-a30a-4338-bc9e-527e84845420",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 320,
                "offset": 13,
                "shift": 93,
                "w": 67,
                "x": 171,
                "y": 1612
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "62f0ca54-c788-4d48-9c4d-444996809a0a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 320,
                "offset": 12,
                "shift": 57,
                "w": 34,
                "x": 862,
                "y": 1612
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "e3a00036-f443-4a53-b38f-9cf62c57ed4f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 320,
                "offset": -2,
                "shift": 100,
                "w": 104,
                "x": 1235,
                "y": 1290
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "b81f1098-b657-42bb-be25-38677a59ec21",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 320,
                "offset": 8,
                "shift": 155,
                "w": 139,
                "x": 437,
                "y": 646
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "0a2184d9-6dc9-4ae2-90b2-da50caed0f8e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 320,
                "offset": 27,
                "shift": 155,
                "w": 115,
                "x": 2,
                "y": 1290
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "7d2066c8-9701-4af9-a867-5f10537307b7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 320,
                "offset": 14,
                "shift": 155,
                "w": 129,
                "x": 1909,
                "y": 646
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "f8ca73cd-e0ed-480e-98a7-956ce8b45ea8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 320,
                "offset": 14,
                "shift": 155,
                "w": 129,
                "x": 1778,
                "y": 646
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "10f8f0c8-984c-4d95-861c-db96af178fc6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 320,
                "offset": 5,
                "shift": 155,
                "w": 145,
                "x": 2,
                "y": 646
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "51857d84-9884-47ac-8c76-d68b80c25c75",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 320,
                "offset": 14,
                "shift": 155,
                "w": 123,
                "x": 772,
                "y": 968
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "0abacad8-b03a-4b8e-a00e-6650157020fe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 320,
                "offset": 14,
                "shift": 155,
                "w": 130,
                "x": 1120,
                "y": 646
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "91c0c497-4dca-408f-8388-71303099c023",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 320,
                "offset": 15,
                "shift": 155,
                "w": 131,
                "x": 987,
                "y": 646
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "f7c5d931-f8d0-4d98-a381-983625a3844c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 320,
                "offset": 13,
                "shift": 155,
                "w": 130,
                "x": 1252,
                "y": 646
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "290d5430-09b2-4c5b-b715-34ad751e98b4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 320,
                "offset": 20,
                "shift": 155,
                "w": 125,
                "x": 645,
                "y": 968
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "de7152a7-c6d2-453c-a4a3-809ba82b61b8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 320,
                "offset": 17,
                "shift": 67,
                "w": 34,
                "x": 898,
                "y": 1612
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "5dd18f26-222d-4259-9541-6f3a164d3a2b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 320,
                "offset": 17,
                "shift": 67,
                "w": 34,
                "x": 934,
                "y": 1612
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "9c3f063f-b9fc-4e41-a0ae-abcdccc0a7a8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 320,
                "offset": 20,
                "shift": 155,
                "w": 105,
                "x": 1021,
                "y": 1290
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "57cd6d98-421d-4f2b-a0f4-5031c7709964",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 320,
                "offset": 20,
                "shift": 155,
                "w": 115,
                "x": 1868,
                "y": 968
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "ea4f320c-2a30-496f-b653-9db130a48a2f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 320,
                "offset": 32,
                "shift": 155,
                "w": 104,
                "x": 1341,
                "y": 1290
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "5af97679-1080-4446-8a4f-5f7fcf562c0e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 320,
                "offset": 5,
                "shift": 106,
                "w": 98,
                "x": 1553,
                "y": 1290
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "6cdcfed4-7a8c-47cd-92e0-35ad16f51a5c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 320,
                "offset": 11,
                "shift": 219,
                "w": 198,
                "x": 880,
                "y": 2
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "55edebc9-b573-4e68-8937-db1af24ba0c3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 320,
                "offset": 1,
                "shift": 182,
                "w": 180,
                "x": 1833,
                "y": 2
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "012b5c23-9844-4332-9b98-c498e9a91669",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 320,
                "offset": 23,
                "shift": 173,
                "w": 136,
                "x": 578,
                "y": 646
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "091f4e4a-4173-41c1-a822-dbb5f0100cff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 320,
                "offset": 12,
                "shift": 183,
                "w": 161,
                "x": 860,
                "y": 324
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "addf9c85-d484-4cc0-b29b-f0622bd7bb11",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 320,
                "offset": 23,
                "shift": 201,
                "w": 167,
                "x": 522,
                "y": 324
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "db0515d9-aedf-449d-a9a5-a3315e4b3a72",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 320,
                "offset": 23,
                "shift": 155,
                "w": 119,
                "x": 1629,
                "y": 968
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "17a3fac3-ca66-4fee-80c1-ffdaa517cc3f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 320,
                "offset": 23,
                "shift": 151,
                "w": 119,
                "x": 1508,
                "y": 968
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "0dcb7fb5-ed10-4b36-a309-51ab478573bc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 320,
                "offset": 12,
                "shift": 196,
                "w": 168,
                "x": 352,
                "y": 324
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "d62b81b2-3136-479e-9491-16ed94f16e32",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 320,
                "offset": 23,
                "shift": 202,
                "w": 156,
                "x": 1181,
                "y": 324
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "cf6b4c83-c659-4e78-986a-dba604b5dc9f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 320,
                "offset": 28,
                "shift": 82,
                "w": 26,
                "x": 1041,
                "y": 1612
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "54acb343-8325-4806-8cdd-d80243acd4e8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 320,
                "offset": 8,
                "shift": 119,
                "w": 88,
                "x": 1843,
                "y": 1290
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "7cd66aa0-3ab8-4372-b3d9-6d4dbe3878a7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 320,
                "offset": 26,
                "shift": 182,
                "w": 154,
                "x": 1339,
                "y": 324
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "493f7db2-bce6-4516-8375-28471ba1fbaf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 320,
                "offset": 23,
                "shift": 137,
                "w": 109,
                "x": 693,
                "y": 1290
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "a4061ca4-a4be-48aa-8930-e154b60f6f42",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 320,
                "offset": 23,
                "shift": 246,
                "w": 200,
                "x": 478,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "71c24a68-b07b-44a1-92c8-3aa79a17666b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 320,
                "offset": 23,
                "shift": 202,
                "w": 156,
                "x": 1023,
                "y": 324
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "0110b1a8-ffc8-4823-83d1-a8a64157966c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 320,
                "offset": 12,
                "shift": 213,
                "w": 189,
                "x": 1274,
                "y": 2
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "28660c5f-2a40-4c2f-a91b-1ff99ac6a367",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 320,
                "offset": 26,
                "shift": 163,
                "w": 129,
                "x": 1647,
                "y": 646
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "cb63d4e1-7bbf-46ff-8173-576ce4e57b79",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 320,
                "offset": 12,
                "shift": 213,
                "w": 198,
                "x": 680,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "19c0613b-e1a5-41d5-8a23-34149908ab5d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 320,
                "offset": 26,
                "shift": 172,
                "w": 143,
                "x": 149,
                "y": 646
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "6d8e74b5-0ce5-4bb9-b95c-8968cf2932e0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 320,
                "offset": 8,
                "shift": 142,
                "w": 125,
                "x": 391,
                "y": 968
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "81815445-1821-45ce-8de6-b17c757c444c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 320,
                "offset": 4,
                "shift": 158,
                "w": 151,
                "x": 1650,
                "y": 324
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "6152beaf-47c2-4d7c-8662-9682b3f0b90c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 320,
                "offset": 21,
                "shift": 195,
                "w": 153,
                "x": 1495,
                "y": 324
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "cbac3ef8-173b-45d1-a5b0-4e7fb7776763",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 320,
                "offset": 1,
                "shift": 182,
                "w": 180,
                "x": 1651,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "09bcb332-6813-4178-ba14-286c4fd09e77",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 320,
                "offset": 2,
                "shift": 272,
                "w": 270,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "8b8344af-aae6-44b3-ba86-ae4148e6937b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 320,
                "offset": 2,
                "shift": 172,
                "w": 169,
                "x": 181,
                "y": 324
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "577b0ef4-8ba7-463b-81af-88f9c989b753",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 320,
                "offset": 1,
                "shift": 168,
                "w": 167,
                "x": 691,
                "y": 324
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "43ff86c3-635a-43f1-a833-e3fb7d1ab26e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 320,
                "offset": 11,
                "shift": 167,
                "w": 146,
                "x": 1803,
                "y": 324
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "55b049d7-1569-4968-b7da-cad99d10a7da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 320,
                "offset": 19,
                "shift": 80,
                "w": 51,
                "x": 720,
                "y": 1612
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "fabced0f-b970-4225-882c-1b9fe4d9809b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 320,
                "offset": -3,
                "shift": 100,
                "w": 104,
                "x": 1447,
                "y": 1290
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "602153c0-1d89-4874-847c-41b479144239",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 320,
                "offset": 12,
                "shift": 80,
                "w": 50,
                "x": 773,
                "y": 1612
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "87b160ce-d64a-4e23-b260-cd77bd204b47",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 320,
                "offset": 21,
                "shift": 155,
                "w": 112,
                "x": 466,
                "y": 1290
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "cb1051c6-f3de-4654-b8e9-3f643af30355",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 320,
                "offset": 0,
                "shift": 105,
                "w": 106,
                "x": 913,
                "y": 1290
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "48166c3e-cc7d-43d3-9b73-021e9e5e6370",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 320,
                "offset": 5,
                "shift": 82,
                "w": 54,
                "x": 502,
                "y": 1612
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "be67129b-1366-4c68-b145-8ab8c4db2a35",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 320,
                "offset": 12,
                "shift": 135,
                "w": 107,
                "x": 804,
                "y": 1290
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "d1a7ff4c-c4d5-4ceb-a607-3ba202c39ce0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 320,
                "offset": 20,
                "shift": 149,
                "w": 120,
                "x": 1020,
                "y": 968
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "0bac9885-1f3f-467a-9d1b-c2dcc3a0136c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 320,
                "offset": 10,
                "shift": 125,
                "w": 111,
                "x": 580,
                "y": 1290
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "72ed3dea-6272-464b-9362-6a9ea775292b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 320,
                "offset": 10,
                "shift": 149,
                "w": 120,
                "x": 1386,
                "y": 968
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "3c022534-91a3-42f6-a9c7-272bb0a7e832",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 320,
                "offset": 10,
                "shift": 140,
                "w": 121,
                "x": 897,
                "y": 968
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "0d0ecf2c-f9d5-4fa3-862b-797962b1d09d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 320,
                "offset": 3,
                "shift": 90,
                "w": 86,
                "x": 1933,
                "y": 1290
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "643759cd-62db-4c3c-8175-18c7091d823e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 320,
                "offset": 7,
                "shift": 136,
                "w": 127,
                "x": 262,
                "y": 968
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "0a64b409-4310-43ac-9689-5bb9ec1e007d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 320,
                "offset": 19,
                "shift": 148,
                "w": 114,
                "x": 119,
                "y": 1290
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "1a6dc843-5c83-4540-a4a3-cabc19a5194c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 320,
                "offset": 17,
                "shift": 68,
                "w": 35,
                "x": 825,
                "y": 1612
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "1d3f4341-e03f-405a-babd-4a24f8b6f430",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 320,
                "offset": -7,
                "shift": 68,
                "w": 60,
                "x": 440,
                "y": 1612
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "ceebdb6a-2561-433f-a04c-97824dad26a2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 320,
                "offset": 20,
                "shift": 140,
                "w": 116,
                "x": 1750,
                "y": 968
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "729dd5ee-8c22-498b-8d38-cabf332529f3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 320,
                "offset": 22,
                "shift": 68,
                "w": 24,
                "x": 1069,
                "y": 1612
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "c0190115-d8a3-49ce-a5e3-44cbe9a09e16",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 320,
                "offset": 19,
                "shift": 219,
                "w": 184,
                "x": 1465,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "aa34286b-8ed7-460d-bc02-3394b1ef92bb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 320,
                "offset": 19,
                "shift": 148,
                "w": 114,
                "x": 235,
                "y": 1290
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "63f3b612-ebc7-4551-b307-2bb27c11d12a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 320,
                "offset": 10,
                "shift": 148,
                "w": 130,
                "x": 1384,
                "y": 646
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "c3d2f9ee-c515-4806-a095-b371612fdee2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 320,
                "offset": 19,
                "shift": 147,
                "w": 120,
                "x": 1142,
                "y": 968
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "adce976c-cc28-4e5b-a8fd-1b6f2d535631",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 320,
                "offset": 10,
                "shift": 149,
                "w": 120,
                "x": 1264,
                "y": 968
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "ad579672-67a5-48f1-9e03-c3aff536a81d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 320,
                "offset": 19,
                "shift": 108,
                "w": 83,
                "x": 2,
                "y": 1612
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "5b544c60-1fb4-451f-b0b4-4d7d044bee98",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 320,
                "offset": 8,
                "shift": 116,
                "w": 97,
                "x": 1653,
                "y": 1290
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "d1d8a660-bb5f-467b-9765-5ec792bb5bcf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 320,
                "offset": 6,
                "shift": 100,
                "w": 89,
                "x": 1752,
                "y": 1290
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "a10b4dc4-3449-4e26-ba69-885e2c62f895",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 320,
                "offset": 16,
                "shift": 148,
                "w": 113,
                "x": 351,
                "y": 1290
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "489e2222-ab49-4f74-90d4-5dd86b23c321",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 320,
                "offset": 2,
                "shift": 137,
                "w": 133,
                "x": 852,
                "y": 646
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "1c79bf65-64e7-414b-ad4f-0f2e40d35ab0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 320,
                "offset": 2,
                "shift": 205,
                "w": 202,
                "x": 274,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "025eee98-f598-47c2-a532-776ab6a76753",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 320,
                "offset": 4,
                "shift": 135,
                "w": 128,
                "x": 2,
                "y": 968
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "571cb1d1-2b6f-4fb3-8954-e1fd40d29b67",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 320,
                "offset": 2,
                "shift": 137,
                "w": 134,
                "x": 716,
                "y": 646
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "5119d0de-6799-4444-a1bf-788db01b8d72",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 320,
                "offset": 9,
                "shift": 123,
                "w": 105,
                "x": 1128,
                "y": 1290
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "bf300064-07c1-4d1c-a4d1-7d7937d5ce33",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 320,
                "offset": 6,
                "shift": 80,
                "w": 64,
                "x": 308,
                "y": 1612
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "50343fc8-af8d-4241-ad38-13ad2dff2244",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 320,
                "offset": 31,
                "shift": 80,
                "w": 20,
                "x": 1118,
                "y": 1612
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "b6004021-98ba-4124-b579-1543c732850f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 320,
                "offset": 12,
                "shift": 80,
                "w": 64,
                "x": 374,
                "y": 1612
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "d99763ff-7fa5-4e87-a04d-4d6575f84a3e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 320,
                "offset": 15,
                "shift": 155,
                "w": 125,
                "x": 518,
                "y": 968
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        {
            "id": "81ca9cdf-79b5-4e27-9641-0750b97819dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -24,
            "first": 34,
            "second": 38
        },
        {
            "id": "0d3d3577-e71e-424c-9df5-e06838842a7b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -30,
            "first": 34,
            "second": 44
        },
        {
            "id": "583c07f9-0a05-45f2-bfa9-cfc6c5fec1c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -24,
            "first": 34,
            "second": 45
        },
        {
            "id": "0a575849-2840-45a5-b104-dbc630eadfce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -30,
            "first": 34,
            "second": 46
        },
        {
            "id": "0816513c-6ee1-4984-8bdc-d3a4ecad02ae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -24,
            "first": 34,
            "second": 47
        },
        {
            "id": "f673da22-4d03-464b-9784-91c0b137d6a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 34,
            "second": 64
        },
        {
            "id": "faf072f5-343b-4d2d-8f3e-1668e276f8e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -24,
            "first": 34,
            "second": 65
        },
        {
            "id": "edb70dfd-9bc4-42df-b7e0-520752794aba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 34,
            "second": 67
        },
        {
            "id": "52c1ecc4-cd4b-40f1-95ea-d5115224d548",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 34,
            "second": 71
        },
        {
            "id": "2121e5a0-5dc7-470d-b2fb-cba81f715e39",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 34,
            "second": 79
        },
        {
            "id": "afe16c27-1314-4c42-b66c-e78ae5f4aa87",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 34,
            "second": 81
        },
        {
            "id": "b5bcc94b-2d00-4967-95d5-f865a35603e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 6,
            "first": 34,
            "second": 86
        },
        {
            "id": "d221bf58-5bc6-4e33-8eec-2ab5bb31b163",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 6,
            "first": 34,
            "second": 87
        },
        {
            "id": "b56a978a-1552-4efe-b8ba-fd6058d74ee6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 4,
            "first": 34,
            "second": 89
        },
        {
            "id": "0851fb11-9c81-41a3-89cc-165b483bde6c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 6,
            "first": 34,
            "second": 92
        },
        {
            "id": "0c6c6b6b-c4fc-43ac-bd0e-bcc771813278",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 34,
            "second": 97
        },
        {
            "id": "2f06e3c4-2c7c-4ef2-9486-beb562890359",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 34,
            "second": 99
        },
        {
            "id": "137af9b5-ff21-4728-8ecb-fc7d7fda03c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 34,
            "second": 100
        },
        {
            "id": "34e28070-f977-4a75-b8ce-51c8e6d39cc8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 34,
            "second": 101
        },
        {
            "id": "5c6a30b2-0f0b-4ca5-a23c-ab0050cf35b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 34,
            "second": 111
        },
        {
            "id": "89f4229f-a01a-42f6-ba5a-f23737094d14",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 34,
            "second": 113
        },
        {
            "id": "dfd4a399-412f-4f96-9079-b3a763230c60",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -24,
            "first": 34,
            "second": 171
        },
        {
            "id": "30b77158-ce06-477f-adf8-99f717d96cad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -24,
            "first": 34,
            "second": 173
        },
        {
            "id": "bcd0b3e9-737a-49f0-a06d-ba8f81e8599c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -24,
            "first": 34,
            "second": 183
        },
        {
            "id": "c51a1856-37db-461e-9672-1d12671cea8f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -24,
            "first": 34,
            "second": 187
        },
        {
            "id": "203de949-86b4-41ca-9844-8688f136b634",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -24,
            "first": 34,
            "second": 192
        },
        {
            "id": "78862775-e84b-43ae-b018-31ef59e787f4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -24,
            "first": 34,
            "second": 193
        },
        {
            "id": "ec5eadac-a938-4c04-8caf-c3475fd1f5c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -24,
            "first": 34,
            "second": 194
        },
        {
            "id": "1db4a02c-e3dc-429a-bede-8b82f08c820a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -24,
            "first": 34,
            "second": 195
        },
        {
            "id": "cf0181bb-9cf0-4800-a200-e629624971eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -24,
            "first": 34,
            "second": 196
        },
        {
            "id": "60436b9c-8b48-4b01-bf7f-216440c92c55",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -24,
            "first": 34,
            "second": 197
        },
        {
            "id": "5904d060-b72b-415b-8c82-10c5e600c123",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -24,
            "first": 34,
            "second": 198
        },
        {
            "id": "1da0336b-e394-42c8-82ae-a24cde1af484",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 34,
            "second": 199
        },
        {
            "id": "39084567-4ab3-4e0b-b85b-280dc527d0e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 34,
            "second": 210
        },
        {
            "id": "508f38ca-b762-46d4-a792-1f9ffb91a91b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 34,
            "second": 211
        },
        {
            "id": "e6f73920-b3bd-4249-a16c-b81f1c25422d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 34,
            "second": 212
        },
        {
            "id": "eb3be676-ca75-4681-ab75-f9627ebd1c2d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 34,
            "second": 213
        },
        {
            "id": "5f1924d2-f9fd-4661-ad76-b0bcffb906c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 34,
            "second": 214
        },
        {
            "id": "4690bc87-9bb7-4429-b16b-a736b0398e4e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 34,
            "second": 216
        },
        {
            "id": "89902a2d-e6e9-445a-ab9d-17ed76025425",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 4,
            "first": 34,
            "second": 221
        },
        {
            "id": "672e4ad9-abc3-461a-afcd-e1ec8ec289c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 34,
            "second": 224
        },
        {
            "id": "6c38933b-879d-4631-83b8-69d7654ce7d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 34,
            "second": 225
        },
        {
            "id": "6755705d-e2f7-4f3b-9b42-c1d86609ba6f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 34,
            "second": 226
        },
        {
            "id": "973ccb38-457b-4591-85e2-a3ca34ff7d12",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 34,
            "second": 227
        },
        {
            "id": "9532aa88-9fdc-4103-9524-26db44c459da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 34,
            "second": 228
        },
        {
            "id": "94efd6e9-d3df-46ce-a74d-2ef83af13e5a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 34,
            "second": 229
        },
        {
            "id": "10f984d0-ac81-485f-9769-62e5937416ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 34,
            "second": 230
        },
        {
            "id": "2e599367-0239-4a2a-b384-ceedc18cde46",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 34,
            "second": 231
        },
        {
            "id": "6e7be34d-5e8c-4286-bf69-51a818b664d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 34,
            "second": 232
        },
        {
            "id": "167c5ee7-442e-4b0d-aff5-06b7ee6c1e31",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 34,
            "second": 233
        },
        {
            "id": "81b985e5-82ea-41ec-b60d-52fc5ab1b10e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 34,
            "second": 234
        },
        {
            "id": "6e53c403-f4e1-49eb-a15e-0ded3e92729b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 34,
            "second": 235
        },
        {
            "id": "b83f2777-aea3-44c5-a2c1-e65799009219",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 34,
            "second": 240
        },
        {
            "id": "539e1d3e-e293-4acf-b76f-b94903501931",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 34,
            "second": 242
        },
        {
            "id": "39c985f3-9837-4bf8-b5a2-c57a9320c249",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 34,
            "second": 243
        },
        {
            "id": "3123bcaf-4b8e-4644-8cf1-67c1cfc4cdc1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 34,
            "second": 244
        },
        {
            "id": "0b16fee2-cfa6-4015-b971-b440e8f17a28",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 34,
            "second": 245
        },
        {
            "id": "ba5648d5-ad67-46d6-9845-b122189d0d9d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 34,
            "second": 246
        },
        {
            "id": "137dd7ee-6b7e-4f61-8711-54b935c22f45",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 34,
            "second": 248
        },
        {
            "id": "e1487120-9413-4ca6-8c23-0e14d578aea4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -24,
            "first": 34,
            "second": 260
        },
        {
            "id": "b689f394-af26-46f4-81cc-a98bb517f3f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 34,
            "second": 261
        },
        {
            "id": "fe52d424-7eaf-40c4-82c6-dfbbf88ec4f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 34,
            "second": 262
        },
        {
            "id": "bed5ad15-6cd2-4902-a872-fa2823993d95",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 34,
            "second": 263
        },
        {
            "id": "b6cdd810-5c41-489a-9247-21bf777f34f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 34,
            "second": 281
        },
        {
            "id": "6b0753e2-cde5-4baa-b53a-7771649ae7ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 34,
            "second": 338
        },
        {
            "id": "2b51a4c5-6cfa-41e8-9bdf-8a46ad4c4404",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 34,
            "second": 339
        },
        {
            "id": "701b0c61-c257-4675-b1ce-f83dc83627cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 4,
            "first": 34,
            "second": 376
        },
        {
            "id": "eaabbd87-cb25-43bb-80f5-d960d4063ecb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -24,
            "first": 34,
            "second": 8211
        },
        {
            "id": "d8749c96-efe6-4968-9e4f-f3ea6a5fce2d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -24,
            "first": 34,
            "second": 8212
        },
        {
            "id": "401842aa-cf13-4b0a-842c-b187225f36af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -30,
            "first": 34,
            "second": 8218
        },
        {
            "id": "0e73b34e-3501-489f-aa20-d382683844e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -30,
            "first": 34,
            "second": 8222
        },
        {
            "id": "ecf7aff5-10ea-400b-b74d-3a8dd77730c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -24,
            "first": 34,
            "second": 8226
        },
        {
            "id": "5f6425c4-fc4d-4d0a-8829-294c9b948ca7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -24,
            "first": 34,
            "second": 8249
        },
        {
            "id": "2a30fcf3-2945-4e65-9a78-6e1485da0d08",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -24,
            "first": 34,
            "second": 8250
        },
        {
            "id": "79346d71-fd6c-4e12-a6c7-68c55ddb3b8f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -24,
            "first": 34,
            "second": 8710
        },
        {
            "id": "4917575e-84b9-4f07-bf0f-c859376d7e43",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -24,
            "first": 39,
            "second": 38
        },
        {
            "id": "01d4f3ef-c259-40f2-8e26-c3f06e252f9a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -30,
            "first": 39,
            "second": 44
        },
        {
            "id": "2e28fc24-e1f8-4606-bd20-a58277d1919d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -24,
            "first": 39,
            "second": 45
        },
        {
            "id": "f2d0abbc-c1af-417a-9163-f5f929b78fc3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -30,
            "first": 39,
            "second": 46
        },
        {
            "id": "bf1f9dd2-643c-4325-850a-22e9a1b81b50",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -24,
            "first": 39,
            "second": 47
        },
        {
            "id": "02bc29bb-6743-4821-8140-91303442ae5b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 39,
            "second": 64
        },
        {
            "id": "8f5d7e00-470f-4994-9caf-5b266930fbfd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -24,
            "first": 39,
            "second": 65
        },
        {
            "id": "fd813f85-05ba-4300-ba0f-8c3cefa9eba2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 39,
            "second": 67
        },
        {
            "id": "8aea1091-1d65-44c9-8292-96199e9cd87d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 39,
            "second": 71
        },
        {
            "id": "b7dea2e0-7441-479b-85f9-3cb4ddc01fb2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 39,
            "second": 79
        },
        {
            "id": "cc417a6a-129f-4a80-82d1-c2e7e304874a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 39,
            "second": 81
        },
        {
            "id": "4175459a-38a9-4f05-b17a-1107c0fb572c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 6,
            "first": 39,
            "second": 86
        },
        {
            "id": "fab83485-1122-46b3-b2b9-c5b26fd0bba9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 6,
            "first": 39,
            "second": 87
        },
        {
            "id": "22491e4f-516b-4ebe-99f6-cccc3ca56fd4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 4,
            "first": 39,
            "second": 89
        },
        {
            "id": "c99e054f-e221-4054-a356-2ac58398b9b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 6,
            "first": 39,
            "second": 92
        },
        {
            "id": "200023e5-5601-4736-a660-4c84334dc74e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 39,
            "second": 97
        },
        {
            "id": "e194cb54-7151-4371-a9c9-de27795aadde",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 39,
            "second": 99
        },
        {
            "id": "4735b971-dfe4-4392-a9ec-369084f6a0ed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 39,
            "second": 100
        },
        {
            "id": "09e29745-0eee-45a1-a25f-7a86866f0f49",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 39,
            "second": 101
        },
        {
            "id": "08f5f417-3f2e-428c-b647-61afa6bbf803",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 39,
            "second": 111
        },
        {
            "id": "37108856-8c1a-4270-9915-3af315170e4a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 39,
            "second": 113
        },
        {
            "id": "f6dac87f-c323-4aa5-82ff-1af9378bf8b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -24,
            "first": 39,
            "second": 171
        },
        {
            "id": "80772044-bd41-4bcf-8a75-efdb2431a116",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -24,
            "first": 39,
            "second": 173
        },
        {
            "id": "9668c316-9418-4cb5-bd2e-a97977ad28b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -24,
            "first": 39,
            "second": 183
        },
        {
            "id": "64099d7e-7afb-4493-b52d-56af88c137be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -24,
            "first": 39,
            "second": 187
        },
        {
            "id": "6b3ad507-748c-4a28-8358-7b7fc25d0640",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -24,
            "first": 39,
            "second": 192
        },
        {
            "id": "0f696c2c-46ae-4076-9741-6c0c315a670a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -24,
            "first": 39,
            "second": 193
        },
        {
            "id": "d161481c-79ab-48d0-9bdc-5770c84bfdff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -24,
            "first": 39,
            "second": 194
        },
        {
            "id": "89eea11f-46f7-4d1f-8a03-99dcd01ca752",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -24,
            "first": 39,
            "second": 195
        },
        {
            "id": "5cea2600-72d9-4d6d-baef-81b47fd9ef8d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -24,
            "first": 39,
            "second": 196
        },
        {
            "id": "c0b79a81-18c6-4858-9ba5-e4d8c74b4456",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -24,
            "first": 39,
            "second": 197
        },
        {
            "id": "dd249a23-be2c-4dd0-b0e1-b3f7953b7980",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -24,
            "first": 39,
            "second": 198
        },
        {
            "id": "2d04ba13-09d6-4f55-83a7-0a5687f128c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 39,
            "second": 199
        },
        {
            "id": "99e1b770-f721-4caa-8c48-75c4eef205b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 39,
            "second": 210
        },
        {
            "id": "09142c3a-b5ed-4ac6-8257-41ae7e9fc4a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 39,
            "second": 211
        },
        {
            "id": "d4fdb39d-c213-4324-9e3b-4f6a36477436",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 39,
            "second": 212
        },
        {
            "id": "b38529e0-94e6-45df-83f0-d45b2ea4f07b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 39,
            "second": 213
        },
        {
            "id": "336401a1-f4b4-412d-9d59-9b1441084447",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 39,
            "second": 214
        },
        {
            "id": "928dbdd4-020e-4f5d-9953-1d308c95137a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 39,
            "second": 216
        },
        {
            "id": "38d66547-0366-41eb-a042-e5d28808186a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 4,
            "first": 39,
            "second": 221
        },
        {
            "id": "20e03981-1643-427e-818a-312fc3ab2a81",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 39,
            "second": 224
        },
        {
            "id": "efa5916b-9568-40d3-84e4-283270b8ad35",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 39,
            "second": 225
        },
        {
            "id": "3db58311-1a04-4b1f-a7d1-4b1d1f0be2f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 39,
            "second": 226
        },
        {
            "id": "3f618915-fe16-4e5b-b135-98838ce9e269",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 39,
            "second": 227
        },
        {
            "id": "9c8e9f42-c9bc-4131-8954-f661f5561f19",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 39,
            "second": 228
        },
        {
            "id": "aa22f3f4-ccbd-40d2-b5d5-322695af1991",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 39,
            "second": 229
        },
        {
            "id": "e18adbc5-4002-4808-9a35-a7b2bf030b30",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 39,
            "second": 230
        },
        {
            "id": "2792053a-5e7e-42a9-b62f-fd29df701785",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 39,
            "second": 231
        },
        {
            "id": "a0380f88-45f5-4542-92a2-fadb0110e663",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 39,
            "second": 232
        },
        {
            "id": "0269b30f-5dd6-456b-9ccc-daecd91f4d3f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 39,
            "second": 233
        },
        {
            "id": "3ff299cc-e292-4826-bc27-f3e75c18b2eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 39,
            "second": 234
        },
        {
            "id": "888b28dc-9b63-4d76-ade4-bc54a45ac6e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 39,
            "second": 235
        },
        {
            "id": "b81b59ea-d1da-43ea-83dd-ab3aa6a0b25c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 39,
            "second": 240
        },
        {
            "id": "8acd6121-3f27-4ae0-be5b-f27c5ad1fdfb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 39,
            "second": 242
        },
        {
            "id": "7506ea73-6658-4b34-9a45-7ac5d2fe2348",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 39,
            "second": 243
        },
        {
            "id": "a345febc-bac1-4e96-be92-62733afd4e04",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 39,
            "second": 244
        },
        {
            "id": "4ea3166e-a96c-48cd-977c-0df631a948ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 39,
            "second": 245
        },
        {
            "id": "07926447-0b62-4960-98ce-bd814ec09740",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 39,
            "second": 246
        },
        {
            "id": "717d2f81-c8ea-49d8-8cb2-5664f61fbc2a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 39,
            "second": 248
        },
        {
            "id": "d2db4dd3-a8cb-416e-824d-01b873fe24a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -24,
            "first": 39,
            "second": 260
        },
        {
            "id": "5b70970a-7ee4-4431-80fe-63742c1923db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 39,
            "second": 261
        },
        {
            "id": "94931826-f714-42d7-8c24-1af5cadebbd0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 39,
            "second": 262
        },
        {
            "id": "2f075ea3-288a-4390-82d4-24aaabbc3092",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 39,
            "second": 263
        },
        {
            "id": "0cca6706-8e4d-412f-a67a-c043a3d390d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 39,
            "second": 281
        },
        {
            "id": "c20bf07c-30b4-4b4f-8615-b64b75fd69bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 39,
            "second": 338
        },
        {
            "id": "e6c3c738-6526-4e48-8078-8c30a95baec3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 39,
            "second": 339
        },
        {
            "id": "dfd34f1f-6363-40e5-aa45-1cb4708dbae1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 4,
            "first": 39,
            "second": 376
        },
        {
            "id": "f7670a91-9059-4159-a991-99c4e18e5f7c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -24,
            "first": 39,
            "second": 8211
        },
        {
            "id": "b9f5b2a1-68ca-4233-ba67-e20d00ee21b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -24,
            "first": 39,
            "second": 8212
        },
        {
            "id": "f196af08-e5ae-42f5-94b5-7612994cefe1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -30,
            "first": 39,
            "second": 8218
        },
        {
            "id": "d04bf51f-d5ec-41c2-87fa-e6bb252ff948",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -30,
            "first": 39,
            "second": 8222
        },
        {
            "id": "1a862757-d71f-40a2-82de-49fef4c44336",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -24,
            "first": 39,
            "second": 8226
        },
        {
            "id": "f46d0c69-7942-4e53-94f4-9a4594e6a64a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -24,
            "first": 39,
            "second": 8249
        },
        {
            "id": "d32a6805-32c6-48ca-9b88-501985536a2d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -24,
            "first": 39,
            "second": 8250
        },
        {
            "id": "59897f50-a814-45fe-abb8-f4417d3a4206",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -24,
            "first": 39,
            "second": 8710
        },
        {
            "id": "ad9f5e70-df34-4c1b-90af-3304b74a117e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 40,
            "second": 64
        },
        {
            "id": "1fffddaa-dce7-4de2-b708-93415551c0ed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 40,
            "second": 67
        },
        {
            "id": "f4a96b91-870c-4be1-be5b-a954a358ea48",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 40,
            "second": 71
        },
        {
            "id": "9caad313-0c6c-47b9-88ce-961c317bac1b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 40,
            "second": 79
        },
        {
            "id": "54ac9eb5-9fe0-4fac-9d20-f04c6419278c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 40,
            "second": 81
        },
        {
            "id": "43e655b5-6cf7-4352-b85a-cd478d746233",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 40,
            "second": 99
        },
        {
            "id": "124e38d8-2364-40e8-aee8-3bac482e3c16",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 40,
            "second": 100
        },
        {
            "id": "6b09b5c8-059f-4003-b545-904a71799928",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 40,
            "second": 101
        },
        {
            "id": "bd9ff727-59b1-4221-b32e-68cd440a400f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 40,
            "second": 111
        },
        {
            "id": "39ce386a-63f7-42ec-b2d2-3012c4c102a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 40,
            "second": 113
        },
        {
            "id": "e177c574-1b44-45cb-8a38-f75990b20a6e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 40,
            "second": 199
        },
        {
            "id": "065677e7-b3bc-43de-94b9-7181322be2af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 40,
            "second": 210
        },
        {
            "id": "8898da9c-b35e-49e7-9c3c-e2f7befd9f67",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 40,
            "second": 211
        },
        {
            "id": "de554282-642f-4ffc-9ac3-d37d08678573",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 40,
            "second": 212
        },
        {
            "id": "4a0523e6-3e18-413b-ab96-9ae85c6f20c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 40,
            "second": 213
        },
        {
            "id": "d99742bd-d52f-4860-a8df-e038e415e8a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 40,
            "second": 214
        },
        {
            "id": "13304e32-c3f5-4b23-a02f-901f33fc5b0c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 40,
            "second": 216
        },
        {
            "id": "f251b610-c373-447d-940b-1a33e44ef000",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 40,
            "second": 231
        },
        {
            "id": "270fbbe6-9e75-4877-89ee-d763496a5230",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 40,
            "second": 232
        },
        {
            "id": "d00e6c23-223c-4886-bf35-b8777e1d061f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 40,
            "second": 233
        },
        {
            "id": "b20c7741-2a43-44e8-8542-20ab092c2387",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 40,
            "second": 234
        },
        {
            "id": "b39da165-2f34-4737-a118-64b981040fb1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 40,
            "second": 235
        },
        {
            "id": "7ecdd99d-d736-43f6-a8da-54087f8dfb55",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 40,
            "second": 240
        },
        {
            "id": "13559016-e873-4a10-a0a6-2b1ddf232113",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 40,
            "second": 242
        },
        {
            "id": "5c46576a-0c08-470a-8e4e-e89f954c3df2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 40,
            "second": 243
        },
        {
            "id": "a96d09d5-9510-46ea-9d50-dddb788fc03f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 40,
            "second": 244
        },
        {
            "id": "d61fe505-3f63-42fb-98ac-91d6325efca3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 40,
            "second": 245
        },
        {
            "id": "9b1c07b2-8342-4076-b4d8-6ef1a47988dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 40,
            "second": 246
        },
        {
            "id": "492abd38-0cdd-4e24-91d1-3632a11cb34e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 40,
            "second": 248
        },
        {
            "id": "e15807ea-c3ee-482d-b1c2-3c477d54c71e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 40,
            "second": 262
        },
        {
            "id": "527266cd-3779-4882-8482-9ac77f31a971",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 40,
            "second": 263
        },
        {
            "id": "a2020dde-cd2d-4831-a501-257ead268c77",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 40,
            "second": 281
        },
        {
            "id": "b368a899-aad9-4dba-930f-a7f23d8f6ef9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 40,
            "second": 338
        },
        {
            "id": "19533ea0-6e4c-432f-9785-c7744e0a8585",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 40,
            "second": 339
        },
        {
            "id": "ce07a3cb-e511-4959-a217-d81023510737",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -24,
            "first": 42,
            "second": 38
        },
        {
            "id": "1cb977fb-3f4f-4984-aa64-26a8e37ae76f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -30,
            "first": 42,
            "second": 44
        },
        {
            "id": "2a6184ea-67b4-4c98-873d-83b80767cd82",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -24,
            "first": 42,
            "second": 45
        },
        {
            "id": "3711844d-70b4-4c21-a3dd-2fc94b060b9c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -30,
            "first": 42,
            "second": 46
        },
        {
            "id": "7c26430c-a8dd-4fd5-ba7c-bfd81555c7e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -24,
            "first": 42,
            "second": 47
        },
        {
            "id": "b5fe6b0c-5fd2-4a2a-a5ae-9e453d02c081",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 42,
            "second": 64
        },
        {
            "id": "cf8e4729-4529-424e-b7d0-8bbdc366bb99",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -24,
            "first": 42,
            "second": 65
        },
        {
            "id": "10772853-8ae3-4ba3-8de4-02f88e9194c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 42,
            "second": 67
        },
        {
            "id": "8e84b3e1-c66a-4d74-a67d-e0bedc6b7478",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 42,
            "second": 71
        },
        {
            "id": "16866c69-cb3a-449e-b0eb-34aa6cb567d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 42,
            "second": 79
        },
        {
            "id": "f9b1f668-6da4-4dee-b649-7b3fa125aa92",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 42,
            "second": 81
        },
        {
            "id": "a2744a1f-3e56-47e7-960b-779818f10d7b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 6,
            "first": 42,
            "second": 86
        },
        {
            "id": "ae98fd5e-536b-4603-829c-98b8edc8ad3a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 6,
            "first": 42,
            "second": 87
        },
        {
            "id": "3c622d37-7465-4718-bd78-641fddb54a3d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 4,
            "first": 42,
            "second": 89
        },
        {
            "id": "a25ea0db-f2f2-41bb-8430-a3d4ae517138",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 6,
            "first": 42,
            "second": 92
        },
        {
            "id": "eddcd62c-920f-42b3-a893-1b622a4db6d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 42,
            "second": 97
        },
        {
            "id": "e90abe8c-2ff1-4ed5-aa8f-62607ee00ff5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 42,
            "second": 99
        },
        {
            "id": "fc1a5457-968b-4095-a033-04fb466c098d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 42,
            "second": 100
        },
        {
            "id": "687feaeb-c77d-42e1-9355-702e77d96a6b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 42,
            "second": 101
        },
        {
            "id": "42382753-bfdb-4f60-99de-5416c098efac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 42,
            "second": 111
        },
        {
            "id": "6112a6bf-4f08-411c-b76c-6e93056d5dda",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 42,
            "second": 113
        },
        {
            "id": "66600f76-491a-4677-9bda-1c936b207de5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -24,
            "first": 42,
            "second": 171
        },
        {
            "id": "0d95ac5f-e6ef-4d90-adef-1ffc73570662",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -24,
            "first": 42,
            "second": 173
        },
        {
            "id": "c33a407b-dbb6-48d4-aff0-50ed07ca95da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -24,
            "first": 42,
            "second": 183
        },
        {
            "id": "edeb05af-9cfe-4891-bb1c-f84ae9e2c7e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -24,
            "first": 42,
            "second": 187
        },
        {
            "id": "48bdecd5-f1e8-4e07-8ddf-98346312dbc9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -24,
            "first": 42,
            "second": 192
        },
        {
            "id": "1d7402d0-cec0-48e3-bc96-d0ef8e1b51e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -24,
            "first": 42,
            "second": 193
        },
        {
            "id": "47f3f3b0-7c05-46f2-b36c-e8fa50a95aff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -24,
            "first": 42,
            "second": 194
        },
        {
            "id": "69c199fa-a214-446f-9aad-63868df0f409",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -24,
            "first": 42,
            "second": 195
        },
        {
            "id": "aea11171-1751-4a7c-83ab-e48ffb7dfafe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -24,
            "first": 42,
            "second": 196
        },
        {
            "id": "791d2802-6dc5-4fcb-8a36-72296a742830",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -24,
            "first": 42,
            "second": 197
        },
        {
            "id": "4dc4454d-4658-45a7-9cce-7828a50c9345",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -24,
            "first": 42,
            "second": 198
        },
        {
            "id": "39789e72-7f38-4dae-ae63-c28f36128b69",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 42,
            "second": 199
        },
        {
            "id": "29c2cb8c-d5c6-4297-a4e3-55a6ed6f9c60",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 42,
            "second": 210
        },
        {
            "id": "c1fc86d6-38af-4e4d-9d54-dc78c17b6b97",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 42,
            "second": 211
        },
        {
            "id": "fc7cecbf-d60a-4d94-ac16-f186ebc74382",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 42,
            "second": 212
        },
        {
            "id": "d68f984e-7f8a-4899-94fe-792b3a672439",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 42,
            "second": 213
        },
        {
            "id": "63ca211a-f030-4816-80ef-78c8980e3952",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 42,
            "second": 214
        },
        {
            "id": "301e0cbd-6075-481c-8762-be8d631af7ae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 42,
            "second": 216
        },
        {
            "id": "9cb2944e-b81b-4477-800f-f32e838c8d68",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 4,
            "first": 42,
            "second": 221
        },
        {
            "id": "e66385c0-42e7-4048-b9c8-5e904c1fcc6d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 42,
            "second": 224
        },
        {
            "id": "39862f9a-622f-44e1-a3cf-710efaf629a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 42,
            "second": 225
        },
        {
            "id": "53261ef1-6699-4dac-a7b1-ff934abd84d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 42,
            "second": 226
        },
        {
            "id": "bcbccedd-28d2-4ccf-995d-e5db66dfa091",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 42,
            "second": 227
        },
        {
            "id": "127bbd0f-7c2b-439a-a855-142b7f4e02fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 42,
            "second": 228
        },
        {
            "id": "7b61d38e-52bc-469c-87fc-52a26a70be58",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 42,
            "second": 229
        },
        {
            "id": "74a55a92-1ac7-48b0-84a3-93c4adc2b234",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 42,
            "second": 230
        },
        {
            "id": "14453c1e-7b01-4d29-8019-9f5457aa528c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 42,
            "second": 231
        },
        {
            "id": "62c8aa7a-ffef-4520-bb64-9d0e5d81b85c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 42,
            "second": 232
        },
        {
            "id": "e97a481d-d61f-4369-90ad-94bdbaf3ac50",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 42,
            "second": 233
        },
        {
            "id": "e9dd7c47-3074-4a33-88a5-5ab09d3ce377",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 42,
            "second": 234
        },
        {
            "id": "c011e6a4-92e4-4ce2-b0e6-986e71c86856",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 42,
            "second": 235
        },
        {
            "id": "658b85be-0fbc-462a-abd0-8fd9ef1e2f45",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 42,
            "second": 240
        },
        {
            "id": "5fdfb344-cd8e-4f17-b747-e24f64dcd4f6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 42,
            "second": 242
        },
        {
            "id": "87eec2bf-6206-4117-a7d4-2419eebd6eaf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 42,
            "second": 243
        },
        {
            "id": "6d116fff-0e0e-4e8a-8498-379483a220f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 42,
            "second": 244
        },
        {
            "id": "7fa35a1b-32a0-4007-9de4-573c63c735b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 42,
            "second": 245
        },
        {
            "id": "15ed772a-0615-4be6-bdc7-54e3cddb9f47",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 42,
            "second": 246
        },
        {
            "id": "e1614da3-2de3-4c67-9249-de341acf82f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 42,
            "second": 248
        },
        {
            "id": "7c1acf02-b2c3-4bcf-ac1f-9b50a0f6880c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -24,
            "first": 42,
            "second": 260
        },
        {
            "id": "7bc2574d-e90a-470a-a890-000c2565b54c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 42,
            "second": 261
        },
        {
            "id": "762a1148-a849-4304-bf00-b6b491c853b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 42,
            "second": 262
        },
        {
            "id": "e17d473e-23f4-4dd0-83b9-4bfa540de575",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 42,
            "second": 263
        },
        {
            "id": "10f6fdbc-7dd9-4109-aea6-300d53046f97",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 42,
            "second": 281
        },
        {
            "id": "dc764e28-a3bc-4fc1-8d8a-e12cdd528562",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 42,
            "second": 338
        },
        {
            "id": "c60e1303-3946-4d61-8d7b-7e7c7519882e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 42,
            "second": 339
        },
        {
            "id": "294a9b21-73a4-43f9-9e42-cd8f8e6db085",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 4,
            "first": 42,
            "second": 376
        },
        {
            "id": "e037c272-1871-4a02-8cce-1d2a78f62aa2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -24,
            "first": 42,
            "second": 8211
        },
        {
            "id": "1c5cdd25-01d3-4437-ae0e-6b182ace11ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -24,
            "first": 42,
            "second": 8212
        },
        {
            "id": "ce7310ee-5881-4353-9601-2c8076174f2f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -30,
            "first": 42,
            "second": 8218
        },
        {
            "id": "989ce4e1-1bb6-495d-8182-e38d9b73bff8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -30,
            "first": 42,
            "second": 8222
        },
        {
            "id": "0ec409b7-57c1-4231-8cda-ca1566499c2b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -24,
            "first": 42,
            "second": 8226
        },
        {
            "id": "e1d09a67-7440-49d7-8b55-b9003719338b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -24,
            "first": 42,
            "second": 8249
        },
        {
            "id": "55a6ea0a-1b5e-41ce-ad71-0d00eb4f2c2c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -24,
            "first": 42,
            "second": 8250
        },
        {
            "id": "a1a7890c-6e0c-4aff-a417-2f5b122a1738",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -24,
            "first": 42,
            "second": 8710
        },
        {
            "id": "b548af8f-66ff-4bd8-a5ce-64d5187df060",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -30,
            "first": 44,
            "second": 34
        },
        {
            "id": "990c80ed-3597-4134-8f96-7bbe82c0ff71",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -30,
            "first": 44,
            "second": 39
        },
        {
            "id": "943df272-c4bb-4c51-b16b-5d36270d2a4a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -30,
            "first": 44,
            "second": 42
        },
        {
            "id": "edbbb3c0-190b-4501-893b-b4c8b14bef06",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -18,
            "first": 44,
            "second": 45
        },
        {
            "id": "40ce8f38-b5a1-4dc6-9ec4-2d56e8ace9ed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 44,
            "second": 64
        },
        {
            "id": "cc786d72-664a-45b9-a1d5-b982a107c202",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 44,
            "second": 67
        },
        {
            "id": "510a45a8-51a2-4690-b50f-ce478dea601a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 44,
            "second": 71
        },
        {
            "id": "2718694e-3b53-4789-b4b3-53164f14fd06",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 44,
            "second": 79
        },
        {
            "id": "76562b4b-4ddd-4b77-b0a2-8ee49ac8abc7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 44,
            "second": 81
        },
        {
            "id": "677bb3b1-afaa-4824-bdc3-31acb492f628",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -24,
            "first": 44,
            "second": 84
        },
        {
            "id": "635ee756-8749-4f9e-96d1-5bf66230cedb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -24,
            "first": 44,
            "second": 86
        },
        {
            "id": "871250e2-fdb4-4f19-ab40-97c4ef66af12",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -16,
            "first": 44,
            "second": 87
        },
        {
            "id": "c683e0c6-a277-4ca8-84e8-f4fb5c96d779",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -20,
            "first": 44,
            "second": 89
        },
        {
            "id": "8bb86caf-621d-429e-8502-f5c6a484b8d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -24,
            "first": 44,
            "second": 92
        },
        {
            "id": "e062d404-e033-4819-a1d4-1185d558b39e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -18,
            "first": 44,
            "second": 118
        },
        {
            "id": "85d1b653-7333-437c-a3cd-ad3b35c9c5b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 44,
            "second": 119
        },
        {
            "id": "41ed74ba-68d1-4db3-9b74-4e8c05bb2608",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -18,
            "first": 44,
            "second": 121
        },
        {
            "id": "17f99b45-03b9-4b89-aad8-771a16691874",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -30,
            "first": 44,
            "second": 170
        },
        {
            "id": "4b8099ad-b3e7-449d-8848-68cd99173824",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -18,
            "first": 44,
            "second": 171
        },
        {
            "id": "a52e94f5-d5b3-4034-9fa1-2685903a0637",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -18,
            "first": 44,
            "second": 173
        },
        {
            "id": "97294e86-832b-4e2a-9032-45127c6be0db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -30,
            "first": 44,
            "second": 176
        },
        {
            "id": "075c4efd-b953-48bc-8990-ffb6111d7720",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -18,
            "first": 44,
            "second": 183
        },
        {
            "id": "bb734c5f-366b-4717-9338-d61337a34a65",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -30,
            "first": 44,
            "second": 186
        },
        {
            "id": "525ddc04-c202-4b1f-8733-4addbe027d00",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -18,
            "first": 44,
            "second": 187
        },
        {
            "id": "20ba6aca-2daf-4350-8531-2616f5986894",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 44,
            "second": 199
        },
        {
            "id": "30e6bc0a-0cdd-47d5-a572-35a5e93ec28f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 44,
            "second": 210
        },
        {
            "id": "53f68878-0989-42d7-b2ab-a6657e0d1787",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 44,
            "second": 211
        },
        {
            "id": "260cab67-aabe-48ee-bf80-76508da792cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 44,
            "second": 212
        },
        {
            "id": "a67d0de1-0b8a-4428-94a7-63fff8f7327f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 44,
            "second": 213
        },
        {
            "id": "37b51287-e570-4594-aaeb-7dffa5ff2a06",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 44,
            "second": 214
        },
        {
            "id": "ea2c3098-8a07-46d6-94df-3bd2178eb022",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 44,
            "second": 216
        },
        {
            "id": "c6c9be24-8fc2-4d2a-87cf-f7eb0b30dd4e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -20,
            "first": 44,
            "second": 221
        },
        {
            "id": "3b96db5a-91f7-47a1-a90f-7246d6fe76a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 44,
            "second": 262
        },
        {
            "id": "33e6b65f-1c6c-4f4d-be1a-57c45b1b6472",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 44,
            "second": 338
        },
        {
            "id": "f0b83bed-0cf9-48d6-8397-c76f17af6685",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -20,
            "first": 44,
            "second": 376
        },
        {
            "id": "8fcefa7c-6f19-42a3-b2c9-e05581bc142e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -18,
            "first": 44,
            "second": 8211
        },
        {
            "id": "642b4b2b-89b7-4d4a-9688-73af71100965",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -18,
            "first": 44,
            "second": 8212
        },
        {
            "id": "32336210-7643-435e-9b47-72c542df7715",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -30,
            "first": 44,
            "second": 8216
        },
        {
            "id": "345d4a14-e726-405e-9ae3-bd17e2ca1528",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -30,
            "first": 44,
            "second": 8217
        },
        {
            "id": "20405757-b1a7-4ec1-aa94-e9433d39f78d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -30,
            "first": 44,
            "second": 8220
        },
        {
            "id": "0bd10ca9-4441-4cde-9fcd-4c5ddc8feb71",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -30,
            "first": 44,
            "second": 8221
        },
        {
            "id": "23b14bd6-4157-4907-9f6c-f219be8d9aa8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -18,
            "first": 44,
            "second": 8226
        },
        {
            "id": "182013b7-f616-45b7-8661-2a9b04dc08dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -18,
            "first": 44,
            "second": 8249
        },
        {
            "id": "bb074121-e457-4a53-8da5-3a6e60154fcf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -18,
            "first": 44,
            "second": 8250
        },
        {
            "id": "c30c42f4-45ac-415d-9fec-9dcc9ad5f65f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -30,
            "first": 44,
            "second": 8482
        },
        {
            "id": "685fa65e-7ef2-4d63-9f7f-8447d67e31f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -24,
            "first": 45,
            "second": 34
        },
        {
            "id": "58d47536-bd32-4a6d-953c-8794499e928f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 45,
            "second": 38
        },
        {
            "id": "0bc51621-d443-46ec-a319-56a4e34ab6b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -24,
            "first": 45,
            "second": 39
        },
        {
            "id": "3ac49c44-27aa-40dd-85be-5c3511edb28d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -24,
            "first": 45,
            "second": 42
        },
        {
            "id": "64d7052e-c1c2-4e30-ae79-476dd05e1974",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -18,
            "first": 45,
            "second": 44
        },
        {
            "id": "51ffeaf0-5eb9-4aa1-ac2a-70b6ed955988",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -18,
            "first": 45,
            "second": 46
        },
        {
            "id": "e58540c6-93fc-4a37-82be-07371b4a5dd4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 45,
            "second": 47
        },
        {
            "id": "f2a43f79-47b2-4dd8-abc6-d5dc6b71bddc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 45,
            "second": 65
        },
        {
            "id": "cce53cdb-0b67-4a2e-bbe8-ea91a83d18d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -24,
            "first": 45,
            "second": 84
        },
        {
            "id": "3effd24b-8d67-42fe-8c50-2dc2317b6097",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -15,
            "first": 45,
            "second": 86
        },
        {
            "id": "332289d6-bbae-4095-afea-70a842818376",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 45,
            "second": 87
        },
        {
            "id": "7d533217-2855-4539-9d7f-26717ac0fcaa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 45,
            "second": 88
        },
        {
            "id": "7f7072db-a240-4b15-9bbe-23730b13b748",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -21,
            "first": 45,
            "second": 89
        },
        {
            "id": "278558a9-9639-4ef5-a8f8-828a062309d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 45,
            "second": 90
        },
        {
            "id": "0917beaf-128b-4011-a74f-3274cb6159e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -15,
            "first": 45,
            "second": 92
        },
        {
            "id": "ee056724-c7fd-4763-a2a0-b189bc43e210",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -24,
            "first": 45,
            "second": 170
        },
        {
            "id": "7cdbcea9-a6c5-415d-acb7-47906f4cab53",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -24,
            "first": 45,
            "second": 176
        },
        {
            "id": "db912fdc-c537-49e3-b7a8-c1b104b656bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -24,
            "first": 45,
            "second": 186
        },
        {
            "id": "3d4551b1-fca3-4d03-aab3-0b1a8848e5c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 45,
            "second": 192
        },
        {
            "id": "c97cf4c3-1756-4f3a-87c0-fdaa247f4a96",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 45,
            "second": 193
        },
        {
            "id": "bab2d0c0-ce34-4fca-a02b-def307a912a2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 45,
            "second": 194
        },
        {
            "id": "dab40512-8bd8-4617-8028-23eadf9065b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 45,
            "second": 195
        },
        {
            "id": "94989548-3b8b-435b-85fb-640d9359abdb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 45,
            "second": 196
        },
        {
            "id": "9b537e17-e24e-4734-924d-a4c305720f43",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 45,
            "second": 197
        },
        {
            "id": "3b8a1c6f-b5ff-48be-a4f4-acab1d30dc95",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 45,
            "second": 198
        },
        {
            "id": "ad9046c3-34d5-4d00-8002-e92e1ad1b5a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -21,
            "first": 45,
            "second": 221
        },
        {
            "id": "f55b1ae5-9ced-4820-bb21-c1ba6c4452b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 45,
            "second": 260
        },
        {
            "id": "b13127c6-8299-4426-bce4-b078cf4a7ea3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -21,
            "first": 45,
            "second": 376
        },
        {
            "id": "5341a880-893a-4538-a792-079a910a00d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 45,
            "second": 377
        },
        {
            "id": "b0fdfdf3-0f68-4c6c-a02c-34057d932b26",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 45,
            "second": 379
        },
        {
            "id": "b47a60f0-c083-48ce-800d-133c6a0a54d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 45,
            "second": 381
        },
        {
            "id": "070e6233-879a-4bc6-a5ad-7cbada9e23d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -24,
            "first": 45,
            "second": 8216
        },
        {
            "id": "a1d4cc48-3149-48bd-bd2a-94e0b4ff48a2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -24,
            "first": 45,
            "second": 8217
        },
        {
            "id": "3bed9744-39f8-4051-987f-55ecaf376a8a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -18,
            "first": 45,
            "second": 8218
        },
        {
            "id": "dabc8d62-e58c-4072-a7af-77a96713632f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -24,
            "first": 45,
            "second": 8220
        },
        {
            "id": "856fcf87-fecb-4de5-987c-743d2807549a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -24,
            "first": 45,
            "second": 8221
        },
        {
            "id": "7b1c8cad-3959-44c7-b6ae-abd0bbf64dec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -18,
            "first": 45,
            "second": 8222
        },
        {
            "id": "790b0ead-7c2c-49e4-8e3b-62d68b4e6f82",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -24,
            "first": 45,
            "second": 8482
        },
        {
            "id": "da796cc2-9450-45a3-9641-7371cea99768",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 45,
            "second": 8710
        },
        {
            "id": "25e76fa3-32d2-4829-a70d-32cb2d5b712c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -30,
            "first": 46,
            "second": 34
        },
        {
            "id": "820fb7c2-2d1b-4b8d-872b-4b30aedefdd9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -30,
            "first": 46,
            "second": 39
        },
        {
            "id": "ff80a528-9536-45a7-a67d-0834ea4802f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -30,
            "first": 46,
            "second": 42
        },
        {
            "id": "47a961d0-30dc-40db-8d72-97babdcc5bfd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -18,
            "first": 46,
            "second": 45
        },
        {
            "id": "b7741e07-c3e4-4e6f-a580-3d86b198a6b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 46,
            "second": 64
        },
        {
            "id": "2c3b1dfb-6b7a-463d-8d8f-57dd22dd4dd4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 46,
            "second": 67
        },
        {
            "id": "87b9ee68-97ad-49fb-ac55-3ce749c23b88",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 46,
            "second": 71
        },
        {
            "id": "57097269-6c70-4282-9799-0343af66523a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 46,
            "second": 79
        },
        {
            "id": "1439dee9-ea3c-442a-b899-d2c5d047bd38",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 46,
            "second": 81
        },
        {
            "id": "643f7157-ad6d-4cfa-af57-53f448119a91",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -24,
            "first": 46,
            "second": 84
        },
        {
            "id": "55a934ee-00b2-40b4-8a4a-1efdccb7df3c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -24,
            "first": 46,
            "second": 86
        },
        {
            "id": "cfc9ae9b-0756-492e-8c86-1ffaeb8bf615",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -16,
            "first": 46,
            "second": 87
        },
        {
            "id": "85778012-1d8b-4a75-a2fe-7b110b8c6c25",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -20,
            "first": 46,
            "second": 89
        },
        {
            "id": "8f0bb6cc-0b64-4dd5-91f6-fabd016bd34c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -24,
            "first": 46,
            "second": 92
        },
        {
            "id": "47b871e0-550e-4d25-8f41-1b83998181b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -18,
            "first": 46,
            "second": 118
        },
        {
            "id": "002a35ca-f63f-4640-9c0c-35801e55ab73",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 46,
            "second": 119
        },
        {
            "id": "3e1658e2-f089-496f-b257-e07d7e650806",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -18,
            "first": 46,
            "second": 121
        },
        {
            "id": "2b47017e-8096-4952-868c-e17a7ff92bc9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -30,
            "first": 46,
            "second": 170
        },
        {
            "id": "248fd99a-bf39-4d1c-a82b-36f95126b98e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -18,
            "first": 46,
            "second": 171
        },
        {
            "id": "772270f2-0a69-414d-b33e-ad70a5886169",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -18,
            "first": 46,
            "second": 173
        },
        {
            "id": "417d5e6e-a852-4647-9762-06760b5ab1c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -30,
            "first": 46,
            "second": 176
        },
        {
            "id": "b3eee61a-fbad-4649-9af0-44ef8affdd11",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -18,
            "first": 46,
            "second": 183
        },
        {
            "id": "f3d0091b-cfec-4392-95f1-bc4105044db6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -30,
            "first": 46,
            "second": 186
        },
        {
            "id": "8019a5ce-7a29-40c4-bebe-9451769ac26b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -18,
            "first": 46,
            "second": 187
        },
        {
            "id": "a936e7f2-93fb-4299-b227-d8b41adb08a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 46,
            "second": 199
        },
        {
            "id": "d79b57ba-8df7-4f33-820f-bc20dc758550",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 46,
            "second": 210
        },
        {
            "id": "6ac768df-977a-4aca-9e76-dcde92e12e0c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 46,
            "second": 211
        },
        {
            "id": "80757f3e-94ab-4dcd-ab62-3a639c1b1e5a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 46,
            "second": 212
        },
        {
            "id": "ff20c88e-717a-4789-a611-5fe2c2db0424",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 46,
            "second": 213
        },
        {
            "id": "46969297-2420-46bd-9fd8-cdbf719791d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 46,
            "second": 214
        },
        {
            "id": "db645c66-92ec-40b0-b063-b06c74081969",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 46,
            "second": 216
        },
        {
            "id": "195b9517-a9b3-4f7a-a2c9-8b6f4989cb2e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -20,
            "first": 46,
            "second": 221
        },
        {
            "id": "0ff378b1-c2ec-4fa1-ab54-88de3c4f2d66",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 46,
            "second": 262
        },
        {
            "id": "8533747b-fa2c-4ff6-a89a-5d9d5a7d2a0b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 46,
            "second": 338
        },
        {
            "id": "b0b5278c-10a8-4d4a-800e-fe0b83527f50",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -20,
            "first": 46,
            "second": 376
        },
        {
            "id": "05b583e0-0c1c-43b4-ad40-9bfe3feace36",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -18,
            "first": 46,
            "second": 8211
        },
        {
            "id": "9e961bf9-680f-466e-a8c7-992d573d2ba8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -18,
            "first": 46,
            "second": 8212
        },
        {
            "id": "c0c1343f-ebb1-42be-837d-49f58e3a9e31",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -30,
            "first": 46,
            "second": 8216
        },
        {
            "id": "853ebbfb-7b3f-44b2-895e-520ada8e4914",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -30,
            "first": 46,
            "second": 8217
        },
        {
            "id": "8bc2b781-6f06-412e-9ff1-9f08bb5bc80c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -30,
            "first": 46,
            "second": 8220
        },
        {
            "id": "3aedf944-5fec-47ad-9d5f-0b9854555ac1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -30,
            "first": 46,
            "second": 8221
        },
        {
            "id": "ff6238e1-bbe7-4346-ac13-1b3bd8e6794f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -18,
            "first": 46,
            "second": 8226
        },
        {
            "id": "c24be27a-fb83-4488-9c77-c4735d9da3cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -18,
            "first": 46,
            "second": 8249
        },
        {
            "id": "a94e63ed-ab5e-4daa-bdc4-799744e3859b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -18,
            "first": 46,
            "second": 8250
        },
        {
            "id": "7abec9a6-2d70-496d-84b3-74d4b9682df6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -30,
            "first": 46,
            "second": 8482
        },
        {
            "id": "24bb8b70-f6bd-4107-919e-ba052eb27fa9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 6,
            "first": 47,
            "second": 34
        },
        {
            "id": "56fdd4b9-c75f-493c-81d4-6ff1c1f5dbc3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -18,
            "first": 47,
            "second": 38
        },
        {
            "id": "852adab9-8fec-4688-a689-43b96d7eb880",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 6,
            "first": 47,
            "second": 39
        },
        {
            "id": "58b10a51-9df4-4683-a7b3-039ca2fee3fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 6,
            "first": 47,
            "second": 42
        },
        {
            "id": "4c59f0af-0a8f-4141-8df6-1d5047c98613",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -26,
            "first": 47,
            "second": 44
        },
        {
            "id": "a7dea614-e02a-4fb4-87ca-1d2ef818e35b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -15,
            "first": 47,
            "second": 45
        },
        {
            "id": "a88bd00f-b352-4e42-8a0b-6eb1aa52e020",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -26,
            "first": 47,
            "second": 46
        },
        {
            "id": "cf5b6772-6d46-4a91-a9e8-f4728e0de3c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -18,
            "first": 47,
            "second": 47
        },
        {
            "id": "84855ca7-3d40-41eb-8847-9056c35698e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 47,
            "second": 58
        },
        {
            "id": "11ee0f4a-db9d-4e96-87ff-cdde2abea8f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 47,
            "second": 59
        },
        {
            "id": "8bd3f91e-990e-4424-90e3-a169935cd730",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 6,
            "first": 47,
            "second": 63
        },
        {
            "id": "e1df5153-67e9-4324-abad-24ec1824a117",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 47,
            "second": 64
        },
        {
            "id": "1118a032-ca41-4e47-969a-31dceaa09a92",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -18,
            "first": 47,
            "second": 65
        },
        {
            "id": "98036e9b-caf3-436b-8fc2-b44a47c6634d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 47,
            "second": 67
        },
        {
            "id": "658992af-c885-49d0-ad00-84945973ba1f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 47,
            "second": 71
        },
        {
            "id": "b40864c9-e0aa-4e62-a380-69efbd380d94",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -20,
            "first": 47,
            "second": 74
        },
        {
            "id": "94ef9298-431c-4c75-a93e-25ae463dd0fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 47,
            "second": 79
        },
        {
            "id": "77b3cbd4-5427-4f26-8f25-b05e05d3f702",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 47,
            "second": 81
        },
        {
            "id": "0210bbe0-aaf4-441f-8289-4b39d5b5c754",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -15,
            "first": 47,
            "second": 97
        },
        {
            "id": "f5e6826f-8c10-4079-86a1-890ad0462db4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -15,
            "first": 47,
            "second": 99
        },
        {
            "id": "2f3b7f1d-7fd5-4908-a5ed-ab93fc8e51d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -15,
            "first": 47,
            "second": 100
        },
        {
            "id": "574ab467-11ef-4607-9432-20af8ae2c5d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -15,
            "first": 47,
            "second": 101
        },
        {
            "id": "49d480de-1664-4033-883e-6eaa0ac28406",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 47,
            "second": 102
        },
        {
            "id": "0464ba09-64db-47a3-b43d-5d8a98170c69",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -18,
            "first": 47,
            "second": 103
        },
        {
            "id": "39c9401c-8b22-4dc0-8d0c-367f86158f6a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 47,
            "second": 109
        },
        {
            "id": "30be9c6f-b62b-4090-8d65-8a8837aeba5b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 47,
            "second": 110
        },
        {
            "id": "3e60707b-7906-4a07-bc13-5f09d3b29c86",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -15,
            "first": 47,
            "second": 111
        },
        {
            "id": "5f479f50-cdb8-46f3-b234-834d1160b954",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 47,
            "second": 112
        },
        {
            "id": "2a9f5fb0-47ae-4abf-b165-d34980c6b674",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -15,
            "first": 47,
            "second": 113
        },
        {
            "id": "9bda2006-032b-47ca-b831-2f7816d69d58",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 47,
            "second": 114
        },
        {
            "id": "248f5a55-cc5f-48f6-b0a4-fcce14f11184",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -14,
            "first": 47,
            "second": 115
        },
        {
            "id": "2439f8b3-1b6b-46ce-ac33-fc33b9f66f5b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 47,
            "second": 116
        },
        {
            "id": "c8262deb-90fb-4cd0-aa77-eb1b20f8a4d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 47,
            "second": 117
        },
        {
            "id": "265c7b79-fe05-4e3d-816d-6a8ce7fdc78e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 47,
            "second": 118
        },
        {
            "id": "84b6b1eb-3b2d-48c6-a9de-971a6aeb4d8d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 47,
            "second": 120
        },
        {
            "id": "df83bbda-99f0-4167-a544-63aeb7006df5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 47,
            "second": 121
        },
        {
            "id": "8966bca7-4de1-4039-ac06-61a2202113df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 47,
            "second": 122
        },
        {
            "id": "d21041c5-59e8-405d-a9b6-a8b47877a54c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 6,
            "first": 47,
            "second": 170
        },
        {
            "id": "67107dda-3d2c-4e1a-9784-8f2409ad5839",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -15,
            "first": 47,
            "second": 171
        },
        {
            "id": "46f5b085-d61e-40fb-a216-2c6d94e0b2d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -15,
            "first": 47,
            "second": 173
        },
        {
            "id": "d9a3a64d-1d3e-4b05-8514-89ba28a626bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 6,
            "first": 47,
            "second": 176
        },
        {
            "id": "cc108f37-dde1-4332-95a7-7311aad4a82f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 8,
            "first": 47,
            "second": 178
        },
        {
            "id": "af355a37-1fe6-40f8-93b7-6e36e4b3880b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 8,
            "first": 47,
            "second": 179
        },
        {
            "id": "cef41d23-10eb-4d06-bc40-6b8ddbde183f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 47,
            "second": 181
        },
        {
            "id": "08b54365-273a-4792-9a0a-5f989829cade",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -15,
            "first": 47,
            "second": 183
        },
        {
            "id": "78c96bba-584b-4cca-b183-38cc7fbb593e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 8,
            "first": 47,
            "second": 185
        },
        {
            "id": "d618248a-4445-42aa-a12a-714c46dd7ad7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 6,
            "first": 47,
            "second": 186
        },
        {
            "id": "1f860a08-0f04-4222-83ed-3aa4b2f53996",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -15,
            "first": 47,
            "second": 187
        },
        {
            "id": "62945011-309c-4887-821c-64424ef75791",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -18,
            "first": 47,
            "second": 192
        },
        {
            "id": "1efa02fd-f209-4c5a-a9dd-335e1a45c867",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -18,
            "first": 47,
            "second": 193
        },
        {
            "id": "0583df52-8999-46ac-b350-2f2a54d6f5e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -18,
            "first": 47,
            "second": 194
        },
        {
            "id": "c847b50d-3b19-45e2-b1ab-7792ea163517",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -18,
            "first": 47,
            "second": 195
        },
        {
            "id": "c43e76d2-9c28-4866-8e68-c50718332c0e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -18,
            "first": 47,
            "second": 196
        },
        {
            "id": "f9613aa3-8fa9-4c87-80e8-6841e9abb671",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -18,
            "first": 47,
            "second": 197
        },
        {
            "id": "5edee6c6-954d-4a03-ad21-6affd711f5eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -18,
            "first": 47,
            "second": 198
        },
        {
            "id": "6e031da7-4251-4bb2-a931-79bc2a933774",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 47,
            "second": 199
        },
        {
            "id": "f8fc30b4-03af-434e-83a8-522dc322a796",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 47,
            "second": 210
        },
        {
            "id": "7d6a61bb-f6c3-4c57-abbb-9df1697aaae7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 47,
            "second": 211
        },
        {
            "id": "ab5dfaa0-a5cc-4b93-a1bd-f2843aa12ddc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 47,
            "second": 212
        },
        {
            "id": "9ac2013f-de74-4690-953f-a68aedc7260e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 47,
            "second": 213
        },
        {
            "id": "4eec1464-6085-4239-ab39-e86373df91e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 47,
            "second": 214
        },
        {
            "id": "0b8ad539-204e-47ed-aa46-af331bd7060e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 47,
            "second": 216
        },
        {
            "id": "2016b4ac-ea1f-40ca-b225-dc29c8c49997",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -15,
            "first": 47,
            "second": 224
        },
        {
            "id": "23f71b4c-d7c1-40f3-888c-f15856ed4ef4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -15,
            "first": 47,
            "second": 225
        },
        {
            "id": "d10d134f-8b0b-4b15-9267-4e232d9fb954",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -15,
            "first": 47,
            "second": 226
        },
        {
            "id": "a993072e-b153-42d0-bab1-4a022895f05d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -15,
            "first": 47,
            "second": 227
        },
        {
            "id": "8c9f292c-5506-42bc-a8bd-36d1dd6e475a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -15,
            "first": 47,
            "second": 228
        },
        {
            "id": "ed31d817-7de8-4b0f-b3e3-ca105c5f4234",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -15,
            "first": 47,
            "second": 229
        },
        {
            "id": "d8d3c39b-8867-4096-b484-549104be72e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -15,
            "first": 47,
            "second": 230
        },
        {
            "id": "16cc5e44-85d5-4315-b60e-5f6b4470723f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -15,
            "first": 47,
            "second": 231
        },
        {
            "id": "7123f537-abf0-4464-9839-aa913b601206",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -15,
            "first": 47,
            "second": 232
        },
        {
            "id": "df021a74-863e-49c0-b69e-7121fc16fe0a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -15,
            "first": 47,
            "second": 233
        },
        {
            "id": "58e05e80-7e69-4c7a-9164-5b067a53b0e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -15,
            "first": 47,
            "second": 234
        },
        {
            "id": "05528c94-a48d-4730-ab06-3df0f7ac7e3e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -15,
            "first": 47,
            "second": 235
        },
        {
            "id": "54bb39bd-2f5d-44f3-9866-5ff6c05edd16",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -15,
            "first": 47,
            "second": 240
        },
        {
            "id": "f8d17f70-441e-47b5-8461-df8db99b61a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 47,
            "second": 241
        },
        {
            "id": "efba87a0-97cf-45f8-9664-eb9d80d9978b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -15,
            "first": 47,
            "second": 242
        },
        {
            "id": "f7e7f9b4-9dc2-491f-881e-e3857aea7b3f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -15,
            "first": 47,
            "second": 243
        },
        {
            "id": "a1f4cf5e-53c8-4b2a-b878-31a7cde8c7b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -15,
            "first": 47,
            "second": 244
        },
        {
            "id": "abbd19f9-2980-472e-8d8b-844c16cef7ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -15,
            "first": 47,
            "second": 245
        },
        {
            "id": "c73e15b8-c925-4c2a-903f-a0683c4d458d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -15,
            "first": 47,
            "second": 246
        },
        {
            "id": "d04544b9-8b16-4e35-8e3f-f9b2400b4bfe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -15,
            "first": 47,
            "second": 248
        },
        {
            "id": "fd8976c5-f3ba-41be-b56e-a0d2c088e5c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 47,
            "second": 249
        },
        {
            "id": "45b31f51-7265-4cf2-9720-d05fa77c9486",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 47,
            "second": 250
        },
        {
            "id": "824c0d2d-7bae-4e15-a5cc-e1c9155a48f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 47,
            "second": 251
        },
        {
            "id": "5286d907-f630-4883-bddf-d4ccf9d9f0ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 47,
            "second": 252
        },
        {
            "id": "c694938c-8a30-4a83-b160-b8d53eaac4a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -18,
            "first": 47,
            "second": 260
        },
        {
            "id": "74e7ae2e-531f-47c0-8e4c-e267a631ce58",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -15,
            "first": 47,
            "second": 261
        },
        {
            "id": "fb84eb82-7f30-4055-b930-50ddad7b9f8f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 47,
            "second": 262
        },
        {
            "id": "f85b509b-897a-41dd-adf5-c90d0039c42a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -15,
            "first": 47,
            "second": 263
        },
        {
            "id": "a8f5cbeb-8299-4939-9f11-de8b8c46d1d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -15,
            "first": 47,
            "second": 281
        },
        {
            "id": "b9fc0c31-10f1-4422-b35d-0b3aa72b4c5a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 47,
            "second": 324
        },
        {
            "id": "16f6af7c-3551-4670-9634-0ccb6fe0de8b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 47,
            "second": 338
        },
        {
            "id": "610e2430-e29e-4d6f-a931-1d15ffb20e38",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -15,
            "first": 47,
            "second": 339
        },
        {
            "id": "ee3d29e7-fb8f-41cf-8b63-7700553f262a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -15,
            "first": 47,
            "second": 8211
        },
        {
            "id": "cb03fc64-54b1-4c58-b57d-df279443a531",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -15,
            "first": 47,
            "second": 8212
        },
        {
            "id": "2e985f56-34cb-470c-8e38-d1617ff0a12a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 6,
            "first": 47,
            "second": 8216
        },
        {
            "id": "d369e2ad-e707-4edb-95df-938002020eac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 6,
            "first": 47,
            "second": 8217
        },
        {
            "id": "2b93706c-497f-41f1-a067-ca31113dfa8f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -26,
            "first": 47,
            "second": 8218
        },
        {
            "id": "7d87c93d-eb47-47e6-8fba-e4102fda27aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 6,
            "first": 47,
            "second": 8220
        },
        {
            "id": "28f813f0-01be-4855-ae8c-cb2f9b541d10",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 6,
            "first": 47,
            "second": 8221
        },
        {
            "id": "1caa9cc4-a119-449d-a7ed-bda58f307b24",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -26,
            "first": 47,
            "second": 8222
        },
        {
            "id": "f68b7e28-e109-4bee-978d-87dcd1fecf3b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -15,
            "first": 47,
            "second": 8226
        },
        {
            "id": "d10bfba1-58d2-45cb-8879-95e742533ff6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -15,
            "first": 47,
            "second": 8249
        },
        {
            "id": "9ac41117-c186-4180-a14c-9f2393d366dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -15,
            "first": 47,
            "second": 8250
        },
        {
            "id": "b4632346-cf51-429a-ade1-2f3873a08a50",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 6,
            "first": 47,
            "second": 8482
        },
        {
            "id": "911d716e-c917-4adf-a3e5-8c9f15164c61",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -18,
            "first": 47,
            "second": 8710
        },
        {
            "id": "d04a885b-8cc2-4d72-aa2e-fc0acd38ebe9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 64,
            "second": 34
        },
        {
            "id": "665679d8-6e19-497a-a645-0561f85e4593",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 64,
            "second": 38
        },
        {
            "id": "f58bd440-c7a3-48a5-aeb2-cea4e846a34c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 64,
            "second": 39
        },
        {
            "id": "6b60f93d-030f-4d9b-aa74-4d6b7813c62e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 64,
            "second": 41
        },
        {
            "id": "b8c55a36-5159-4261-86ec-41d6af41cb4a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 64,
            "second": 42
        },
        {
            "id": "6c0ea58a-6ebf-4f71-9631-3f13a40f07e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 64,
            "second": 44
        },
        {
            "id": "b77b70d7-a37c-4f42-8369-8282f6840406",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 64,
            "second": 46
        },
        {
            "id": "af62672d-f7e2-4daf-bce8-4585d3c117da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 64,
            "second": 47
        },
        {
            "id": "85fb7c0f-fc7c-4567-b3f8-85c5520b2ddd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 64,
            "second": 65
        },
        {
            "id": "779cc80a-a2fa-4db6-a545-354d6feaaf9c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -13,
            "first": 64,
            "second": 84
        },
        {
            "id": "4dd63c2d-17fd-4b09-831f-27b0f84d0cb6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 64,
            "second": 86
        },
        {
            "id": "8550fe01-96d3-4146-ab49-0f278eed3f56",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 64,
            "second": 88
        },
        {
            "id": "d1b1aaa0-fedc-44c9-a78c-a7288f3ad8af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 64,
            "second": 89
        },
        {
            "id": "9dbff7fb-feec-4f5e-8850-abf9ab901cf5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 64,
            "second": 90
        },
        {
            "id": "9bd0d6c3-9351-4c63-b1e0-dc215e8a8f1b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 64,
            "second": 92
        },
        {
            "id": "94788ae2-7f91-45a9-bddc-e8326680883d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 64,
            "second": 93
        },
        {
            "id": "3bac14da-a674-4dd4-a4c8-af0a16d46042",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 64,
            "second": 125
        },
        {
            "id": "0b7bcfd6-792c-4809-b495-ec88c59395d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 64,
            "second": 170
        },
        {
            "id": "07575599-a2c8-492a-b138-bab770201862",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 64,
            "second": 176
        },
        {
            "id": "8d303393-f6fd-4ff4-af50-1169119846b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 64,
            "second": 186
        },
        {
            "id": "fcab2dd2-6766-429c-a0db-ef8be2b8172d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 64,
            "second": 192
        },
        {
            "id": "05586ecc-b20c-4396-a5ad-279fcc4dd7b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 64,
            "second": 193
        },
        {
            "id": "bde72973-f9d9-4f76-8d5d-fb76edce72b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 64,
            "second": 194
        },
        {
            "id": "2f6f882a-5ef6-463f-be96-5819285e564b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 64,
            "second": 195
        },
        {
            "id": "c5418ff8-c525-4a3f-b653-b6b87ac687cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 64,
            "second": 196
        },
        {
            "id": "0328c88e-72a3-4c30-a264-8610b9e0596f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 64,
            "second": 197
        },
        {
            "id": "ee82c9ef-f1ae-47a7-9ada-9b68163952f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 64,
            "second": 198
        },
        {
            "id": "8026cc98-aed4-45aa-94d5-b27c8bf0cf41",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 64,
            "second": 221
        },
        {
            "id": "ec893bcf-cc36-4839-afbf-1a6b6712c428",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 64,
            "second": 260
        },
        {
            "id": "8f879870-7c69-4f85-a15f-b3f976e3bf01",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 64,
            "second": 376
        },
        {
            "id": "3a5f7f3c-7681-4e52-9ff8-541a194a0657",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 64,
            "second": 377
        },
        {
            "id": "291dd67a-9e7b-4735-a519-9bd7a1a9f737",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 64,
            "second": 379
        },
        {
            "id": "8821fdda-2a35-48e6-bd7e-8ff351753ba3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 64,
            "second": 381
        },
        {
            "id": "66232bde-f97c-47bf-b9a0-ceb0c023acdf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 64,
            "second": 8216
        },
        {
            "id": "1c053503-0343-4bd1-be2d-eb95253dafa9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 64,
            "second": 8217
        },
        {
            "id": "7bbdaf68-2307-494b-9a3e-7ca21500f2ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 64,
            "second": 8218
        },
        {
            "id": "115b0704-1ff3-4764-bc87-f121e6bbd1ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 64,
            "second": 8220
        },
        {
            "id": "e33b5787-7b26-440d-9a76-158a24ee4b54",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 64,
            "second": 8221
        },
        {
            "id": "fd0b36b0-4610-4ecf-ae0f-ddf32f896d5e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 64,
            "second": 8222
        },
        {
            "id": "a29b4e1d-8d6b-4ac5-9fdb-7cacf66a965c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 64,
            "second": 8482
        },
        {
            "id": "00c2f400-4245-47dd-afec-c7dbd6229f99",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 64,
            "second": 8710
        },
        {
            "id": "e3d05e99-a056-4b7e-99c1-e0de39c87e78",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -24,
            "first": 65,
            "second": 34
        },
        {
            "id": "d3624863-4593-42bb-9088-531040288da2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -24,
            "first": 65,
            "second": 39
        },
        {
            "id": "eecd7d3f-ac7e-4e24-8f1f-12906f01e195",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -24,
            "first": 65,
            "second": 42
        },
        {
            "id": "7b903a46-80e0-4ce2-b69e-0c60e8642f74",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 65,
            "second": 45
        },
        {
            "id": "0160c89d-f339-4460-a467-098fed7806c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 65,
            "second": 63
        },
        {
            "id": "534f4ddd-7e9b-4819-9890-c09929933ab4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 65,
            "second": 64
        },
        {
            "id": "b792d6c0-ad72-4772-b0a0-43762bfe34da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 65,
            "second": 67
        },
        {
            "id": "4348b728-a7f3-4ab4-aa54-5252ddfae0e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 65,
            "second": 71
        },
        {
            "id": "a68c4817-cd19-4578-84cf-0d63289dc7a2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 7,
            "first": 65,
            "second": 74
        },
        {
            "id": "fed3ef6a-a079-4381-b040-423164561965",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 65,
            "second": 79
        },
        {
            "id": "db0a4adf-b062-4698-97f8-a5ac6dc90fea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 65,
            "second": 81
        },
        {
            "id": "8f7191c2-bb6b-4787-8c64-b426dad1af0d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -18,
            "first": 65,
            "second": 84
        },
        {
            "id": "2c513088-b46e-455c-8c70-ddc582185f28",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 65,
            "second": 85
        },
        {
            "id": "1bc85288-3087-49c0-8508-ec19fe8d7e81",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -18,
            "first": 65,
            "second": 86
        },
        {
            "id": "c866a26c-5405-4903-85df-ab0ceeb373b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 65,
            "second": 87
        },
        {
            "id": "4714901d-c993-4e1c-ba4c-79650f7c837c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -22,
            "first": 65,
            "second": 89
        },
        {
            "id": "a78eeb1d-a139-4a9d-b15c-166d934214e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -18,
            "first": 65,
            "second": 92
        },
        {
            "id": "83d55f1e-d5d8-43d5-818a-adbe59717556",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 65,
            "second": 118
        },
        {
            "id": "07635d26-8c17-4fda-9942-c78397196e33",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 65,
            "second": 121
        },
        {
            "id": "2ccb5385-5c1b-4334-bcb4-6a35471bf1e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -24,
            "first": 65,
            "second": 170
        },
        {
            "id": "9ff586d3-0c8d-4c8c-aabd-97fd32492b9e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 65,
            "second": 171
        },
        {
            "id": "fa37ab1e-c5b2-474d-b846-2c182d6d444a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 65,
            "second": 173
        },
        {
            "id": "fb4eb054-882c-4b5f-86cd-27bff807697b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -24,
            "first": 65,
            "second": 176
        },
        {
            "id": "6bfa1325-3408-4193-b787-c08b896d120e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -25,
            "first": 65,
            "second": 178
        },
        {
            "id": "489242e7-2339-4165-8b2e-8bb40be8f10a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -25,
            "first": 65,
            "second": 179
        },
        {
            "id": "5e8e7af9-b8c1-4672-8ba0-4315f5401b2e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 65,
            "second": 183
        },
        {
            "id": "b1278add-b5b6-49d1-9884-34ea8b475e5b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -25,
            "first": 65,
            "second": 185
        },
        {
            "id": "df32a034-434e-4e0f-bbaa-ca1ff240b248",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -24,
            "first": 65,
            "second": 186
        },
        {
            "id": "89dad854-3e55-4227-9fbb-d3898a055d3a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 65,
            "second": 187
        },
        {
            "id": "d41dd5bd-d49f-43e9-9193-f0090c46607c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 65,
            "second": 199
        },
        {
            "id": "416a1986-bed8-408d-aa38-ca74527328d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 65,
            "second": 210
        },
        {
            "id": "7c41f891-a001-4238-94ee-43f7b199257c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 65,
            "second": 211
        },
        {
            "id": "0400875f-e661-49a8-ae0d-06987af7256f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 65,
            "second": 212
        },
        {
            "id": "98a93c97-7c88-4980-bfa6-cdbf463bf5cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 65,
            "second": 213
        },
        {
            "id": "a52929f9-4a92-4035-8b44-0d2e6290f950",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 65,
            "second": 214
        },
        {
            "id": "78e8f2f1-f5d6-4cf0-b74d-c5c2c49ec030",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 65,
            "second": 216
        },
        {
            "id": "2c906f74-1037-41b5-9754-8f6c6b155961",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 65,
            "second": 217
        },
        {
            "id": "3c2125f4-fd82-4fbb-9c7a-73111258d3c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 65,
            "second": 218
        },
        {
            "id": "277e3103-41e6-42e1-88a5-3e3e32a255d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 65,
            "second": 219
        },
        {
            "id": "0dabbde0-c982-4528-af41-7b70722c81b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 65,
            "second": 220
        },
        {
            "id": "38fc3050-6465-4241-adf0-f994fe818f22",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -22,
            "first": 65,
            "second": 221
        },
        {
            "id": "d0df6ed5-67f9-47ea-be83-22f27c270b49",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 65,
            "second": 262
        },
        {
            "id": "a74f6608-26b1-4071-9d68-47b0d1a6b304",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 65,
            "second": 338
        },
        {
            "id": "e8ad0cfd-7ec8-487a-b175-3102215102b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -22,
            "first": 65,
            "second": 376
        },
        {
            "id": "1d61c310-436f-4453-83ad-ff459d8a0400",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 65,
            "second": 8211
        },
        {
            "id": "6eb691a0-6f1c-4df2-9db1-d2fa5598f593",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 65,
            "second": 8212
        },
        {
            "id": "42b2caf7-22e9-42b3-b571-8f32d603f87c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -24,
            "first": 65,
            "second": 8216
        },
        {
            "id": "13418da9-1dab-4662-8754-f257b4ee3338",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -24,
            "first": 65,
            "second": 8217
        },
        {
            "id": "c0e1dbe1-87db-4d8c-b9ae-5a0fa35161ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -24,
            "first": 65,
            "second": 8220
        },
        {
            "id": "1356432a-36e4-4975-b7a1-bdc0a9ca9bf3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -24,
            "first": 65,
            "second": 8221
        },
        {
            "id": "5e8b945f-134d-4007-9588-487df35fb611",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 65,
            "second": 8226
        },
        {
            "id": "230abd18-b1a9-4448-932a-203c622f6928",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 65,
            "second": 8249
        },
        {
            "id": "f6929b53-795a-40ec-8c92-e8987d4d786a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 65,
            "second": 8250
        },
        {
            "id": "f252e8ca-55a5-4e76-b26d-a11815059e87",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -24,
            "first": 65,
            "second": 8482
        },
        {
            "id": "74e3b7a9-f754-4fab-8614-8a6f85b07504",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -20,
            "first": 67,
            "second": 45
        },
        {
            "id": "fb1c35b3-92aa-4959-9959-ef80f7f19c3a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -20,
            "first": 67,
            "second": 171
        },
        {
            "id": "c6d652cd-9372-41f2-9270-322904c220b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -20,
            "first": 67,
            "second": 173
        },
        {
            "id": "772f5e73-a2d4-48c7-9681-424406047dd5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -20,
            "first": 67,
            "second": 183
        },
        {
            "id": "698c1c01-a4a7-4957-877c-7b7340ef96eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -20,
            "first": 67,
            "second": 187
        },
        {
            "id": "3723d457-7082-493f-bdc2-95b5a526cff2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -20,
            "first": 67,
            "second": 8211
        },
        {
            "id": "abafa819-ef85-473d-99d7-32adc47dabbd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -20,
            "first": 67,
            "second": 8212
        },
        {
            "id": "aa6c6b79-93ac-464b-a727-4bb1a8d9d7aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -20,
            "first": 67,
            "second": 8226
        },
        {
            "id": "d77c9a88-9a63-43b0-b02a-2abfe765cb57",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -20,
            "first": 67,
            "second": 8249
        },
        {
            "id": "6945a364-9338-475d-b722-30dff3e95e31",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -20,
            "first": 67,
            "second": 8250
        },
        {
            "id": "ca52d33b-50c3-435b-802f-d5649d08740e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 68,
            "second": 34
        },
        {
            "id": "cd9198ad-4535-402c-860a-e2391a843bda",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 68,
            "second": 38
        },
        {
            "id": "46382974-36c1-4012-84fb-e15eb231e467",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 68,
            "second": 39
        },
        {
            "id": "b7de8304-7fbe-4f8e-b063-c26b13110fe6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 68,
            "second": 41
        },
        {
            "id": "5dd9af03-7706-4cb0-8340-bb06bacd3c80",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 68,
            "second": 42
        },
        {
            "id": "205870e9-288b-471e-9885-79fc0230603d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 68,
            "second": 44
        },
        {
            "id": "0f09886b-e55c-4278-a40a-278fe7ea43f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 68,
            "second": 46
        },
        {
            "id": "e91ac1dd-a57a-4416-81bd-018112ed91c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 68,
            "second": 47
        },
        {
            "id": "27f25515-bc42-4f9f-becf-c3563733d17a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 68,
            "second": 65
        },
        {
            "id": "50acfdbe-09e9-4fdc-a0b1-5570639cd66b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -13,
            "first": 68,
            "second": 84
        },
        {
            "id": "282bb6d3-2fc2-4991-ae4e-d5422fd30cf4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 68,
            "second": 86
        },
        {
            "id": "5a2f1ee4-42df-4dad-8fda-2d57bd4506ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 68,
            "second": 88
        },
        {
            "id": "038b5479-6eee-40d5-8b05-bc7bc129bcce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 68,
            "second": 89
        },
        {
            "id": "5b1619bc-59b9-4119-8c51-8d1c7eca447d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 68,
            "second": 90
        },
        {
            "id": "5f816da4-1631-4205-b4b5-d2bf211df02d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 68,
            "second": 92
        },
        {
            "id": "3a3b130b-44a9-4eb8-96b0-fa77dd7e5b70",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 68,
            "second": 93
        },
        {
            "id": "e7e13bb4-0fa7-43de-adab-f1999182a0b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 68,
            "second": 125
        },
        {
            "id": "7c035840-19a5-4160-83a6-4f75e88e951c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 68,
            "second": 170
        },
        {
            "id": "b303b8b1-8ee0-4725-9350-60e70fb33025",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 68,
            "second": 176
        },
        {
            "id": "3330db31-4174-4a76-81f7-2b72bb252cb3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 68,
            "second": 186
        },
        {
            "id": "1d3303ad-b999-4e61-9f34-4efd8ec9a857",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 68,
            "second": 192
        },
        {
            "id": "69180646-2956-40df-b11b-b042f481dba9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 68,
            "second": 193
        },
        {
            "id": "2d6effc5-b4e0-41b6-a158-8e88618023d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 68,
            "second": 194
        },
        {
            "id": "e863eef5-3724-429e-9d2b-5008eac01d5c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 68,
            "second": 195
        },
        {
            "id": "28bfc1cd-a803-4822-9182-6c3cebd9e374",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 68,
            "second": 196
        },
        {
            "id": "5d97da6e-d8a0-468d-a66f-1afc955158ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 68,
            "second": 197
        },
        {
            "id": "b0d0fc80-71e6-42f5-9ec3-f1d2f89b36f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 68,
            "second": 198
        },
        {
            "id": "af47ea91-893c-4cf4-a136-75d69e14acc5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 68,
            "second": 221
        },
        {
            "id": "23634f8b-4249-4945-abf0-7f97a6429ca4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 68,
            "second": 260
        },
        {
            "id": "9378d80e-c93c-4b97-ada8-4053cc69c2b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 68,
            "second": 376
        },
        {
            "id": "3cb94ad1-72ae-474c-929c-55b5cbc8b7f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 68,
            "second": 377
        },
        {
            "id": "db255f9b-6a12-4c5d-9f06-306709fc6ec7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 68,
            "second": 379
        },
        {
            "id": "5f5223ab-66b2-4dc0-89ba-35085faf68f4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 68,
            "second": 381
        },
        {
            "id": "b127b6e2-7040-462f-834d-b091760a253d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 68,
            "second": 8216
        },
        {
            "id": "d9cc1437-e80f-4f13-ba3a-b98b71ad35fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 68,
            "second": 8217
        },
        {
            "id": "199514d2-255b-47eb-a618-a2e4ac0307dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 68,
            "second": 8218
        },
        {
            "id": "c4a09100-2b65-4abe-9e8f-0ea49dc7ed5a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 68,
            "second": 8220
        },
        {
            "id": "c936e93b-b31e-4341-9294-68904b52a84b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 68,
            "second": 8221
        },
        {
            "id": "41c4429d-bb0c-4b81-bfef-49ff2366d499",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 68,
            "second": 8222
        },
        {
            "id": "45b503b5-1f50-46b8-b777-d6ef8a6d6752",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 68,
            "second": 8482
        },
        {
            "id": "01ef9c52-a1be-40fa-95a4-df55836f3e3f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 68,
            "second": 8710
        },
        {
            "id": "7e4278bd-1296-4a78-8538-0b264d0492e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -18,
            "first": 70,
            "second": 38
        },
        {
            "id": "7a947886-57d5-4fce-b479-7e6b9e5493be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -24,
            "first": 70,
            "second": 44
        },
        {
            "id": "1e906272-9827-4b64-934c-4ba577ca4eb3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -24,
            "first": 70,
            "second": 46
        },
        {
            "id": "c8b6f34a-a022-4976-ac0d-f73e8bf20d70",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -18,
            "first": 70,
            "second": 47
        },
        {
            "id": "e92ccfcd-a27c-4969-a207-743c63192e1f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 70,
            "second": 58
        },
        {
            "id": "f49c183b-6e42-416c-9e61-275fef77e8f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 70,
            "second": 59
        },
        {
            "id": "9df8343a-3ba3-48b4-8319-e7b2b51b60ed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 4,
            "first": 70,
            "second": 63
        },
        {
            "id": "a1c74cfb-6021-4f37-9711-02487fad03f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -18,
            "first": 70,
            "second": 65
        },
        {
            "id": "8c1992b4-c4ee-4e59-b025-01707b379b9c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -26,
            "first": 70,
            "second": 74
        },
        {
            "id": "65216f17-8b3c-4ac6-b91e-47bbdaef308e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 70,
            "second": 99
        },
        {
            "id": "4bfc6d14-941c-43fa-a101-8e9326f16915",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 70,
            "second": 100
        },
        {
            "id": "8f6badb8-3246-4f87-9dbb-f4ad930c4db7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 70,
            "second": 101
        },
        {
            "id": "221dfc0c-762f-479e-9588-d6ebdad13679",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 70,
            "second": 109
        },
        {
            "id": "d3cc1a6e-a16d-4dfa-8d35-b7e6bbebbb08",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 70,
            "second": 110
        },
        {
            "id": "1e7ecf64-bb71-4e2f-960f-5e3aaa84520b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 70,
            "second": 111
        },
        {
            "id": "bd299411-a074-4371-b1cc-e15199cc2f74",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 70,
            "second": 112
        },
        {
            "id": "463d92da-5e6e-4baf-b8eb-2540ee06288e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 70,
            "second": 113
        },
        {
            "id": "353722fa-387c-4971-aa35-716dc877a309",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 70,
            "second": 114
        },
        {
            "id": "9c4b881d-a87f-496d-8b18-58d9f0b20bbe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 70,
            "second": 117
        },
        {
            "id": "9a275f93-b47d-4feb-b932-3dd81d7a0c5b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 70,
            "second": 181
        },
        {
            "id": "5b1c0119-2c75-481f-8b0d-04b35b546f3d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -18,
            "first": 70,
            "second": 192
        },
        {
            "id": "8c7575cc-5100-40ce-989d-2c4e29db0423",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -18,
            "first": 70,
            "second": 193
        },
        {
            "id": "90fbc13a-5728-4946-9a46-c9d1e450db6e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -18,
            "first": 70,
            "second": 194
        },
        {
            "id": "62d0dbf0-3a48-4418-9631-19f094e103f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -18,
            "first": 70,
            "second": 195
        },
        {
            "id": "e18345dc-52b6-4762-a778-882239849028",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -18,
            "first": 70,
            "second": 196
        },
        {
            "id": "630ed2b3-7f0d-4c71-8890-c85a754972be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -18,
            "first": 70,
            "second": 197
        },
        {
            "id": "961cfe06-7080-44fa-b252-66d7f686d26b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -18,
            "first": 70,
            "second": 198
        },
        {
            "id": "dbe147f5-39c6-46a9-b47c-74e911f1546c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 70,
            "second": 231
        },
        {
            "id": "a8bfb4a7-842d-4a22-9950-9a5d10410555",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 70,
            "second": 232
        },
        {
            "id": "ef1283cb-8783-4d96-8845-35f744d05389",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 70,
            "second": 233
        },
        {
            "id": "668f52eb-1582-4ac3-a908-bdf3cb65fd48",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 70,
            "second": 234
        },
        {
            "id": "3ffd00d6-2777-4845-a891-e59322688dcb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 70,
            "second": 235
        },
        {
            "id": "5aaeeed2-5529-4c82-a8e4-24b2a1f63a1b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 70,
            "second": 240
        },
        {
            "id": "3136112c-950f-4129-8e54-eabd53b354fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 70,
            "second": 241
        },
        {
            "id": "a3904e15-a01e-4668-9e21-6154a6152105",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 70,
            "second": 242
        },
        {
            "id": "7b62c2ae-68d3-4f2f-9912-ba28fa1a0d18",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 70,
            "second": 243
        },
        {
            "id": "6934d6c1-4b8e-4c87-8452-48928e6732e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 70,
            "second": 244
        },
        {
            "id": "8f12e5f5-753b-4d40-a52a-efc8b7a7dba1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 70,
            "second": 245
        },
        {
            "id": "d3f36da9-1a4a-4267-b52d-472379c2319c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 70,
            "second": 246
        },
        {
            "id": "cc5d512a-06ed-4f26-a150-ce93e2f72789",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 70,
            "second": 248
        },
        {
            "id": "11dc8af1-2710-47ed-8d72-40b6750edb05",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 70,
            "second": 249
        },
        {
            "id": "ec844fd0-ce3d-48f4-a161-7e7c48252689",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 70,
            "second": 250
        },
        {
            "id": "1b171e90-5ad5-4b68-b859-f1e40a52148d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 70,
            "second": 251
        },
        {
            "id": "5b6030c2-e160-4a06-8dca-f948f8fcd11a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 70,
            "second": 252
        },
        {
            "id": "e5ac7ccb-b287-4528-ad0c-98bb7caf7980",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -18,
            "first": 70,
            "second": 260
        },
        {
            "id": "87e3a12c-e8a7-4e01-852d-7d304a734294",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 70,
            "second": 263
        },
        {
            "id": "65025eb9-3a1e-4094-b4a9-499e00358fb9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 70,
            "second": 281
        },
        {
            "id": "896c65db-f983-4ecf-8ca2-0309207fbdc2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 70,
            "second": 324
        },
        {
            "id": "da3fff28-e50b-4c70-8e1c-a6cc007c2e07",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 70,
            "second": 339
        },
        {
            "id": "d78e7ede-d1c6-4144-a4b3-edeb9e122d57",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -24,
            "first": 70,
            "second": 8218
        },
        {
            "id": "bafc1a32-fdd2-4004-aadc-d6a6647b07da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -24,
            "first": 70,
            "second": 8222
        },
        {
            "id": "3ba56977-eddc-4642-82d9-3bb101c52c3c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -18,
            "first": 70,
            "second": 8710
        },
        {
            "id": "5be24107-82c1-4b67-bb44-638edacb2d18",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 74,
            "second": 38
        },
        {
            "id": "cd73c261-7e76-4625-9701-1f4545b1298f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 74,
            "second": 44
        },
        {
            "id": "4d1bb72a-132e-4e5f-bd9c-546f49d55c2f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 74,
            "second": 46
        },
        {
            "id": "b6ecebfb-97f0-4492-995c-a8766e9ff283",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 74,
            "second": 47
        },
        {
            "id": "fa75c326-f2f2-4217-b8eb-10a65903525e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 74,
            "second": 65
        },
        {
            "id": "9017ad8b-59a8-49fa-8929-7c5f7796efeb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 74,
            "second": 192
        },
        {
            "id": "8592babb-3709-4335-9875-fcacfdf7c971",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 74,
            "second": 193
        },
        {
            "id": "a026480c-0fb3-4132-9eaa-f06cbb0bb105",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 74,
            "second": 194
        },
        {
            "id": "d078f9e7-c07e-4b13-aef7-c0c8773dde84",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 74,
            "second": 195
        },
        {
            "id": "986618c4-1631-433f-94a3-e52dc7330d3b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 74,
            "second": 196
        },
        {
            "id": "183f7fa5-d6df-48d1-a01d-8dc65e603530",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 74,
            "second": 197
        },
        {
            "id": "ebc2a5a0-0f43-4f04-ae51-a768378125d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 74,
            "second": 198
        },
        {
            "id": "ca6933a5-fe2b-4bac-99d3-25f07aa16fe4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 74,
            "second": 260
        },
        {
            "id": "8142da12-6b17-43f7-a56b-b74c14775161",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 74,
            "second": 8218
        },
        {
            "id": "546e9915-3e24-4798-b76a-b38d4290b851",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 74,
            "second": 8222
        },
        {
            "id": "fca91720-3322-4cb8-ae4e-7e139929beb2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 74,
            "second": 8710
        },
        {
            "id": "726a17b0-7432-4eea-aadb-9b5ec6151216",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 75,
            "second": 45
        },
        {
            "id": "f4955162-4b18-43c1-8be7-e54738dd9cf1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 75,
            "second": 64
        },
        {
            "id": "330681f9-1c98-403c-b60c-2568b28b62da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 75,
            "second": 67
        },
        {
            "id": "2fdc68a9-57e3-40e1-8662-8b0e4f68591b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 75,
            "second": 71
        },
        {
            "id": "e0ee5c81-7d9f-4764-8b60-45115ed93a77",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 75,
            "second": 79
        },
        {
            "id": "728a4d04-e741-4c6e-a239-5c3fba321020",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 75,
            "second": 81
        },
        {
            "id": "5aad1e90-4661-44df-a18e-7378fb00afbf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 75,
            "second": 99
        },
        {
            "id": "fc9a84e3-3b40-4241-beaa-57337c99ec5d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 75,
            "second": 100
        },
        {
            "id": "54edbe15-9abb-4fb3-a3e3-c3798d45c1a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 75,
            "second": 101
        },
        {
            "id": "2a2fe55a-377e-4c71-9ef6-63f4a95a4371",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 75,
            "second": 102
        },
        {
            "id": "371db1ad-456f-4c73-aaef-b2d633404998",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 75,
            "second": 111
        },
        {
            "id": "cbabb03f-b3ea-4482-95fd-1f1b17370360",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 75,
            "second": 113
        },
        {
            "id": "ba5c7370-4ab5-4e9e-8664-aa4b62db85c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 75,
            "second": 116
        },
        {
            "id": "05095c9d-4cc6-47b3-a7e0-e8b226d25db1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 75,
            "second": 118
        },
        {
            "id": "19f29b6e-5aff-4166-a389-b9dd67b20a0b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 75,
            "second": 119
        },
        {
            "id": "37a9d1f8-069b-4d3a-ae03-ef062ef4e55e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 75,
            "second": 121
        },
        {
            "id": "fa2b2c24-65ac-4ed7-a2ed-8d377e68f099",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 75,
            "second": 171
        },
        {
            "id": "7a4a521a-d8eb-456b-93c2-06e95842deea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 75,
            "second": 173
        },
        {
            "id": "356b0d91-6fa6-40c1-8a58-37ece3fd94ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 75,
            "second": 183
        },
        {
            "id": "cd5e0908-6819-4c32-baaf-7ec664ef432d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 75,
            "second": 187
        },
        {
            "id": "6b2feedb-b52d-400b-8c21-37f9b073359a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 75,
            "second": 199
        },
        {
            "id": "053fce10-1ed2-41bb-820e-25e5adf434b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 75,
            "second": 210
        },
        {
            "id": "c19769c2-bfa2-476a-9dbe-ac14e603f18a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 75,
            "second": 211
        },
        {
            "id": "6104fb97-fffb-4035-b7bc-037d53db5eef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 75,
            "second": 212
        },
        {
            "id": "4ad057ec-4fd0-429b-8068-1f036b598463",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 75,
            "second": 213
        },
        {
            "id": "d595c516-5c23-453d-a1f5-d981643a42c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 75,
            "second": 214
        },
        {
            "id": "687e6f75-5edb-4f75-a7c2-58a487308c15",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 75,
            "second": 216
        },
        {
            "id": "7c73bda1-4754-44eb-b003-ba8dcbd8e33a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 75,
            "second": 231
        },
        {
            "id": "0b177917-9b76-4e21-8df9-cb6f8dcc23c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 75,
            "second": 232
        },
        {
            "id": "d0069396-8e09-47e7-97cb-a4c6ad296551",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 75,
            "second": 233
        },
        {
            "id": "362343f2-aa45-4375-9a83-ea855661aaa5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 75,
            "second": 234
        },
        {
            "id": "566eecfb-fc53-46a6-99c2-ae3c2bef33a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 75,
            "second": 235
        },
        {
            "id": "c15a54b4-e8e4-4bad-b293-eaac3cf9593b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 75,
            "second": 240
        },
        {
            "id": "3171bab9-0ae3-4aa9-8b0f-2278d8b64979",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 75,
            "second": 242
        },
        {
            "id": "502ff351-38fc-443c-8721-06b55c2486c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 75,
            "second": 243
        },
        {
            "id": "b332955f-1af3-4b1f-a63b-6d5be48669e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 75,
            "second": 244
        },
        {
            "id": "d9a47959-9874-437c-b1db-1ec2d41c3c91",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 75,
            "second": 245
        },
        {
            "id": "c698fc68-0f8e-4fcf-8f5c-8ed74e82ad07",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 75,
            "second": 246
        },
        {
            "id": "5c9ce57f-6a17-4c4f-9bc7-4d0fabf2e7e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 75,
            "second": 248
        },
        {
            "id": "b89ea667-f063-4f6f-850b-baf77fbaf190",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 75,
            "second": 262
        },
        {
            "id": "78ba92e7-6fb9-4111-909e-09aafff38aaa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 75,
            "second": 263
        },
        {
            "id": "82f07679-de94-4906-8ce9-88a7b6892b52",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 75,
            "second": 281
        },
        {
            "id": "c326feec-6c25-4c3a-858d-d34ac40f5064",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 75,
            "second": 338
        },
        {
            "id": "3a57bd3b-eab6-4287-b050-400d8153372f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 75,
            "second": 339
        },
        {
            "id": "c65873c4-d0c7-4150-9d61-d797a117fdab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 75,
            "second": 8211
        },
        {
            "id": "dd265884-62b1-4fc6-bd11-ab640e1a79b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 75,
            "second": 8212
        },
        {
            "id": "c353e9d8-63d8-4e4f-bdb5-059ed2e3ab0e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 75,
            "second": 8226
        },
        {
            "id": "3efc899e-57fc-4c51-a27f-b28ac67b95c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 75,
            "second": 8249
        },
        {
            "id": "d055e5af-baff-4ddb-8d1d-720e4fa15dce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 75,
            "second": 8250
        },
        {
            "id": "f411c60c-edde-4eea-90ea-7fd8c85e40e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -39,
            "first": 76,
            "second": 34
        },
        {
            "id": "cf0d9f24-3743-412b-b5a7-7c29ec1e4011",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -39,
            "first": 76,
            "second": 39
        },
        {
            "id": "ae6173dd-583f-4da1-a106-568705187420",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -39,
            "first": 76,
            "second": 42
        },
        {
            "id": "5475558c-d4a5-42c7-8e08-57ab4072334d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 7,
            "first": 76,
            "second": 44
        },
        {
            "id": "82253f81-d92d-4562-9c23-90161bd96a0c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -26,
            "first": 76,
            "second": 45
        },
        {
            "id": "84fe86cb-6bac-41ec-9f9b-0ab94d146adf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 7,
            "first": 76,
            "second": 46
        },
        {
            "id": "512a1163-e840-4804-85ce-e9695f838d76",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 76,
            "second": 63
        },
        {
            "id": "e1f63108-bd8f-482b-9370-55beb4f0279f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 76,
            "second": 64
        },
        {
            "id": "8851ae5d-a37d-4ec2-a3c9-03f60765e1f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 76,
            "second": 67
        },
        {
            "id": "6a3fd9e5-df43-4816-9de3-515b4c257da8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 76,
            "second": 71
        },
        {
            "id": "6b3041da-c8f0-4167-9ff0-094f3f63cb23",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 76,
            "second": 79
        },
        {
            "id": "7b2f5772-9152-4eea-adf5-04047e3a50c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 76,
            "second": 81
        },
        {
            "id": "6f74d1a6-4af7-44a0-9da4-86812ebf10e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -23,
            "first": 76,
            "second": 84
        },
        {
            "id": "6e22d99c-c3df-4f92-b911-5e5d4b50c93a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -24,
            "first": 76,
            "second": 86
        },
        {
            "id": "903dbacc-02d6-4e88-93f3-93df4409bc70",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -20,
            "first": 76,
            "second": 87
        },
        {
            "id": "411c350a-adb7-462f-b728-e03766295276",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -28,
            "first": 76,
            "second": 89
        },
        {
            "id": "af9b767a-ef3a-4ce2-97f8-41dee784114a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -24,
            "first": 76,
            "second": 92
        },
        {
            "id": "f3a7ed3f-959f-4374-b9e2-8b3bd5193fff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 76,
            "second": 99
        },
        {
            "id": "7a384799-452b-4b6d-a4d4-81b05870cb84",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 76,
            "second": 100
        },
        {
            "id": "e08c6780-0528-4e98-977f-ce6713b80638",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 76,
            "second": 101
        },
        {
            "id": "fde4c439-0214-4f3a-96b5-d0bc1048dc73",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 76,
            "second": 111
        },
        {
            "id": "dec48fa0-4363-4150-ae41-8a87d1151cff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 76,
            "second": 113
        },
        {
            "id": "999655d5-c012-4774-b6c9-5a3a09ff0cdb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -14,
            "first": 76,
            "second": 118
        },
        {
            "id": "2e5db188-a044-4f06-93ba-3ad459b91ae8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 76,
            "second": 119
        },
        {
            "id": "f76c2f32-a72f-4a98-9baa-c5e22eb6f0e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -14,
            "first": 76,
            "second": 121
        },
        {
            "id": "effb1ea5-fd8b-441e-add9-311cbdd93867",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -39,
            "first": 76,
            "second": 170
        },
        {
            "id": "62ab874e-dfef-4eb9-b881-7489b8e6ccde",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -26,
            "first": 76,
            "second": 171
        },
        {
            "id": "bfe03679-396a-4989-ab90-9a75c991f04f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -26,
            "first": 76,
            "second": 173
        },
        {
            "id": "ed8265ea-a2ac-402c-8ef4-e9ab69966465",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -39,
            "first": 76,
            "second": 176
        },
        {
            "id": "38269200-5acd-407a-be38-7e6cac0cf354",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -27,
            "first": 76,
            "second": 178
        },
        {
            "id": "c451987a-f3bf-406d-9eec-9299bc6adefe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -27,
            "first": 76,
            "second": 179
        },
        {
            "id": "70be7e05-77dc-4863-8f6e-98e7c4c06b31",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -26,
            "first": 76,
            "second": 183
        },
        {
            "id": "0e963b1c-2b41-4c96-8581-ca762c857120",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -27,
            "first": 76,
            "second": 185
        },
        {
            "id": "6f7dc5e9-62f0-43b5-bbc7-0e873f691cd9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -39,
            "first": 76,
            "second": 186
        },
        {
            "id": "489bbdbb-01fb-4b28-8b42-c871d50bfcbb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -26,
            "first": 76,
            "second": 187
        },
        {
            "id": "7a3f161f-b120-4dfc-b19e-069fe1ab1db8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 76,
            "second": 199
        },
        {
            "id": "a5a55a0d-265c-47ab-98be-afd508018777",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 76,
            "second": 210
        },
        {
            "id": "aab73e98-e35a-4542-94fa-316a3d9d7030",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 76,
            "second": 211
        },
        {
            "id": "221441cf-ad0d-4759-b32e-ab103b221633",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 76,
            "second": 212
        },
        {
            "id": "a1ad55cb-af98-4b8b-9dc0-d1146052a727",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 76,
            "second": 213
        },
        {
            "id": "96b43e8a-6803-4018-a29c-6333f1c1922e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 76,
            "second": 214
        },
        {
            "id": "af78e0c5-8cc6-4da3-9f8d-eded36374f19",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 76,
            "second": 216
        },
        {
            "id": "5c8f1e48-919f-4908-a9df-88525ba0e0d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -28,
            "first": 76,
            "second": 221
        },
        {
            "id": "a15ab418-c851-4e82-b94c-d5a06c07c716",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 76,
            "second": 231
        },
        {
            "id": "26ac374a-a2f8-4aa3-9f65-c8c3a1704fd0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 76,
            "second": 232
        },
        {
            "id": "0f517dff-aa85-4ec0-bff0-64583e9840d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 76,
            "second": 233
        },
        {
            "id": "01f4fa5f-8c48-43d8-aefe-a64858ee09ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 76,
            "second": 234
        },
        {
            "id": "45c53b21-ec92-4c9c-8c8b-8ad4ac05390d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 76,
            "second": 235
        },
        {
            "id": "d486a62c-7b68-4f1c-b052-84bb6f3cc1c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 76,
            "second": 240
        },
        {
            "id": "211c1de9-29a9-4e5f-84ee-c3c8e5f7c30b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 76,
            "second": 242
        },
        {
            "id": "87fe4d82-e73b-4a6c-b8e2-1a01f899c839",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 76,
            "second": 243
        },
        {
            "id": "cd5aadf5-3d4e-4c62-a783-bb1ab15dee16",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 76,
            "second": 244
        },
        {
            "id": "be4b39e2-3ef3-4546-9440-6c82893892d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 76,
            "second": 245
        },
        {
            "id": "d960424e-3e5d-4830-a943-72df751a5fbb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 76,
            "second": 246
        },
        {
            "id": "858cc176-7854-4cec-99f9-e2e0d8dec655",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 76,
            "second": 248
        },
        {
            "id": "e6b15d3c-2530-4257-b7c3-38acc1cbd61b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 76,
            "second": 262
        },
        {
            "id": "b3f84480-120e-4d41-8b72-3a71545a1814",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 76,
            "second": 263
        },
        {
            "id": "d8856536-04a8-4b4d-9b3f-14f6f532a2c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 76,
            "second": 281
        },
        {
            "id": "abb2d25c-abb1-4cad-9c57-034508082622",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 76,
            "second": 338
        },
        {
            "id": "6f79c1ff-d184-428b-97d3-c2d3da6d44bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 76,
            "second": 339
        },
        {
            "id": "adffca74-5100-425b-9d3a-149cf8888101",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -28,
            "first": 76,
            "second": 376
        },
        {
            "id": "d9161f21-fa03-4d0c-a3c6-003bbc44c4a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -26,
            "first": 76,
            "second": 8211
        },
        {
            "id": "30e84a6a-b3f6-40a9-a9fc-cbb3974ee548",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -26,
            "first": 76,
            "second": 8212
        },
        {
            "id": "132b9ea1-e04f-41f5-a561-d32b733eaeb4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -39,
            "first": 76,
            "second": 8216
        },
        {
            "id": "763162db-e0a5-4a91-ba97-7d01900a4e6d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -39,
            "first": 76,
            "second": 8217
        },
        {
            "id": "3aec4260-0425-458b-b581-5efb0ca24e68",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 7,
            "first": 76,
            "second": 8218
        },
        {
            "id": "048bf92f-8297-4586-b4cd-9482bb7014c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -39,
            "first": 76,
            "second": 8220
        },
        {
            "id": "2aa79834-065e-4b39-b815-e51a9037433f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -39,
            "first": 76,
            "second": 8221
        },
        {
            "id": "b344e85c-add0-4899-a657-9bcd4d6292bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 7,
            "first": 76,
            "second": 8222
        },
        {
            "id": "6ec2c853-eecd-4395-a755-33c39cd224f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -26,
            "first": 76,
            "second": 8226
        },
        {
            "id": "493d6e35-9c19-4fb0-948d-1569154c1593",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -26,
            "first": 76,
            "second": 8249
        },
        {
            "id": "1464faf4-3d99-4b10-977b-aae5340662bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -26,
            "first": 76,
            "second": 8250
        },
        {
            "id": "24f16e18-38bb-4dde-a953-382521b5fff8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -39,
            "first": 76,
            "second": 8482
        },
        {
            "id": "15a6b1a3-6ad9-415a-9ab4-ffa005577bcb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 79,
            "second": 34
        },
        {
            "id": "a815066b-f963-4f74-85b5-ca919978714d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 79,
            "second": 38
        },
        {
            "id": "b7798fb0-0980-4ba4-a995-3b6eb4cb8596",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 79,
            "second": 39
        },
        {
            "id": "1ad6e621-661f-4d2c-aefe-5d0b9c44dcc1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 79,
            "second": 41
        },
        {
            "id": "f2950d8d-e2de-4764-b2cc-bb27fe0c07e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 79,
            "second": 42
        },
        {
            "id": "f4b6643e-a4c2-4b55-a86c-cc9cb991d983",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 79,
            "second": 44
        },
        {
            "id": "70273e27-af6b-418b-a240-338b22d5ab8b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 79,
            "second": 46
        },
        {
            "id": "014c6c06-1277-4417-bbfd-59d8bda39d4e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 79,
            "second": 47
        },
        {
            "id": "eee53be1-b07a-47e0-bf5f-e37574fce053",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 79,
            "second": 65
        },
        {
            "id": "5b994406-4659-4bec-b756-8d07c3a074f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -13,
            "first": 79,
            "second": 84
        },
        {
            "id": "979b2cfd-c952-4db4-a2d1-7aa8937aafcd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 79,
            "second": 86
        },
        {
            "id": "ff38f227-7d2c-4033-8abe-e63bf1aaca85",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 79,
            "second": 88
        },
        {
            "id": "3084abbc-ac19-4070-8816-19547a37787f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 79,
            "second": 89
        },
        {
            "id": "6a1fe74f-7dc1-4ba3-9c87-3cb1fa2f1323",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 79,
            "second": 90
        },
        {
            "id": "8e9a0613-188d-47fd-a0a8-aad38994fd52",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 79,
            "second": 92
        },
        {
            "id": "9e71bd9e-0fd6-43ed-84b9-6327db96d5d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 79,
            "second": 93
        },
        {
            "id": "eb4bce28-2f23-48be-a9a2-58ba83d014be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 79,
            "second": 125
        },
        {
            "id": "788126eb-3bf3-4a00-9396-030a52514e68",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 79,
            "second": 170
        },
        {
            "id": "247e6505-c462-4874-a4db-f3596b65d419",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 79,
            "second": 176
        },
        {
            "id": "47cab242-c9ff-40a3-8e56-51747653a01a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 79,
            "second": 186
        },
        {
            "id": "39d10dce-5e4e-48c5-9b94-562bcf013e9d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 79,
            "second": 192
        },
        {
            "id": "e68ca2d9-5e33-489f-8421-37e8f597309f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 79,
            "second": 193
        },
        {
            "id": "26fe8dc5-b888-47bf-b375-ea483896f02b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 79,
            "second": 194
        },
        {
            "id": "640c2942-0eb7-4a2c-928a-98a506be0912",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 79,
            "second": 195
        },
        {
            "id": "acfd9a9d-2d63-4a8b-a4b9-24a2f1d972e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 79,
            "second": 196
        },
        {
            "id": "3151e35e-469b-49be-8ab4-7651c4d4d742",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 79,
            "second": 197
        },
        {
            "id": "7ca72896-3643-4801-ba15-b0989e676f31",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 79,
            "second": 198
        },
        {
            "id": "e334a585-c583-4619-9941-30a55caf7963",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 79,
            "second": 221
        },
        {
            "id": "cd625f1c-61b5-45b4-add1-aee6756cde6f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 79,
            "second": 260
        },
        {
            "id": "7b09705b-c71c-4ddf-bc77-020fa4d64b54",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 79,
            "second": 376
        },
        {
            "id": "6c4b46c0-9ea0-48e4-9dab-f52b53ce9070",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 79,
            "second": 377
        },
        {
            "id": "458e8bf4-f6e5-4bba-9aa4-f03cac4ee0a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 79,
            "second": 379
        },
        {
            "id": "f8cbd0e8-3e75-4e26-a3eb-14990aad4e71",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 79,
            "second": 381
        },
        {
            "id": "f1890838-de71-477e-8a33-a1a4a5ca7480",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 79,
            "second": 8216
        },
        {
            "id": "468a01f9-8b34-4944-b02d-15e893fe79c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 79,
            "second": 8217
        },
        {
            "id": "53cbd25b-0fc0-48a7-a899-55b6d6197eb3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 79,
            "second": 8218
        },
        {
            "id": "2569ecc3-ba3b-47bc-a629-61f6b3b79933",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 79,
            "second": 8220
        },
        {
            "id": "555ddf70-3816-46e4-8f2b-a72b1076acda",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 79,
            "second": 8221
        },
        {
            "id": "3bc2ecd1-230f-4375-817a-347604cc5cca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 79,
            "second": 8222
        },
        {
            "id": "2f427f2f-cea0-42ae-9b79-645b26519f9d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 79,
            "second": 8482
        },
        {
            "id": "f1326e7e-ce90-4567-8700-61a5d1e37fba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 79,
            "second": 8710
        },
        {
            "id": "579b7b57-5083-4f13-a53d-567a954008bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -18,
            "first": 80,
            "second": 38
        },
        {
            "id": "7c53b341-558e-43c1-a947-886e6090cc13",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -33,
            "first": 80,
            "second": 44
        },
        {
            "id": "cf075317-d506-40cf-b1d9-6d63a8028c8e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -33,
            "first": 80,
            "second": 46
        },
        {
            "id": "e08d150c-bbe3-4fb5-9440-217226ec3671",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -18,
            "first": 80,
            "second": 47
        },
        {
            "id": "e6754a6f-6bfa-43cd-be09-f61e5325664d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -18,
            "first": 80,
            "second": 65
        },
        {
            "id": "e03e927a-b24e-4080-a23c-746b57d074ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -24,
            "first": 80,
            "second": 74
        },
        {
            "id": "71e4acd8-9b40-4121-b705-a0aaed5a3ddf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 80,
            "second": 97
        },
        {
            "id": "ac2c3d26-93c9-4a4e-92d9-f6fcd5f80d1f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 80,
            "second": 99
        },
        {
            "id": "a1463b94-adb2-4d5b-aa54-e12c0d698fe0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 80,
            "second": 100
        },
        {
            "id": "44e5d178-ba80-484f-9fa8-3ef1b769a6f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 80,
            "second": 101
        },
        {
            "id": "e79788d7-d5ba-45da-b46d-1b69bd7781fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 80,
            "second": 111
        },
        {
            "id": "37569e90-0a15-4297-8214-387f5538057d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 80,
            "second": 113
        },
        {
            "id": "0df2f311-8cff-44fe-bd50-de8aa33d9494",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -18,
            "first": 80,
            "second": 192
        },
        {
            "id": "6a072d10-2801-40bf-8dae-9a33552f32c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -18,
            "first": 80,
            "second": 193
        },
        {
            "id": "3e603fd8-70c6-471c-9462-32f511d0c289",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -18,
            "first": 80,
            "second": 194
        },
        {
            "id": "694c9ec0-2d31-471f-96a2-576369b3c040",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -18,
            "first": 80,
            "second": 195
        },
        {
            "id": "a5bf3f15-e8d7-42d2-81a5-0cb7580b664d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -18,
            "first": 80,
            "second": 196
        },
        {
            "id": "eea2b9d1-dd47-43fa-a175-ae10826f1f85",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -18,
            "first": 80,
            "second": 197
        },
        {
            "id": "ec7d3f26-1b87-4637-9f5b-fedfad13042e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -18,
            "first": 80,
            "second": 198
        },
        {
            "id": "28117eed-8b6b-4e4e-9a06-62a73e0cedee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 80,
            "second": 224
        },
        {
            "id": "a430bf8f-9c1c-4cb2-851a-50b8bd28d89d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 80,
            "second": 225
        },
        {
            "id": "0ff8f501-bd7c-4a84-87dd-387679d4df2a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 80,
            "second": 226
        },
        {
            "id": "812e4f77-75fb-414b-8acd-8981cb741f50",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 80,
            "second": 227
        },
        {
            "id": "4ffe9509-e426-47a4-b4e6-6cc671fb37f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 80,
            "second": 228
        },
        {
            "id": "e7694bcf-ad11-4d0b-be02-54a7e1e308be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 80,
            "second": 229
        },
        {
            "id": "984cb82f-6870-4594-b5bf-f0d5484e62fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 80,
            "second": 230
        },
        {
            "id": "5bea8617-a9a9-44a7-aec3-5d534214d262",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 80,
            "second": 231
        },
        {
            "id": "d74c208e-980d-45b0-b17a-6a3e3eb7c755",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 80,
            "second": 232
        },
        {
            "id": "ba854190-cf8c-408d-99a0-7c6b7e72c206",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 80,
            "second": 233
        },
        {
            "id": "918476a6-cbb0-48a5-9f73-74e5cd87e814",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 80,
            "second": 234
        },
        {
            "id": "02b31361-8cd0-46d5-99c7-4fe1e39e5dd4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 80,
            "second": 235
        },
        {
            "id": "e4df1d70-c6f2-41fe-8a91-86266811f4c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 80,
            "second": 240
        },
        {
            "id": "4edf38f6-fc89-467f-a2c5-8077e9bfbf79",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 80,
            "second": 242
        },
        {
            "id": "28c9caaa-df40-47e0-bab0-aefd3fb7cb5d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 80,
            "second": 243
        },
        {
            "id": "a113877d-c8f9-440e-8e33-267b21983fd0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 80,
            "second": 244
        },
        {
            "id": "0347717d-0d07-47ee-9342-4152c5929ed6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 80,
            "second": 245
        },
        {
            "id": "581083ed-7020-4d08-a2be-b9fb1135e882",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 80,
            "second": 246
        },
        {
            "id": "1b0f7aa0-5633-4960-a3b8-58c94e6b0c8a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 80,
            "second": 248
        },
        {
            "id": "7cd9a752-f61c-4ba9-bbaf-d606de303c5c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -18,
            "first": 80,
            "second": 260
        },
        {
            "id": "5222ff48-b29c-4284-ba24-47d76cf38793",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 80,
            "second": 261
        },
        {
            "id": "a17ded42-56ca-4261-b1dc-3393cab78ab4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 80,
            "second": 263
        },
        {
            "id": "a08577c0-b11a-40f9-9e19-d394c68c6777",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 80,
            "second": 281
        },
        {
            "id": "8e1073ab-f0e2-478d-8ed1-e30fbad953fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 80,
            "second": 339
        },
        {
            "id": "b74c1831-f636-4542-81f5-bf1a1be915d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -33,
            "first": 80,
            "second": 8218
        },
        {
            "id": "d3430edc-49c1-4cd5-9f81-5fa0c4ab6758",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -33,
            "first": 80,
            "second": 8222
        },
        {
            "id": "b3f66e7c-5a56-49da-82ad-7e55b36f5f2e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -18,
            "first": 80,
            "second": 8710
        },
        {
            "id": "a8b51d77-8ead-449e-bb38-38f9a2fdbac0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 81,
            "second": 34
        },
        {
            "id": "a7db423e-8dc9-4f5a-b1f0-6fce10756ac6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 81,
            "second": 38
        },
        {
            "id": "766f0308-38d5-4b27-9786-1368006451e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 81,
            "second": 39
        },
        {
            "id": "f53cb7c4-06ec-463d-9f60-7a757fa54ce1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 81,
            "second": 41
        },
        {
            "id": "224aac39-5177-4f67-8f38-7b77a0b86cde",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 81,
            "second": 42
        },
        {
            "id": "83eab7ca-12e8-4c3a-838c-ab46dd9d7542",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 81,
            "second": 44
        },
        {
            "id": "3e3973e7-6333-428e-a61e-6c091840ca99",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 81,
            "second": 46
        },
        {
            "id": "57eb806b-b6df-412f-8cc8-a2fdf85e1354",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 81,
            "second": 47
        },
        {
            "id": "a498cd9c-2d60-4629-828f-0d9a641f51f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 81,
            "second": 65
        },
        {
            "id": "1ebc6914-5573-466e-8bfb-173475fa8d81",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -13,
            "first": 81,
            "second": 84
        },
        {
            "id": "4f7ad40d-4921-4aea-9018-214e02cebc10",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 81,
            "second": 86
        },
        {
            "id": "d42fadc0-718a-4feb-ae59-29735c51d2a2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 81,
            "second": 88
        },
        {
            "id": "c52fa86d-010c-49ff-b4bd-0d1557190712",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 81,
            "second": 89
        },
        {
            "id": "ffecc897-9de2-4c23-ae21-67eee92e481d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 81,
            "second": 90
        },
        {
            "id": "40bbe3af-3593-4a0b-92d9-1d913a99e0eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 81,
            "second": 92
        },
        {
            "id": "8e2d96c7-c409-4c5a-a189-c496cc10261a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 81,
            "second": 93
        },
        {
            "id": "daf3c3de-fd06-42d0-8dc9-f4d0a5c4e4ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 81,
            "second": 125
        },
        {
            "id": "188717ba-43f4-43f3-bf14-4b108bc0900d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 81,
            "second": 170
        },
        {
            "id": "f338ebc5-68bb-43ad-b9ce-5500845df7ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 81,
            "second": 176
        },
        {
            "id": "6becb2fb-96a1-4e5a-991b-02db96c64fc7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 81,
            "second": 186
        },
        {
            "id": "d4cf1a1d-7adb-49eb-98e5-d7121004a5b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 81,
            "second": 192
        },
        {
            "id": "36e281ff-0f77-4473-b2cf-fa71c6856f86",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 81,
            "second": 193
        },
        {
            "id": "9c8e45a9-924e-4683-871f-9132b945f2b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 81,
            "second": 194
        },
        {
            "id": "1700aebd-eb18-44a7-af79-6bd7756c0c67",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 81,
            "second": 195
        },
        {
            "id": "21c32edf-00fd-4ee8-8bb5-8c6ce9c5f095",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 81,
            "second": 196
        },
        {
            "id": "a35c84ab-7b4c-402b-98d3-f5e419124a4c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 81,
            "second": 197
        },
        {
            "id": "ec6eba39-5131-4a41-a2fd-f6e6772b539f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 81,
            "second": 198
        },
        {
            "id": "d1ebd4b7-97bc-4c47-aa61-e915fc13d3c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 81,
            "second": 221
        },
        {
            "id": "6202f407-a777-48d3-bf23-fcc42a5ee7f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 81,
            "second": 260
        },
        {
            "id": "8671e269-1386-45bf-aa65-bba43240e597",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 81,
            "second": 376
        },
        {
            "id": "9b3ef177-f218-49ea-8730-ada3c7cbc129",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 81,
            "second": 377
        },
        {
            "id": "c18528f4-d2e3-4b1e-a32f-1d86e9362783",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 81,
            "second": 379
        },
        {
            "id": "2690eb69-c214-416f-bdd3-c90cc0c763e1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 81,
            "second": 381
        },
        {
            "id": "e640fb65-6c0e-46e0-ba45-94d17017c079",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 81,
            "second": 8216
        },
        {
            "id": "616e56aa-2f76-4a99-ac00-28a66a81865a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 81,
            "second": 8217
        },
        {
            "id": "6eff6a04-3de1-4ec7-8009-0a188369eeb4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 81,
            "second": 8218
        },
        {
            "id": "2fada400-5daf-41ea-8789-d45547503a94",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 81,
            "second": 8220
        },
        {
            "id": "f2ea7882-ecc2-45cc-8d46-f96b341e0ebd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 81,
            "second": 8221
        },
        {
            "id": "2f32c3d2-1a1f-4658-8f9f-0cc96a5ea826",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 81,
            "second": 8222
        },
        {
            "id": "ab80e752-0297-4ba7-8482-1bc0374abe2f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 81,
            "second": 8482
        },
        {
            "id": "d34f5d3f-ca0a-4579-8840-5f5d89b94c64",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 81,
            "second": 8710
        },
        {
            "id": "efc2183a-8244-4f09-8fb9-7e326216b98f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 82,
            "second": 64
        },
        {
            "id": "320550cc-5d3a-45e9-a95b-ed277214f7ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 82,
            "second": 67
        },
        {
            "id": "ef4bb6c1-1e44-4628-8d8e-424febf2b40c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 82,
            "second": 71
        },
        {
            "id": "cc5e7571-d695-4546-96b0-830049b2e683",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 82,
            "second": 79
        },
        {
            "id": "dbc2d606-70c7-46bd-95f5-c30461a3d0a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 82,
            "second": 81
        },
        {
            "id": "0ab9c6b6-4f09-4dba-8442-a5e13c5c9310",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 82,
            "second": 84
        },
        {
            "id": "cb974ec8-2e54-45a4-be56-5c8c9fbbb1c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 82,
            "second": 85
        },
        {
            "id": "d78d7b56-ae43-4b36-9194-a9715cec9405",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 82,
            "second": 199
        },
        {
            "id": "dc63ce0a-9450-46d6-8f8c-ec80655a1027",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 82,
            "second": 210
        },
        {
            "id": "fae6f551-0c4a-47d4-bc53-384d398f7e44",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 82,
            "second": 211
        },
        {
            "id": "ca6ceb98-87a4-4ab1-bad1-78495a406bc3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 82,
            "second": 212
        },
        {
            "id": "5c0a4b7f-20f5-498d-800c-d44c2cd14b17",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 82,
            "second": 213
        },
        {
            "id": "75905ac0-6552-4196-a434-cb692f4a155e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 82,
            "second": 214
        },
        {
            "id": "579c30a7-8f87-47a8-853b-313c97fc9437",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 82,
            "second": 216
        },
        {
            "id": "5795450f-f6b8-49b3-8049-ac2872da6f10",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 82,
            "second": 217
        },
        {
            "id": "807b4213-0c21-414e-b740-fb6034c3eb8f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 82,
            "second": 218
        },
        {
            "id": "66c30a44-f8fa-4f4d-b173-c55545ef8d0e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 82,
            "second": 219
        },
        {
            "id": "83824d28-ca77-4f1f-9a8b-e02e0a2fe9e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 82,
            "second": 220
        },
        {
            "id": "f4dada7b-53df-4e11-88cc-8123d34bb4fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 82,
            "second": 262
        },
        {
            "id": "eca190e0-d78c-4e29-be0d-c978f739b491",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 82,
            "second": 338
        },
        {
            "id": "3b5739b7-27fb-42b4-b6f6-2f1cc0a7c173",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -18,
            "first": 84,
            "second": 38
        },
        {
            "id": "2b1913a0-db0f-4321-a225-6a6a15b297d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -24,
            "first": 84,
            "second": 44
        },
        {
            "id": "54d42646-ac2d-4f8e-bb16-54a0c6df6d8c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -24,
            "first": 84,
            "second": 45
        },
        {
            "id": "cba27897-1f03-400c-a36a-308e95d9ba5c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -24,
            "first": 84,
            "second": 46
        },
        {
            "id": "ca35033c-be92-4c24-973f-81ed9ab67922",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -18,
            "first": 84,
            "second": 47
        },
        {
            "id": "d412e292-8368-4dbc-b1af-c688562800e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -21,
            "first": 84,
            "second": 58
        },
        {
            "id": "d6c804cf-9826-450a-b459-0332e48b913d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -21,
            "first": 84,
            "second": 59
        },
        {
            "id": "c8d3c884-0c72-496f-a41e-b7d7a4596b7b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -13,
            "first": 84,
            "second": 64
        },
        {
            "id": "9a6af239-5d5e-4d30-b4dc-ee4f4b02922c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -18,
            "first": 84,
            "second": 65
        },
        {
            "id": "7f553214-bbcd-48e8-b760-994757df9fee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -13,
            "first": 84,
            "second": 67
        },
        {
            "id": "b15cd672-35f3-4658-924c-eb0ebba2304a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -13,
            "first": 84,
            "second": 71
        },
        {
            "id": "13bea916-36c2-441b-9102-8f22b4101fa6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -27,
            "first": 84,
            "second": 74
        },
        {
            "id": "eb8d6eab-183c-46db-baa5-232d6c886d3c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -13,
            "first": 84,
            "second": 79
        },
        {
            "id": "2c46ad03-1b5f-42cc-afdc-53d58eeb0581",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -13,
            "first": 84,
            "second": 81
        },
        {
            "id": "47e4de47-cc17-47c1-a4a3-47e16664e4b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -33,
            "first": 84,
            "second": 97
        },
        {
            "id": "59e43a13-a7ba-453b-905b-361630f63604",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -28,
            "first": 84,
            "second": 99
        },
        {
            "id": "428cbbb8-b284-49a7-b9c2-85f6f00eb4ae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -28,
            "first": 84,
            "second": 100
        },
        {
            "id": "138d4943-33bb-4232-abe5-97af2d180576",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -28,
            "first": 84,
            "second": 101
        },
        {
            "id": "eb51b8e3-cf60-4134-b216-2b4d0650876a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -25,
            "first": 84,
            "second": 103
        },
        {
            "id": "e6004e1a-4913-46c7-bc8f-9b9cb4d34515",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -21,
            "first": 84,
            "second": 109
        },
        {
            "id": "d8c5144e-a6a8-435e-8a42-fc05d2ef75a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -21,
            "first": 84,
            "second": 110
        },
        {
            "id": "30c90933-5cb3-4076-bc5f-257bc4d5536a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -28,
            "first": 84,
            "second": 111
        },
        {
            "id": "20473c1c-bef4-4ab0-9432-d207caa03554",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -21,
            "first": 84,
            "second": 112
        },
        {
            "id": "d4932797-ac6a-457f-a2f1-557b5480424d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -28,
            "first": 84,
            "second": 113
        },
        {
            "id": "f04bea04-0ee0-41ff-a01b-83dfc97e4368",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -21,
            "first": 84,
            "second": 114
        },
        {
            "id": "bbee62d1-6c5e-4595-a873-54781022e463",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -22,
            "first": 84,
            "second": 115
        },
        {
            "id": "cc6e0fd3-1ea0-47da-b0b9-7641ff66a640",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -21,
            "first": 84,
            "second": 117
        },
        {
            "id": "82336bb3-2d5e-400a-8323-7218680ed519",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -24,
            "first": 84,
            "second": 118
        },
        {
            "id": "7668c34d-9214-4451-9e05-1ac1a278ce9f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -19,
            "first": 84,
            "second": 119
        },
        {
            "id": "4fae6fa2-4f03-460a-a062-773c0a0fa36e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -19,
            "first": 84,
            "second": 120
        },
        {
            "id": "bfc10637-fcbb-4a01-b971-b62b3c2fe488",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -24,
            "first": 84,
            "second": 121
        },
        {
            "id": "c745ba69-6581-46ae-93e8-1ff872c98622",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -16,
            "first": 84,
            "second": 122
        },
        {
            "id": "d7d97dc6-4d41-43ed-8ff4-c8dcb28ba022",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -24,
            "first": 84,
            "second": 171
        },
        {
            "id": "f3e224f9-02e3-4a5e-a0e7-3bc237627754",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -24,
            "first": 84,
            "second": 173
        },
        {
            "id": "3cd94de7-e894-405f-8d93-7dd07940bea0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -21,
            "first": 84,
            "second": 181
        },
        {
            "id": "b7c69c9e-a68e-4465-8055-6adde5832a4e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -24,
            "first": 84,
            "second": 183
        },
        {
            "id": "3cb06fe1-707c-4519-b83a-b922414fdb3e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -24,
            "first": 84,
            "second": 187
        },
        {
            "id": "8eb474a0-49a2-498c-99c3-65d700ac2be9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -18,
            "first": 84,
            "second": 192
        },
        {
            "id": "41fa4977-96d2-4381-a8b5-7d8152d94abc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -18,
            "first": 84,
            "second": 193
        },
        {
            "id": "68fdc544-e9b5-42b5-9ab8-ee4b08229686",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -18,
            "first": 84,
            "second": 194
        },
        {
            "id": "32df3bb5-973c-485b-be39-a509ab7d28ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -18,
            "first": 84,
            "second": 195
        },
        {
            "id": "3de3eeb5-682e-4483-9bc0-dc0fa9f4d9c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -18,
            "first": 84,
            "second": 196
        },
        {
            "id": "26797e45-4606-4b0e-91fa-5f2d3e29e3d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -18,
            "first": 84,
            "second": 197
        },
        {
            "id": "38c26706-e5b5-467c-bcbc-cf7758911bef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -18,
            "first": 84,
            "second": 198
        },
        {
            "id": "9e159ea1-fd6e-4228-83f5-d82c2b235028",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -13,
            "first": 84,
            "second": 199
        },
        {
            "id": "713b7ce2-0341-4a96-a590-6a4ce0971e24",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -13,
            "first": 84,
            "second": 210
        },
        {
            "id": "69c07134-206a-45f3-8378-502946c676de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -13,
            "first": 84,
            "second": 211
        },
        {
            "id": "02fe4575-1207-48f5-be96-809911e3d147",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -13,
            "first": 84,
            "second": 212
        },
        {
            "id": "d867c777-7049-4d36-8c95-5581647081d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -13,
            "first": 84,
            "second": 213
        },
        {
            "id": "f436ad8b-3ad9-4e41-a499-8709900cffb2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -13,
            "first": 84,
            "second": 214
        },
        {
            "id": "7a5820dc-a8c1-45f8-857b-7cd9f35131c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -13,
            "first": 84,
            "second": 216
        },
        {
            "id": "2ba111d9-97da-4886-9485-08380958019b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -33,
            "first": 84,
            "second": 224
        },
        {
            "id": "508c8d51-7590-42c8-8428-f715b9e66108",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -33,
            "first": 84,
            "second": 225
        },
        {
            "id": "7374d6e6-c2c4-4cbb-95dc-2b9043a3528c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -33,
            "first": 84,
            "second": 226
        },
        {
            "id": "0e934ca5-832c-4426-81e0-8b3a9b29a740",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -33,
            "first": 84,
            "second": 227
        },
        {
            "id": "9fe9f9eb-47cf-404a-bebc-814fab2ce688",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -33,
            "first": 84,
            "second": 228
        },
        {
            "id": "7eb9d364-06a4-4ad7-b051-67f5dbf4f6f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -33,
            "first": 84,
            "second": 229
        },
        {
            "id": "a5951b2b-04c0-4de7-985d-591b1e828228",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -33,
            "first": 84,
            "second": 230
        },
        {
            "id": "041de099-a6a5-4e03-b7f2-3021c5e2f12f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -28,
            "first": 84,
            "second": 231
        },
        {
            "id": "8b353b87-9751-4818-9def-aa2f80b8d838",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -28,
            "first": 84,
            "second": 232
        },
        {
            "id": "9a3818e5-8640-4977-99f0-cd5935bab722",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -28,
            "first": 84,
            "second": 233
        },
        {
            "id": "86ff42e1-02e0-4ab9-90b1-436ea996e84c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -28,
            "first": 84,
            "second": 234
        },
        {
            "id": "6a4502b2-96ac-4607-ad90-5ad5b15b7abc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -28,
            "first": 84,
            "second": 235
        },
        {
            "id": "12e359ad-ba77-4344-8a02-fcac7574ac58",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -28,
            "first": 84,
            "second": 240
        },
        {
            "id": "e9c56c2f-65fe-429e-850e-f6ba2604be97",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -21,
            "first": 84,
            "second": 241
        },
        {
            "id": "89ba673d-6dc4-411b-b601-756c8b04572f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -28,
            "first": 84,
            "second": 242
        },
        {
            "id": "92e14a86-5328-4b96-84ff-bea841480d85",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -28,
            "first": 84,
            "second": 243
        },
        {
            "id": "429a7a62-3a42-4713-8af4-767e051dd5d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -28,
            "first": 84,
            "second": 244
        },
        {
            "id": "ff8ad6e7-9734-445f-a055-7b932902909e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -28,
            "first": 84,
            "second": 245
        },
        {
            "id": "c1cfb424-a204-4e5c-96c3-9bc27b321d8a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -28,
            "first": 84,
            "second": 246
        },
        {
            "id": "dc02e3cf-7eac-42b0-b00b-f99e8735b4ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -28,
            "first": 84,
            "second": 248
        },
        {
            "id": "12d4de90-6ab4-4361-bbb3-f4d1d26837c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -21,
            "first": 84,
            "second": 249
        },
        {
            "id": "87b88d2d-1cc4-4e4b-b501-4be986847552",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -21,
            "first": 84,
            "second": 250
        },
        {
            "id": "fbb0416d-bc00-4c31-ba55-89ec4abf7e01",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -21,
            "first": 84,
            "second": 251
        },
        {
            "id": "93086fa4-63cd-4ed7-9093-f3014ec9480c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -21,
            "first": 84,
            "second": 252
        },
        {
            "id": "96e078e9-d06c-4fd3-a6ee-92eec0332bb9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -18,
            "first": 84,
            "second": 260
        },
        {
            "id": "70c16acf-8f76-4132-9c0f-e19219f67ab8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -33,
            "first": 84,
            "second": 261
        },
        {
            "id": "2379dad7-f9aa-4039-a823-a0672cfce31d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -13,
            "first": 84,
            "second": 262
        },
        {
            "id": "7c24d3ea-95bd-459d-86ef-945692613903",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -28,
            "first": 84,
            "second": 263
        },
        {
            "id": "02b310be-e925-4108-bb3e-da333d201c19",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -28,
            "first": 84,
            "second": 281
        },
        {
            "id": "edff6238-1286-4aeb-9ac5-408b212f7253",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -21,
            "first": 84,
            "second": 324
        },
        {
            "id": "dd9e1116-7c69-4757-b504-3d5f24c964ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -13,
            "first": 84,
            "second": 338
        },
        {
            "id": "acee1e69-059c-486b-8491-f530d1a258cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -28,
            "first": 84,
            "second": 339
        },
        {
            "id": "f58d3673-dc7c-467d-982a-cb05b0c8b2c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -24,
            "first": 84,
            "second": 8211
        },
        {
            "id": "61feed60-29d1-4452-bb78-605a7c0d7e72",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -24,
            "first": 84,
            "second": 8212
        },
        {
            "id": "0f2862e8-ce32-4de3-b525-06be4dac1092",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -24,
            "first": 84,
            "second": 8218
        },
        {
            "id": "e8c931d0-c1c8-48cb-a50c-8333b33f3e5b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -24,
            "first": 84,
            "second": 8222
        },
        {
            "id": "49383c0a-6d32-4bef-a440-5d21d68d1520",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -24,
            "first": 84,
            "second": 8226
        },
        {
            "id": "ec3a7113-7856-4805-9d42-462651f59c52",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -24,
            "first": 84,
            "second": 8249
        },
        {
            "id": "4b7cf6bb-4fd3-40c3-bed8-64c03f5eab81",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -24,
            "first": 84,
            "second": 8250
        },
        {
            "id": "b23c435d-8039-4334-8108-33208a3f0c4f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -18,
            "first": 84,
            "second": 8710
        },
        {
            "id": "a7918d88-78a8-4ef8-ad50-50c5f5028f05",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 85,
            "second": 38
        },
        {
            "id": "186c941b-e3ae-471b-9745-222281eae09c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 85,
            "second": 44
        },
        {
            "id": "d8564ccf-feff-4e12-9cec-443b6c111180",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 85,
            "second": 46
        },
        {
            "id": "beecb7d6-0d6b-4cb5-b08d-a848bf67095a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 85,
            "second": 47
        },
        {
            "id": "528e1980-b93d-4c15-b079-8c0d0f6ada80",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 85,
            "second": 65
        },
        {
            "id": "4950a16b-f486-4b40-9426-ae878f0a3db9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 85,
            "second": 192
        },
        {
            "id": "854aa6de-1164-4756-8ea6-2fb9388c5248",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 85,
            "second": 193
        },
        {
            "id": "05c282e6-12fd-43b4-9f0e-ea05353e1e08",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 85,
            "second": 194
        },
        {
            "id": "334e8455-0363-46c1-bf92-aa2c5d6767ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 85,
            "second": 195
        },
        {
            "id": "9a12a57e-2ec0-4a5f-a6f4-60262903bb57",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 85,
            "second": 196
        },
        {
            "id": "28cf600c-2c14-4d8b-a3af-98c688b207c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 85,
            "second": 197
        },
        {
            "id": "fe357cd5-7bb4-40ab-9c40-3d1447cec960",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 85,
            "second": 198
        },
        {
            "id": "b257633a-a171-4956-9145-7f555cc4dccc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 85,
            "second": 260
        },
        {
            "id": "cbb084c2-fe52-4f6c-af16-79bf008f212e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 85,
            "second": 8218
        },
        {
            "id": "190b967d-3ae6-4fe0-908a-e7ea15454d63",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 85,
            "second": 8222
        },
        {
            "id": "d7730730-f35d-4886-89e0-256100627992",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 85,
            "second": 8710
        },
        {
            "id": "fda37030-c051-4c57-9fba-56778ca613ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 6,
            "first": 86,
            "second": 34
        },
        {
            "id": "0cd99f8b-0c3a-44b7-9f87-e9606355f4a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -18,
            "first": 86,
            "second": 38
        },
        {
            "id": "a99e2f94-7c5b-4d14-a51c-442f942653d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 6,
            "first": 86,
            "second": 39
        },
        {
            "id": "f97f3dd2-7582-47b9-89c9-863a09a691a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 6,
            "first": 86,
            "second": 42
        },
        {
            "id": "4c0c1b03-b715-4912-b590-2c1a4c41974c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -26,
            "first": 86,
            "second": 44
        },
        {
            "id": "65ec1338-fd55-4650-91f8-4683c41f2fff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -15,
            "first": 86,
            "second": 45
        },
        {
            "id": "3a5bb56b-76c1-4364-b4c2-67d057991cc2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -26,
            "first": 86,
            "second": 46
        },
        {
            "id": "e9cd238c-1251-4fb8-8294-8afd06ae141f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -18,
            "first": 86,
            "second": 47
        },
        {
            "id": "fdfe1fdf-a94f-4470-8672-5e4054d457fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 86,
            "second": 58
        },
        {
            "id": "0057bc96-6e8d-4f7d-99fb-524b54ecee0e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 86,
            "second": 59
        },
        {
            "id": "837b3e1a-b82e-41c0-9d88-6a80cc8efa53",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 6,
            "first": 86,
            "second": 63
        },
        {
            "id": "a39e5999-eb08-478e-b8e0-9005c0f62f34",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 86,
            "second": 64
        },
        {
            "id": "1cf7210e-d57f-4afc-b762-8a3ef2ef79e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -18,
            "first": 86,
            "second": 65
        },
        {
            "id": "9ab2472f-8fca-48c2-8373-b0feeb7cacd2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 86,
            "second": 67
        },
        {
            "id": "4597229c-ef78-4a79-aeae-febe16b9332d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 86,
            "second": 71
        },
        {
            "id": "7d884aad-4515-42ff-89b7-b38dd7c2f2fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -20,
            "first": 86,
            "second": 74
        },
        {
            "id": "d057850a-5d79-413a-9d9c-7daab3e7a0ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 86,
            "second": 79
        },
        {
            "id": "73dae132-d41a-49a8-8e09-521714ce034d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 86,
            "second": 81
        },
        {
            "id": "d668b98b-39c2-4714-a206-f52367f38679",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -15,
            "first": 86,
            "second": 97
        },
        {
            "id": "f090ea2d-a307-4b8d-8cc2-f03ed3119c74",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -15,
            "first": 86,
            "second": 99
        },
        {
            "id": "c5c7c9bd-562d-421c-b615-58c2d0f41137",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -15,
            "first": 86,
            "second": 100
        },
        {
            "id": "a09f0fdb-efa5-41e5-a8df-6bb273ce8ef3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -15,
            "first": 86,
            "second": 101
        },
        {
            "id": "7eeb24bb-b969-4838-8038-ceecfa2e5182",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 86,
            "second": 102
        },
        {
            "id": "aa5f1d84-82d3-4a0a-b4c3-b735e37490cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -18,
            "first": 86,
            "second": 103
        },
        {
            "id": "63ea1554-8894-49ff-97f8-d4d05bfb614e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 86,
            "second": 109
        },
        {
            "id": "12a9bd1c-2d30-45b0-9fad-a9b2290231d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 86,
            "second": 110
        },
        {
            "id": "0baec578-725d-4527-ab57-462d82ff6af3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -15,
            "first": 86,
            "second": 111
        },
        {
            "id": "02880a23-b725-4011-8051-399c55d02a94",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 86,
            "second": 112
        },
        {
            "id": "598f23d3-46b5-46c5-b730-b9afe8a25ded",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -15,
            "first": 86,
            "second": 113
        },
        {
            "id": "eca0413d-a5fe-4cae-a454-250c64c8e50d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 86,
            "second": 114
        },
        {
            "id": "95a0f160-10b2-412e-8b23-0a2be07de18c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -14,
            "first": 86,
            "second": 115
        },
        {
            "id": "045c1a1e-eca7-4af9-87c3-dfd151ebd69b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 86,
            "second": 116
        },
        {
            "id": "30bb9ca4-eca1-48c1-b6b2-b2e497144251",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 86,
            "second": 117
        },
        {
            "id": "e3dfb678-9eff-42d9-862b-2ab2404954a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 86,
            "second": 118
        },
        {
            "id": "972aaa80-8b66-47d2-96da-b40f50e71b7c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 86,
            "second": 120
        },
        {
            "id": "f56ef599-f638-42f6-8206-a41ba3e8e5b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 86,
            "second": 121
        },
        {
            "id": "0f3398e7-bfc5-4cf7-bb21-0c890bdd77fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 86,
            "second": 122
        },
        {
            "id": "d2da6e84-18be-49f5-ba6e-c06510ef8ca7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 6,
            "first": 86,
            "second": 170
        },
        {
            "id": "84e8c86a-917e-4dec-87d2-58a96cef32be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -15,
            "first": 86,
            "second": 171
        },
        {
            "id": "a9985e72-2cb7-49d8-98be-8e1b88bc5612",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -15,
            "first": 86,
            "second": 173
        },
        {
            "id": "83fba128-3180-4888-8040-99a187021359",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 6,
            "first": 86,
            "second": 176
        },
        {
            "id": "3b2b7775-09a4-483f-afd1-e352ced84783",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 8,
            "first": 86,
            "second": 178
        },
        {
            "id": "bedefa3c-3b09-4467-8255-ba5f8d59d0ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 8,
            "first": 86,
            "second": 179
        },
        {
            "id": "6f7a68fe-402b-4174-a453-16537c237f6a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 86,
            "second": 181
        },
        {
            "id": "cba9a9b8-35ff-49af-875f-94cbaf695387",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -15,
            "first": 86,
            "second": 183
        },
        {
            "id": "13e335f7-ae1c-4d49-a094-798827bcb8e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 8,
            "first": 86,
            "second": 185
        },
        {
            "id": "5c9fec65-3c75-43c2-83bf-ef3baaaccfcc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 6,
            "first": 86,
            "second": 186
        },
        {
            "id": "9103947e-e579-4576-92d5-62295c8d49ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -15,
            "first": 86,
            "second": 187
        },
        {
            "id": "8631479d-9c00-4149-8a0c-987c7d41754a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -18,
            "first": 86,
            "second": 192
        },
        {
            "id": "25689425-1687-497e-95c6-29dda352b134",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -18,
            "first": 86,
            "second": 193
        },
        {
            "id": "47f162a5-52d0-4f4e-b39b-1193d194be75",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -18,
            "first": 86,
            "second": 194
        },
        {
            "id": "8b7135ae-8e0b-4543-9579-8d151dbb0889",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -18,
            "first": 86,
            "second": 195
        },
        {
            "id": "ac22a1da-cf98-4202-b39a-6ea9efd97aeb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -18,
            "first": 86,
            "second": 196
        },
        {
            "id": "cae08201-4902-459c-9526-b7b2aed012c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -18,
            "first": 86,
            "second": 197
        },
        {
            "id": "1c3fd8b1-3c5a-43f9-a1f1-5148a2c62031",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -18,
            "first": 86,
            "second": 198
        },
        {
            "id": "587e1a64-dc66-48a4-a0ef-5ffa5518136a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 86,
            "second": 199
        },
        {
            "id": "acc82611-71c4-4893-9ec7-404ceddda934",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 86,
            "second": 210
        },
        {
            "id": "ea30e754-97eb-40fb-a759-849d7de9a215",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 86,
            "second": 211
        },
        {
            "id": "7b28635a-5fb8-49e3-a6ba-9428d9ec54bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 86,
            "second": 212
        },
        {
            "id": "7c354124-323d-4d23-ac14-7ea255309dab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 86,
            "second": 213
        },
        {
            "id": "4ca6b347-c403-4370-891f-f524d88083c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 86,
            "second": 214
        },
        {
            "id": "872dd36d-21d4-4883-8635-73a9981a1393",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 86,
            "second": 216
        },
        {
            "id": "8037854c-36ba-43fc-b05c-7da7769b0d1f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -15,
            "first": 86,
            "second": 224
        },
        {
            "id": "73d03582-d4b1-45f3-a9f7-598b64e13cbd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -15,
            "first": 86,
            "second": 225
        },
        {
            "id": "da0dcfec-f158-4845-8165-ac31756a3c18",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -15,
            "first": 86,
            "second": 226
        },
        {
            "id": "9a1500ee-991a-4b7e-bc35-f3f094d05304",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -15,
            "first": 86,
            "second": 227
        },
        {
            "id": "a8e39891-2a00-4b58-921e-747e317919fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -15,
            "first": 86,
            "second": 228
        },
        {
            "id": "cd8e8777-3897-4fd5-b480-416467c589e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -15,
            "first": 86,
            "second": 229
        },
        {
            "id": "2994da44-7932-4035-b33a-a9f684b1013e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -15,
            "first": 86,
            "second": 230
        },
        {
            "id": "8f71fbac-a654-433d-9d17-9eca6df81313",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -15,
            "first": 86,
            "second": 231
        },
        {
            "id": "119b3c06-d64c-4400-8798-3dc37c57b811",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -15,
            "first": 86,
            "second": 232
        },
        {
            "id": "ba93bf30-19ad-462e-8d0f-6bdd4398feb5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -15,
            "first": 86,
            "second": 233
        },
        {
            "id": "9969f16b-bdc9-43e7-874a-128e75442725",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -15,
            "first": 86,
            "second": 234
        },
        {
            "id": "ac6de6cd-c5e0-4e93-a0bc-1b41f0bcbf50",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -15,
            "first": 86,
            "second": 235
        },
        {
            "id": "325ab31c-0757-4f36-aad2-c3d7e8c60bd3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -15,
            "first": 86,
            "second": 240
        },
        {
            "id": "080b375a-db4e-40ed-9be8-bcc87a7728d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 86,
            "second": 241
        },
        {
            "id": "fcd3abde-eade-4276-99ba-7311dabf054d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -15,
            "first": 86,
            "second": 242
        },
        {
            "id": "d85c58f9-ac9f-4d45-b94f-eabdc757e728",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -15,
            "first": 86,
            "second": 243
        },
        {
            "id": "92e6e5c5-f44c-4847-adc8-28c051a9530c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -15,
            "first": 86,
            "second": 244
        },
        {
            "id": "48d4b114-7e64-4647-b077-9c6a7449c99b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -15,
            "first": 86,
            "second": 245
        },
        {
            "id": "a9d9c8da-e9b8-4c58-9019-151b0091c1ae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -15,
            "first": 86,
            "second": 246
        },
        {
            "id": "01162228-7716-479b-9dc1-21ad1e8c94d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -15,
            "first": 86,
            "second": 248
        },
        {
            "id": "216aea24-80e1-46d5-bcd7-df217100b483",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 86,
            "second": 249
        },
        {
            "id": "57a64968-94b6-4947-a2e9-be1577c26de4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 86,
            "second": 250
        },
        {
            "id": "25ddddc3-d691-455e-9f80-0fcb2e45b858",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 86,
            "second": 251
        },
        {
            "id": "9ed3f4cf-64bd-4028-8084-c42c1a924a9b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 86,
            "second": 252
        },
        {
            "id": "312eb9fa-610c-4c19-b821-8ad874fd8fa8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -18,
            "first": 86,
            "second": 260
        },
        {
            "id": "42ed6d0f-9c8f-4977-8765-7ad56468a1a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -15,
            "first": 86,
            "second": 261
        },
        {
            "id": "6feb50be-7a18-48e8-a6bb-e7dd4724ec67",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 86,
            "second": 262
        },
        {
            "id": "88bc7369-8aa7-4a17-ac71-ea977461d9d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -15,
            "first": 86,
            "second": 263
        },
        {
            "id": "4ef38036-c0aa-4239-944d-e56994a1d1c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -15,
            "first": 86,
            "second": 281
        },
        {
            "id": "e7459175-1f75-4211-af71-4b095f5d9806",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 86,
            "second": 324
        },
        {
            "id": "c9f94525-aab7-457a-8143-1df38c335efb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 86,
            "second": 338
        },
        {
            "id": "ccd4d0c2-51dc-4b1a-8884-3e3591ebe2aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -15,
            "first": 86,
            "second": 339
        },
        {
            "id": "82e41b14-82e0-4a66-886b-a3fe35b3d233",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -15,
            "first": 86,
            "second": 8211
        },
        {
            "id": "eb454a44-8671-4a8f-a388-d51e50db6c69",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -15,
            "first": 86,
            "second": 8212
        },
        {
            "id": "637f2f45-ac14-4582-a2be-a9386b0e8325",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 6,
            "first": 86,
            "second": 8216
        },
        {
            "id": "396bfdfd-2d11-4d52-8b89-c30e2dfd440e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 6,
            "first": 86,
            "second": 8217
        },
        {
            "id": "21bef6e4-5352-4ef4-831a-036fffca2703",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -26,
            "first": 86,
            "second": 8218
        },
        {
            "id": "76ebf2dd-b889-4a6a-ba24-1c15dd8072b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 6,
            "first": 86,
            "second": 8220
        },
        {
            "id": "a9979cc2-627c-4e78-ac57-84f9f2599a3d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 6,
            "first": 86,
            "second": 8221
        },
        {
            "id": "b823eea1-975e-426e-9cb1-c4e656c021e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -26,
            "first": 86,
            "second": 8222
        },
        {
            "id": "2bb4fcbe-bd36-4c16-ae38-cd00791708e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -15,
            "first": 86,
            "second": 8226
        },
        {
            "id": "7fd7335a-b2e0-4c89-96ef-287ca22173a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -15,
            "first": 86,
            "second": 8249
        },
        {
            "id": "4e6ef733-d890-48ba-b477-0c5fbd9c8cfd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -15,
            "first": 86,
            "second": 8250
        },
        {
            "id": "3e78c6ad-e4fc-48cb-b672-4e381ccedb76",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 6,
            "first": 86,
            "second": 8482
        },
        {
            "id": "1cdff6f6-78db-4708-a89d-43b42d1650ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -18,
            "first": 86,
            "second": 8710
        },
        {
            "id": "c22ab33b-6ad8-4a90-8cf1-d57e77e765a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 6,
            "first": 87,
            "second": 34
        },
        {
            "id": "93d9448f-c2e5-4afd-895c-351f19f40d04",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -13,
            "first": 87,
            "second": 38
        },
        {
            "id": "e1faef3b-0a97-47be-b286-eca6f6735291",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 6,
            "first": 87,
            "second": 39
        },
        {
            "id": "bdb45e51-0b9b-48fd-9eb6-cddd2e7df8e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 6,
            "first": 87,
            "second": 42
        },
        {
            "id": "c5b31897-ec56-4768-9303-c07e1b1a7a52",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -16,
            "first": 87,
            "second": 44
        },
        {
            "id": "0fbd6780-afc0-46da-85e1-6fe5d2138638",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 87,
            "second": 45
        },
        {
            "id": "04e35527-8c63-4d8a-8dfe-2e7fc860b86d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -16,
            "first": 87,
            "second": 46
        },
        {
            "id": "8f3a4c1a-5522-4fbf-8c1b-4ecce2c41cd8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -13,
            "first": 87,
            "second": 47
        },
        {
            "id": "b0e99d88-1f85-443e-9bae-1cf87f1f1837",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 5,
            "first": 87,
            "second": 63
        },
        {
            "id": "cdf7be21-a734-4076-be06-5712b2d44726",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -13,
            "first": 87,
            "second": 65
        },
        {
            "id": "b306956a-0a2f-49d3-a60b-a066759b88b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -14,
            "first": 87,
            "second": 74
        },
        {
            "id": "e729783f-397e-4b1a-bfe5-9170f95f3fcc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 87,
            "second": 97
        },
        {
            "id": "43eb5af3-81f1-4e25-9cf0-7a6a93b31e1f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 87,
            "second": 99
        },
        {
            "id": "c505361a-1f67-44ae-bd10-3fe9b58dc9a2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 87,
            "second": 100
        },
        {
            "id": "c1a77d15-1a03-4916-8d88-31852d535be2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 87,
            "second": 101
        },
        {
            "id": "32c5d2e4-2ac0-42f1-b747-384bc9d732e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -13,
            "first": 87,
            "second": 103
        },
        {
            "id": "676f71c7-f7c4-4f52-a1a7-a7cb2615980d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 87,
            "second": 111
        },
        {
            "id": "dc756b72-51a7-4916-9a8f-c2e8f3c8ebf7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 87,
            "second": 113
        },
        {
            "id": "d50a7526-6d7f-445f-a6db-8db189cac112",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 87,
            "second": 115
        },
        {
            "id": "bbadb66f-8ba7-48b5-925b-12be2f87cf32",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 6,
            "first": 87,
            "second": 170
        },
        {
            "id": "36c74590-1282-4536-b6d6-2dbec28ca1d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 87,
            "second": 171
        },
        {
            "id": "27ae6bdb-98fd-46fb-afc8-31d263e47b10",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 87,
            "second": 173
        },
        {
            "id": "eea78de7-2293-4633-b3d4-4ba61a34a87c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 6,
            "first": 87,
            "second": 176
        },
        {
            "id": "a31d3d5c-90dd-4b29-a34f-31c6ba557c26",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 6,
            "first": 87,
            "second": 178
        },
        {
            "id": "570eb777-92a8-4a8d-8cb3-e6a292139243",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 6,
            "first": 87,
            "second": 179
        },
        {
            "id": "faa8d712-0607-4749-b05a-72f44c89ca03",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 87,
            "second": 183
        },
        {
            "id": "c312e266-d7cc-4908-a431-bd265c243249",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 6,
            "first": 87,
            "second": 185
        },
        {
            "id": "fa656d0e-d177-470b-9530-285f935d614c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 6,
            "first": 87,
            "second": 186
        },
        {
            "id": "937a2558-105b-4289-b0a9-a8f3ee370de6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 87,
            "second": 187
        },
        {
            "id": "febd573a-7bda-4a70-a139-63c59c01f7f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -13,
            "first": 87,
            "second": 192
        },
        {
            "id": "d328099e-617e-437e-9358-cef51c814e0b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -13,
            "first": 87,
            "second": 193
        },
        {
            "id": "93069bf9-18c8-4e2d-bb75-5e53828a9c82",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -13,
            "first": 87,
            "second": 194
        },
        {
            "id": "24421880-0a53-44f2-b50a-1d99079918e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -13,
            "first": 87,
            "second": 195
        },
        {
            "id": "51c0cc29-464b-4b9a-98e5-0e778a06a0b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -13,
            "first": 87,
            "second": 196
        },
        {
            "id": "a19b9ee7-f57f-46ef-b306-27476213f955",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -13,
            "first": 87,
            "second": 197
        },
        {
            "id": "baa8d655-0d99-4985-bb96-5b44fbc7e473",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -13,
            "first": 87,
            "second": 198
        },
        {
            "id": "db36b285-64a0-437b-8e64-f128e6ca6cee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 87,
            "second": 224
        },
        {
            "id": "7365dbb5-971f-4b70-8614-32ad06bb4aa5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 87,
            "second": 225
        },
        {
            "id": "82c2a018-9a0c-40af-bb1e-5e4a4da961b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 87,
            "second": 226
        },
        {
            "id": "4971789d-59e6-4def-97b5-8a34e9525c3a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 87,
            "second": 227
        },
        {
            "id": "27c37d02-ae6e-4eae-8b00-f2ee6dfdf524",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 87,
            "second": 228
        },
        {
            "id": "39c2d3fd-bc33-4e9f-89b5-ba32290ac87e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 87,
            "second": 229
        },
        {
            "id": "3f92ae6e-747e-429b-a74c-c32eac3c9718",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 87,
            "second": 230
        },
        {
            "id": "e8465342-9038-42e4-bf3d-d5dc889c6c00",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 87,
            "second": 231
        },
        {
            "id": "f073e708-f10a-41ce-9060-5a274363fcfb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 87,
            "second": 232
        },
        {
            "id": "e0a262b5-a3cb-4373-a86b-bd275f41e8e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 87,
            "second": 233
        },
        {
            "id": "6178f2aa-7f51-42d3-90fa-878b8053e893",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 87,
            "second": 234
        },
        {
            "id": "1540e999-8fe5-4011-962d-4c11f5a7e50f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 87,
            "second": 235
        },
        {
            "id": "b37b06f2-3c31-4b6e-855f-c9917660c763",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 87,
            "second": 240
        },
        {
            "id": "b3ce0412-0660-4a4e-a963-436fa1d3271d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 87,
            "second": 242
        },
        {
            "id": "64e373af-83e3-4514-895b-7c83d6751591",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 87,
            "second": 243
        },
        {
            "id": "3ce0ccbf-ece2-4aff-8d0a-92b26195faea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 87,
            "second": 244
        },
        {
            "id": "0fcdfad3-c591-4fd8-9712-9a795db4b7e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 87,
            "second": 245
        },
        {
            "id": "6bd7bd88-34ec-4da5-bc8f-8b82dd45bafa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 87,
            "second": 246
        },
        {
            "id": "8ddcc5c7-4221-4c5a-91e6-f6aebe2caa4d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 87,
            "second": 248
        },
        {
            "id": "8c7a9379-61f3-4939-9421-b609d1fa8420",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -13,
            "first": 87,
            "second": 260
        },
        {
            "id": "55943441-b011-4daa-a60a-809b5d5d5024",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 87,
            "second": 261
        },
        {
            "id": "05c8874d-8147-42a9-95fd-7c753cd16019",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 87,
            "second": 263
        },
        {
            "id": "2547689d-8192-42eb-bafc-b3798e0c6c4a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 87,
            "second": 281
        },
        {
            "id": "b998ba7f-7b9b-42d2-a526-fa0c08da4970",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 87,
            "second": 339
        },
        {
            "id": "3530a829-3ceb-472b-a062-51dc43432248",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 87,
            "second": 8211
        },
        {
            "id": "f7205b6b-36fd-473f-9c39-bcce83204a36",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 87,
            "second": 8212
        },
        {
            "id": "a5bbf37a-f014-4947-bca5-052f0eefda34",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 6,
            "first": 87,
            "second": 8216
        },
        {
            "id": "b12719aa-2a88-4b39-8ac6-50855c74988c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 6,
            "first": 87,
            "second": 8217
        },
        {
            "id": "02881439-40e2-4493-86bb-fcdcbf9bbfae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -16,
            "first": 87,
            "second": 8218
        },
        {
            "id": "6650e890-c000-4049-bb16-291e0690e344",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 6,
            "first": 87,
            "second": 8220
        },
        {
            "id": "feab27af-316b-4225-a4ee-913b683ca0e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 6,
            "first": 87,
            "second": 8221
        },
        {
            "id": "dd7a7be1-b54b-499a-ba3f-843b0b13e881",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -16,
            "first": 87,
            "second": 8222
        },
        {
            "id": "743a631f-0be4-4483-8239-54b9d8ff8a1e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 87,
            "second": 8226
        },
        {
            "id": "84ca1ec0-322b-4f55-9b4a-5af5bdc244d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 87,
            "second": 8249
        },
        {
            "id": "dacc875e-c246-4e07-a939-7fbe72fefa54",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 87,
            "second": 8250
        },
        {
            "id": "37b05615-36a1-43a7-afab-d3f1d3b7d6f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 6,
            "first": 87,
            "second": 8482
        },
        {
            "id": "593b8877-4237-47a1-8244-e9381be74991",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -13,
            "first": 87,
            "second": 8710
        },
        {
            "id": "447d60a9-d4c0-4d3b-aa97-ab404c8cb200",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 88,
            "second": 45
        },
        {
            "id": "ed9daa23-eed8-4218-afa4-0aaa31b3658c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 88,
            "second": 64
        },
        {
            "id": "83703e7a-516e-418b-97b9-c32de1abac53",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 88,
            "second": 67
        },
        {
            "id": "33ab3f50-fc46-4e51-8323-3577fee843c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 88,
            "second": 71
        },
        {
            "id": "7762c89a-55cf-4137-ba09-e83cef5d27d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 88,
            "second": 79
        },
        {
            "id": "5c3ff843-138e-459e-bc6d-b87d9dc79610",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 88,
            "second": 81
        },
        {
            "id": "07a6907c-9412-431c-aba2-7a2808318687",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 88,
            "second": 99
        },
        {
            "id": "76468f93-f144-423e-a867-68eb469e3c45",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 88,
            "second": 100
        },
        {
            "id": "c78681ce-95af-41a9-a0d2-b250fd692a8f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 88,
            "second": 101
        },
        {
            "id": "bc359fd0-db89-44d5-90e1-b6f6762f7892",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 88,
            "second": 102
        },
        {
            "id": "0b9e80dd-4179-482e-8bd9-3f1a190edf85",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 88,
            "second": 111
        },
        {
            "id": "63cad1cc-4759-47af-acd4-33809df53a51",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 88,
            "second": 113
        },
        {
            "id": "3db72dae-6132-4d86-8d1c-50626b6762f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 88,
            "second": 116
        },
        {
            "id": "aed910bf-5865-4ddc-bf01-1114d918c98e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 88,
            "second": 118
        },
        {
            "id": "c42e0934-a86b-44c6-a3eb-8deda8c88f92",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 88,
            "second": 119
        },
        {
            "id": "b67a1b07-2d6e-4c31-adef-5be4fbc2bedd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 88,
            "second": 121
        },
        {
            "id": "1ff2fd7f-3385-48f4-a5bb-38c1d70e54ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 88,
            "second": 171
        },
        {
            "id": "f2c65df3-2115-41c5-a380-a4a925b287a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 88,
            "second": 173
        },
        {
            "id": "2a728672-206a-42fe-912a-06798bdf9f58",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 88,
            "second": 183
        },
        {
            "id": "3ec7747f-7c24-4070-80c3-55efb09d8490",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 88,
            "second": 187
        },
        {
            "id": "27e3390a-ecc9-4951-b44a-1ea8fcd9cbbc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 88,
            "second": 199
        },
        {
            "id": "50240cf6-4496-4260-85db-aaf4b76d8634",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 88,
            "second": 210
        },
        {
            "id": "8d953b90-f135-4fbc-abe2-a2a6ac3000f4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 88,
            "second": 211
        },
        {
            "id": "0df36e7d-8d61-4173-84aa-5fc0ab27a188",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 88,
            "second": 212
        },
        {
            "id": "c11432cc-cbcc-4355-b35a-ddc2468bcbbe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 88,
            "second": 213
        },
        {
            "id": "f97c4fc8-b9b7-42da-83b0-f5d7a355358a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 88,
            "second": 214
        },
        {
            "id": "6fb625c6-2793-49e0-b689-8159447f29be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 88,
            "second": 216
        },
        {
            "id": "fbb7b24d-cb25-4b07-8adc-51421d762781",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 88,
            "second": 231
        },
        {
            "id": "f6d0f290-13d6-4928-ba04-eeed11edaa60",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 88,
            "second": 232
        },
        {
            "id": "c1f54445-0bce-44d7-b577-ef3881c02f04",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 88,
            "second": 233
        },
        {
            "id": "0b68600c-089d-4040-acb6-961d38b783db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 88,
            "second": 234
        },
        {
            "id": "e3217271-2e66-47c3-8014-6de434af9714",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 88,
            "second": 235
        },
        {
            "id": "cd574af9-de7a-4d01-8f20-e0a5a590c1ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 88,
            "second": 240
        },
        {
            "id": "c3dec70a-06f3-473b-b5cb-db8869908673",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 88,
            "second": 242
        },
        {
            "id": "f22e7f6b-61b1-494a-ad8b-58a54956221c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 88,
            "second": 243
        },
        {
            "id": "6df036b2-cff6-46dd-9052-c959be3119c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 88,
            "second": 244
        },
        {
            "id": "0d9a8ad9-1458-4d1f-b77e-f7204a6967d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 88,
            "second": 245
        },
        {
            "id": "8cdc70f2-10b6-4cb4-ac90-ae79880d7e07",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 88,
            "second": 246
        },
        {
            "id": "dc679768-0bc6-4a15-a234-2905ec5122b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 88,
            "second": 248
        },
        {
            "id": "a2550b23-5080-4328-85eb-ef1facc0d07a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 88,
            "second": 262
        },
        {
            "id": "8ac8515d-50cc-435f-a731-814a799161ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 88,
            "second": 263
        },
        {
            "id": "03f964bf-0d52-444b-ba5b-e5bd3d70d319",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 88,
            "second": 281
        },
        {
            "id": "833cda93-4e22-4372-adb0-820c5c97e9fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 88,
            "second": 338
        },
        {
            "id": "7dbec674-6fbb-47ee-b151-0148a5199c35",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 88,
            "second": 339
        },
        {
            "id": "2c3891c0-53e9-4ffe-90f1-446d6a9faaa6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 88,
            "second": 8211
        },
        {
            "id": "f8679eb6-99b8-4bcd-b038-70ca08f88cd0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 88,
            "second": 8212
        },
        {
            "id": "10cbd8d1-c123-49f5-88cf-3bb26a896435",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 88,
            "second": 8226
        },
        {
            "id": "7894d8fc-7fa6-4d48-885b-9944513a7a52",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 88,
            "second": 8249
        },
        {
            "id": "cb592c8d-ec5a-43bf-ba45-4ffc2ff2ea0e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 88,
            "second": 8250
        },
        {
            "id": "0830cfaa-3bb2-4899-9662-bd5de35be8d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 4,
            "first": 89,
            "second": 34
        },
        {
            "id": "8cda7ff9-3c17-41ce-b341-3c98071c0f3b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -22,
            "first": 89,
            "second": 38
        },
        {
            "id": "9c63788b-c806-4b52-bdbc-36ab22cfaf7b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 4,
            "first": 89,
            "second": 39
        },
        {
            "id": "10b3917b-3b3b-4a09-a2e6-225d5865f594",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 4,
            "first": 89,
            "second": 42
        },
        {
            "id": "1f6d63d1-5580-43c8-bc20-4eed81c8d005",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -20,
            "first": 89,
            "second": 44
        },
        {
            "id": "b83c78da-47a6-4a51-ab44-4c2551b67695",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -21,
            "first": 89,
            "second": 45
        },
        {
            "id": "5aa6d821-96c5-4c11-9c90-558d745ca99d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -20,
            "first": 89,
            "second": 46
        },
        {
            "id": "c1e80e28-fea9-44b4-84de-5e51b05cc257",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -22,
            "first": 89,
            "second": 47
        },
        {
            "id": "8709fb5e-5798-44b6-8b22-9e1dde144843",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -16,
            "first": 89,
            "second": 58
        },
        {
            "id": "40a586a0-0dbd-433d-98e9-7ee6e5b355b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -16,
            "first": 89,
            "second": 59
        },
        {
            "id": "baab81e1-b103-4c2d-baaa-01b8466ff55e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 5,
            "first": 89,
            "second": 63
        },
        {
            "id": "3bc71b71-9f47-490d-bd6b-b1f5d2966181",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 89,
            "second": 64
        },
        {
            "id": "7a5033b4-a043-4342-9893-15c3ec453e6a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -22,
            "first": 89,
            "second": 65
        },
        {
            "id": "a1b8f097-f38b-42fa-942c-4ebf76ce04a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 89,
            "second": 67
        },
        {
            "id": "e2c7d982-7943-41af-8a5a-3b1938b77a60",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 89,
            "second": 71
        },
        {
            "id": "ef26657e-000b-4fd2-86be-afe95c4b7497",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -27,
            "first": 89,
            "second": 74
        },
        {
            "id": "89b7ccc8-a5f7-459d-8905-936eb4091d01",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 89,
            "second": 79
        },
        {
            "id": "ed0452d8-6fdc-45ab-9e03-7746d4e60f9c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 89,
            "second": 81
        },
        {
            "id": "87f1b872-7eeb-4178-9be1-72709717dc0a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -17,
            "first": 89,
            "second": 97
        },
        {
            "id": "927614af-af6a-4bc9-8515-3c3898a310f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -21,
            "first": 89,
            "second": 99
        },
        {
            "id": "5e16d312-1b8e-41d9-ad80-944a3f59ade1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -21,
            "first": 89,
            "second": 100
        },
        {
            "id": "b7ec3e08-0498-4b2a-85b1-e7c0f7c554c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -21,
            "first": 89,
            "second": 101
        },
        {
            "id": "398d0985-bb08-4676-b173-5f6c2b42cf32",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -23,
            "first": 89,
            "second": 103
        },
        {
            "id": "c4d52e7a-3830-4273-a9ac-312ca1995dc3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -16,
            "first": 89,
            "second": 109
        },
        {
            "id": "ad893db0-ad5f-4b12-b0bb-d31ca5792b0d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -16,
            "first": 89,
            "second": 110
        },
        {
            "id": "66350bec-006c-4cbd-9944-bb9745d83954",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -21,
            "first": 89,
            "second": 111
        },
        {
            "id": "6a82c991-98a2-4737-ae6a-d40b1415e373",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -16,
            "first": 89,
            "second": 112
        },
        {
            "id": "ec3bd771-a9c6-446d-ba34-da9560b3d288",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -21,
            "first": 89,
            "second": 113
        },
        {
            "id": "803fe417-5276-439d-a969-1cc3a957ae1a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -16,
            "first": 89,
            "second": 114
        },
        {
            "id": "607db977-e33c-4a65-8374-6cd233f452ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -17,
            "first": 89,
            "second": 115
        },
        {
            "id": "6e286b8a-f233-442d-955a-56b8bc5117a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -16,
            "first": 89,
            "second": 117
        },
        {
            "id": "7d27494b-20d8-44cd-8db5-a7715ead89b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -13,
            "first": 89,
            "second": 118
        },
        {
            "id": "bc6d4ec5-1c52-4bba-8cb5-d694976a5ec5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 89,
            "second": 119
        },
        {
            "id": "518356ab-4e7a-42d4-8613-00548d06229e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -18,
            "first": 89,
            "second": 120
        },
        {
            "id": "abd9bc37-9eba-4861-a1ba-ad1a6c3b91a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -13,
            "first": 89,
            "second": 121
        },
        {
            "id": "666d5687-4393-4215-9aba-d669ba70cfe3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 4,
            "first": 89,
            "second": 170
        },
        {
            "id": "f076c27b-1257-4db3-90a7-5484129811f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -21,
            "first": 89,
            "second": 171
        },
        {
            "id": "de56986a-cdbc-431e-b94f-5d908834717f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -21,
            "first": 89,
            "second": 173
        },
        {
            "id": "3f8ff35c-1e47-4e97-8388-3a7e3f1450b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 4,
            "first": 89,
            "second": 176
        },
        {
            "id": "2d9d4220-5739-41df-8620-096bf43d7e07",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 7,
            "first": 89,
            "second": 178
        },
        {
            "id": "ab1d2e0a-9bed-44f0-b6ab-9491e4330155",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 7,
            "first": 89,
            "second": 179
        },
        {
            "id": "54f1d477-5462-4035-958b-ed034f50c21c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -16,
            "first": 89,
            "second": 181
        },
        {
            "id": "61b2e99f-aab0-48e6-baf4-d00a963d0763",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -21,
            "first": 89,
            "second": 183
        },
        {
            "id": "951f94fb-4736-4720-a185-e99db1ed5715",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 7,
            "first": 89,
            "second": 185
        },
        {
            "id": "91d2e65c-35b7-41df-9865-f0825004031d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 4,
            "first": 89,
            "second": 186
        },
        {
            "id": "d0344fd0-267d-405a-afb2-3e909aecae87",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -21,
            "first": 89,
            "second": 187
        },
        {
            "id": "2a85b2e9-3fc6-48cf-99d7-cfbc0101c315",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -22,
            "first": 89,
            "second": 192
        },
        {
            "id": "3a33f2d2-18e1-43f1-917b-fddde2870287",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -22,
            "first": 89,
            "second": 193
        },
        {
            "id": "e1b52948-635c-424f-82b8-55bb97c6e804",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -22,
            "first": 89,
            "second": 194
        },
        {
            "id": "9e358b71-ea85-48af-84d4-935e14f1b15f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -22,
            "first": 89,
            "second": 195
        },
        {
            "id": "b36c7dd6-71e6-4002-8b3c-95228e7ef52c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -22,
            "first": 89,
            "second": 196
        },
        {
            "id": "87f2e0e9-2df0-4e51-8e15-c96cfde3bf7c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -22,
            "first": 89,
            "second": 197
        },
        {
            "id": "751851bf-4983-4314-b645-ca449d27f97e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -22,
            "first": 89,
            "second": 198
        },
        {
            "id": "b94b8e4e-eed5-4d28-b537-f0e693fa93a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 89,
            "second": 199
        },
        {
            "id": "5cda5f6a-9aba-49a4-a0e3-193692a36d91",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 89,
            "second": 210
        },
        {
            "id": "db16dcb3-6e91-4e8d-8daf-bedafc9ac44e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 89,
            "second": 211
        },
        {
            "id": "e912fb23-b91a-47bf-91d9-e810818b6575",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 89,
            "second": 212
        },
        {
            "id": "7a4eb5c1-e753-4481-a90d-c1661cda8d15",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 89,
            "second": 213
        },
        {
            "id": "895b8b88-5695-4c98-babf-cb51a29374ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 89,
            "second": 214
        },
        {
            "id": "67127fe6-3b0f-43b4-afba-d21317f67e13",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 89,
            "second": 216
        },
        {
            "id": "f22d9339-a32d-4a46-8cb8-864f02efb771",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -17,
            "first": 89,
            "second": 224
        },
        {
            "id": "ee31197d-42d7-4503-9173-5580d0508c73",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -17,
            "first": 89,
            "second": 225
        },
        {
            "id": "6517a6fa-77e0-4c27-bea5-76e1de1821b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -17,
            "first": 89,
            "second": 226
        },
        {
            "id": "0bc741c4-7f2a-43e0-8f2b-8ef5e4cd68a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -17,
            "first": 89,
            "second": 227
        },
        {
            "id": "8386b503-1a14-4315-a6a5-7254dd87b279",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -17,
            "first": 89,
            "second": 228
        },
        {
            "id": "add8ded5-5cf4-488e-8cf5-6147dba20b4e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -17,
            "first": 89,
            "second": 229
        },
        {
            "id": "f56cf8c2-9acb-41b5-af0a-acc2906fa325",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -17,
            "first": 89,
            "second": 230
        },
        {
            "id": "9374997d-8d51-44a9-a08d-7185b38f52d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -21,
            "first": 89,
            "second": 231
        },
        {
            "id": "82da9a21-5973-4b3c-98da-a377f9cb2c4a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -21,
            "first": 89,
            "second": 232
        },
        {
            "id": "b139e890-26e7-4dc1-ad5f-27c9919fb51c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -21,
            "first": 89,
            "second": 233
        },
        {
            "id": "ce862a02-d9c8-4263-8ec2-a93edb2851a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -21,
            "first": 89,
            "second": 234
        },
        {
            "id": "28efa609-1d54-4a5b-9ebe-a808c5306eaf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -21,
            "first": 89,
            "second": 235
        },
        {
            "id": "64c061de-e38f-4520-b41e-359e93a98dd5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -21,
            "first": 89,
            "second": 240
        },
        {
            "id": "577fc16c-7a6b-41ed-bd00-063aea4639f6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -16,
            "first": 89,
            "second": 241
        },
        {
            "id": "af3f1b4e-a29c-440c-bf43-a30ccc34452c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -21,
            "first": 89,
            "second": 242
        },
        {
            "id": "ad56bb56-63be-4cb2-8222-5e42f79b6b35",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -21,
            "first": 89,
            "second": 243
        },
        {
            "id": "8fe0cb11-dc2a-4fe7-a1de-1cb31ae62972",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -21,
            "first": 89,
            "second": 244
        },
        {
            "id": "fbd828ef-4e7f-43ed-b981-2b7497f2ad99",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -21,
            "first": 89,
            "second": 245
        },
        {
            "id": "9c3e988d-c9e3-4015-8e0b-0a2307c34a5e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -21,
            "first": 89,
            "second": 246
        },
        {
            "id": "dc2dbb77-b86e-48a6-a525-04c2a713f01d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -21,
            "first": 89,
            "second": 248
        },
        {
            "id": "ce1d5212-4a0c-424f-9c93-1e807243298a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -16,
            "first": 89,
            "second": 249
        },
        {
            "id": "b2b0f115-0818-4acc-9294-24c2805851e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -16,
            "first": 89,
            "second": 250
        },
        {
            "id": "978accf4-6bf3-4e6b-81fb-d5c7493a59aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -16,
            "first": 89,
            "second": 251
        },
        {
            "id": "e0c46277-b78b-4cb6-bcdf-d87336482fca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -16,
            "first": 89,
            "second": 252
        },
        {
            "id": "00e344fd-6e74-4806-b340-f14cd26d87f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -22,
            "first": 89,
            "second": 260
        },
        {
            "id": "c6e0b0ed-c5fb-4cdc-a331-4000972fa3f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -17,
            "first": 89,
            "second": 261
        },
        {
            "id": "294acca5-0de0-4132-80ff-4e644715276d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 89,
            "second": 262
        },
        {
            "id": "cf8ede13-8bc7-4087-ba01-f0d5c8d64068",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -21,
            "first": 89,
            "second": 263
        },
        {
            "id": "9854418b-fe42-44b5-aaf6-8edffc4ac3b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -21,
            "first": 89,
            "second": 281
        },
        {
            "id": "87b2b9d2-dde4-4fa5-b681-8d2d627d10df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -16,
            "first": 89,
            "second": 324
        },
        {
            "id": "d3b9ea13-cf4c-4a21-9a55-94d360f6aa07",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 89,
            "second": 338
        },
        {
            "id": "fc0ce7a8-c376-4fa1-b816-8db903e893da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -21,
            "first": 89,
            "second": 339
        },
        {
            "id": "9f8b59c2-aa5d-4b1a-9b95-87e8d934bfd2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -21,
            "first": 89,
            "second": 8211
        },
        {
            "id": "98b0dec6-23f2-4a5d-9228-99023ec110fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -21,
            "first": 89,
            "second": 8212
        },
        {
            "id": "cdd77609-c8f1-4583-aec2-b104da9f01f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 4,
            "first": 89,
            "second": 8216
        },
        {
            "id": "0989bba9-fdd0-47ff-ba89-7f26fc3db31e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 4,
            "first": 89,
            "second": 8217
        },
        {
            "id": "b9d9ef18-6b78-4ea8-a37b-693d3b6ea797",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -20,
            "first": 89,
            "second": 8218
        },
        {
            "id": "3d105c15-9c57-480b-9978-ac9bfe4d8b32",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 4,
            "first": 89,
            "second": 8220
        },
        {
            "id": "e8057015-d3df-4dc6-a0ca-bbb80a8395a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 4,
            "first": 89,
            "second": 8221
        },
        {
            "id": "cd6978e6-c53e-4054-ab58-63912e9d1bc2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -20,
            "first": 89,
            "second": 8222
        },
        {
            "id": "4dd1f246-d88e-4eb7-9f3d-5651d6b6b444",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -21,
            "first": 89,
            "second": 8226
        },
        {
            "id": "d4a43865-937b-428b-b1fd-d1d489054bd0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -21,
            "first": 89,
            "second": 8249
        },
        {
            "id": "83209078-7bbe-4ff7-9ee4-7f51104888c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -21,
            "first": 89,
            "second": 8250
        },
        {
            "id": "f3c8e46f-1dd7-46f7-9962-3efb9d2f699b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 4,
            "first": 89,
            "second": 8482
        },
        {
            "id": "ca3b07b4-f37b-4ae0-909b-390d00a6430b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -22,
            "first": 89,
            "second": 8710
        },
        {
            "id": "f11f8edd-15ea-4fee-a102-f6f7cda9872b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 90,
            "second": 45
        },
        {
            "id": "110a08a0-d4ba-4a7c-b8d0-b266b5cafd2c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 5,
            "first": 90,
            "second": 63
        },
        {
            "id": "94c7e403-a582-4a81-815f-6cff4f4aa1be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 90,
            "second": 64
        },
        {
            "id": "66526a88-4dd4-45ab-9986-db7fcdef7a46",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 90,
            "second": 67
        },
        {
            "id": "c262e2e8-0417-423e-b11e-4f77732b2167",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 90,
            "second": 71
        },
        {
            "id": "b269d712-47c2-43cf-8917-3f1e56505896",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 90,
            "second": 79
        },
        {
            "id": "f1a1619c-19ec-45e4-979b-61aeeda4cf8b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 90,
            "second": 81
        },
        {
            "id": "4508be63-af15-4431-ab9a-7e7ff3331cf4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 90,
            "second": 99
        },
        {
            "id": "96ee82e5-ce0f-460d-9fd0-4bc96aef9828",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 90,
            "second": 100
        },
        {
            "id": "af753c86-eaa2-4d19-b96c-14dbc460434d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 90,
            "second": 101
        },
        {
            "id": "bc5571e4-81f8-407e-af87-ba356140544a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 90,
            "second": 111
        },
        {
            "id": "aa2e8eb5-9b56-42fd-8cc8-fe84ccf13ed8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 90,
            "second": 113
        },
        {
            "id": "c0370ed1-1822-43b2-a2b8-f45d7c4f5c1b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 90,
            "second": 115
        },
        {
            "id": "b853688c-1ece-4167-b5e0-eb39cc040439",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 90,
            "second": 118
        },
        {
            "id": "ec4d638b-cca1-4eca-9a12-f1fa683ecf98",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 90,
            "second": 121
        },
        {
            "id": "673b94b5-30e3-4489-a00d-80ca6597e008",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 90,
            "second": 171
        },
        {
            "id": "cec9d64a-463f-4fec-a2b9-dc76488895be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 90,
            "second": 173
        },
        {
            "id": "006e4717-3be3-4095-b582-b79c464e148e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 90,
            "second": 183
        },
        {
            "id": "e606372f-c43e-45e9-92da-d1e8e7f7e4da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 90,
            "second": 187
        },
        {
            "id": "af19b8de-2f23-493a-9743-cbc828234cf6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 90,
            "second": 199
        },
        {
            "id": "5970135a-5997-47a2-93d8-0aafd41e9163",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 90,
            "second": 210
        },
        {
            "id": "10964c63-70a7-4f7c-8bc7-03cdffbedfed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 90,
            "second": 211
        },
        {
            "id": "d0583fb1-0f56-4554-ae9b-9cf86a3da16e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 90,
            "second": 212
        },
        {
            "id": "edfa4e7f-ce75-4a22-b5e1-31b879b0289e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 90,
            "second": 213
        },
        {
            "id": "b02addda-5f66-4df2-a207-015b8e16bdcf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 90,
            "second": 214
        },
        {
            "id": "162efea2-6d60-419d-bb06-e436f549bc51",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 90,
            "second": 216
        },
        {
            "id": "20d29111-8306-4839-8d72-145c2b78abcb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 90,
            "second": 231
        },
        {
            "id": "ddea6a80-2976-45ed-8a11-02f857d0bb98",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 90,
            "second": 232
        },
        {
            "id": "70ac05f5-21ef-4e99-90f8-d1877dea9e6a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 90,
            "second": 233
        },
        {
            "id": "446ff90e-6642-427c-99e7-c8c739868878",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 90,
            "second": 234
        },
        {
            "id": "1053d568-1ebd-4937-b2e6-1da73b607b3e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 90,
            "second": 235
        },
        {
            "id": "0e3a0d21-7553-424d-bfa6-206651bc2083",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 90,
            "second": 240
        },
        {
            "id": "d69f4d0b-27d9-4e5e-afbf-a49783448b56",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 90,
            "second": 242
        },
        {
            "id": "013ffcd5-8ef9-4cfc-80c2-c3d704c2ce8d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 90,
            "second": 243
        },
        {
            "id": "39840a96-2deb-40ad-b92f-942f1303ba4d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 90,
            "second": 244
        },
        {
            "id": "0ffaca58-2ffa-4047-840e-a2af3be44c26",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 90,
            "second": 245
        },
        {
            "id": "cc2aa19a-a322-4a7e-9ea7-9d24d692d3c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 90,
            "second": 246
        },
        {
            "id": "4a6e0e52-c934-4821-9900-327c84377171",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 90,
            "second": 248
        },
        {
            "id": "346e8062-a8a6-42c9-bfaa-4cc0b2a7d75b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 90,
            "second": 262
        },
        {
            "id": "a39210f2-5abb-4368-afc9-f5f254958d43",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 90,
            "second": 263
        },
        {
            "id": "fc93fa97-09f2-4101-a906-2732da816994",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 90,
            "second": 281
        },
        {
            "id": "d5fc3792-7717-4ed2-bb38-2de76f89b27d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 90,
            "second": 338
        },
        {
            "id": "f671abb4-d434-48a8-85f0-3e435c5cb612",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 90,
            "second": 339
        },
        {
            "id": "e5ec759d-f15e-47da-b1dc-76ba8c4ebf1a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 90,
            "second": 8211
        },
        {
            "id": "8a570a48-fdb2-42d5-b35c-d4ace6ad2038",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 90,
            "second": 8212
        },
        {
            "id": "72aca72a-2541-4c86-a5a6-01c892119996",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 90,
            "second": 8226
        },
        {
            "id": "6891ac42-d3a6-4a7e-9e00-4e62af9e2f31",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 90,
            "second": 8249
        },
        {
            "id": "b7aedca8-4e4d-4fbb-9270-9da7e100962e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 90,
            "second": 8250
        },
        {
            "id": "a8f8f404-9b36-4133-8023-1fca4f7af536",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 91,
            "second": 64
        },
        {
            "id": "d08c7b7c-d28a-4bee-97f5-5ccff27318af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 91,
            "second": 67
        },
        {
            "id": "e0315abc-fed3-4741-9fc9-2b41daa136fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 91,
            "second": 71
        },
        {
            "id": "3e109ffe-44a0-400e-972e-95b1d0ab1f67",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 91,
            "second": 79
        },
        {
            "id": "8c62a3ba-9cc5-4c35-b0ae-24dfe6ae9828",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 91,
            "second": 81
        },
        {
            "id": "2d98c254-8186-4b30-9821-bebeadd8867c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 91,
            "second": 99
        },
        {
            "id": "2c3eaa7a-da5b-42b2-bb69-46107e43cca1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 91,
            "second": 100
        },
        {
            "id": "6584073e-798e-4f68-a8f7-d068980014c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 91,
            "second": 101
        },
        {
            "id": "2197c330-1d69-40b8-87ac-e55ba1367638",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 91,
            "second": 111
        },
        {
            "id": "d7850329-6b40-4cee-b69c-eb7ce46ec8e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 91,
            "second": 113
        },
        {
            "id": "e3087e11-de59-4497-b2e1-7c25d08f143b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 91,
            "second": 199
        },
        {
            "id": "e96d84a6-a28f-403d-a84f-e5fbadf1a038",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 91,
            "second": 210
        },
        {
            "id": "ec2e0d19-d579-4fc9-9318-65160eac0903",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 91,
            "second": 211
        },
        {
            "id": "0754af2b-075b-432c-b796-72daf633806f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 91,
            "second": 212
        },
        {
            "id": "0b341e1b-19be-436f-814b-c966b15b38d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 91,
            "second": 213
        },
        {
            "id": "e018b6af-00db-47f9-8d32-42581547cccc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 91,
            "second": 214
        },
        {
            "id": "36acdfc2-b56d-40d7-b8f1-67a82d19aa94",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 91,
            "second": 216
        },
        {
            "id": "c235d2db-5cb5-4773-9c5f-05a005a69cf7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 91,
            "second": 231
        },
        {
            "id": "b44d2992-3af7-4c7c-884a-50ea1d77943d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 91,
            "second": 232
        },
        {
            "id": "930e3564-c89f-423c-bf5c-eaba32581c18",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 91,
            "second": 233
        },
        {
            "id": "fb593afe-77cc-407d-ad00-4b3e88dac4b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 91,
            "second": 234
        },
        {
            "id": "68044b90-1f97-4b11-a211-2db48c495b34",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 91,
            "second": 235
        },
        {
            "id": "aea0dced-8f6f-4a30-a741-2e913aec2e04",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 91,
            "second": 240
        },
        {
            "id": "73c816e2-58fe-470d-9a10-c5f3d0e8ca04",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 91,
            "second": 242
        },
        {
            "id": "77750181-e12a-4d18-9e39-50697c69eb8c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 91,
            "second": 243
        },
        {
            "id": "d231f524-64c1-444e-a666-c5521423c456",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 91,
            "second": 244
        },
        {
            "id": "a5aa8e21-f151-49f3-95e0-ecd2d7a03a83",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 91,
            "second": 245
        },
        {
            "id": "5c9f57dd-3ca2-4a70-8c10-26b5579c253b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 91,
            "second": 246
        },
        {
            "id": "0f26e00c-9283-4775-b02d-48faee71e1b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 91,
            "second": 248
        },
        {
            "id": "4efcd109-6ec3-4292-bfd1-e9b22441823f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 91,
            "second": 262
        },
        {
            "id": "c5c2e036-52bd-49bb-a0ee-d5d1671f6c39",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 91,
            "second": 263
        },
        {
            "id": "691989cb-fee7-41b2-ba83-9b854cf4afce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 91,
            "second": 281
        },
        {
            "id": "710e476d-8682-4b97-96ec-3ad2ea5dc56b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 91,
            "second": 338
        },
        {
            "id": "3d1524bd-7138-40f2-9f28-f2dc9bd420f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 91,
            "second": 339
        },
        {
            "id": "90f1e014-b4e6-466f-9bdf-27fe2bcb8ab7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -24,
            "first": 92,
            "second": 34
        },
        {
            "id": "c3dd2775-aeed-401d-8641-09812f78dd1d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -24,
            "first": 92,
            "second": 39
        },
        {
            "id": "31e3ccbb-6114-437d-84df-5fb7278798da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -24,
            "first": 92,
            "second": 42
        },
        {
            "id": "e5366acf-b60c-49b1-aa20-63f82874e73f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 92,
            "second": 45
        },
        {
            "id": "4da1cd2e-6539-4ee6-ad70-2705a7981128",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 92,
            "second": 63
        },
        {
            "id": "457f4292-800c-42a2-a7fd-6dd6a8453c49",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 92,
            "second": 64
        },
        {
            "id": "8c02149c-bbf3-4cc1-821d-96f748e539e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 92,
            "second": 67
        },
        {
            "id": "2a3fc5e8-4f18-4bb7-9362-5022eb3bb611",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 92,
            "second": 71
        },
        {
            "id": "8eed9dec-6e92-4cd8-a802-11ece2ca758f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 7,
            "first": 92,
            "second": 74
        },
        {
            "id": "47bf099b-2ff1-49b2-bcc0-f4c626f1f442",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 92,
            "second": 79
        },
        {
            "id": "138293b8-737e-4a11-a5df-855abbdace44",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 92,
            "second": 81
        },
        {
            "id": "29f5ac0c-f758-41ce-86d3-8d2b187dbe1c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -18,
            "first": 92,
            "second": 84
        },
        {
            "id": "affa92e8-1e55-4b23-9bf4-386ca920addb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 92,
            "second": 85
        },
        {
            "id": "50be9e98-fd9e-49e2-a323-947325cadb28",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -18,
            "first": 92,
            "second": 86
        },
        {
            "id": "da30b4cd-2bdf-4cf7-9ca8-366c9a805226",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 92,
            "second": 87
        },
        {
            "id": "3dbf6aff-4040-4983-940e-4a60b1df01e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -22,
            "first": 92,
            "second": 89
        },
        {
            "id": "bb1ecee0-5a25-4313-b774-5881fce4988f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -18,
            "first": 92,
            "second": 92
        },
        {
            "id": "3705ae98-ecdc-4aa4-b6c7-b818c2952bf3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 92,
            "second": 118
        },
        {
            "id": "9fb1b1af-a03c-4eea-8b54-4ee68895daa1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 92,
            "second": 121
        },
        {
            "id": "a025eeec-e879-4788-b309-77e202a709a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -24,
            "first": 92,
            "second": 170
        },
        {
            "id": "2a9afa64-33f4-4a5a-8e95-55da26e06eeb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 92,
            "second": 171
        },
        {
            "id": "31cb4045-ef2d-457d-811e-0b9550a08c4a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 92,
            "second": 173
        },
        {
            "id": "6659de7d-7ca9-48c9-b163-4dbd3a43de1b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -24,
            "first": 92,
            "second": 176
        },
        {
            "id": "019d93b7-bf7a-454d-97bd-3d9942744768",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -25,
            "first": 92,
            "second": 178
        },
        {
            "id": "a5c43efe-9adf-4630-9f78-a431ad25d057",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -25,
            "first": 92,
            "second": 179
        },
        {
            "id": "fc7a3f5f-9d47-429c-a89b-37fb9c5c324d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 92,
            "second": 183
        },
        {
            "id": "3801530f-2b80-446c-a472-57b0124f3c77",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -25,
            "first": 92,
            "second": 185
        },
        {
            "id": "d11d47f8-5eb2-4313-87e7-dae2678fbf0b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -24,
            "first": 92,
            "second": 186
        },
        {
            "id": "ceb8c56b-6398-4ef9-9a95-79ca2de868cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 92,
            "second": 187
        },
        {
            "id": "2beb927c-8245-4d01-bd11-49f130863fbd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 92,
            "second": 199
        },
        {
            "id": "62c37e0b-9077-4f0c-885c-3afbeb5c3874",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 92,
            "second": 210
        },
        {
            "id": "a91074f2-94bf-406e-a84d-2c4e2930557b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 92,
            "second": 211
        },
        {
            "id": "2147a322-0526-4e17-ad10-fafbcba72d32",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 92,
            "second": 212
        },
        {
            "id": "6dc7b478-2e59-4e8c-ac6c-2ef3f2379f1d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 92,
            "second": 213
        },
        {
            "id": "02cf6b2e-b47f-4529-9497-ca10c6a9aef6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 92,
            "second": 214
        },
        {
            "id": "f4be3a96-7964-4421-af93-01b207677b82",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 92,
            "second": 216
        },
        {
            "id": "3c41c0e6-1346-40a4-a138-a50c6000cb3c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 92,
            "second": 217
        },
        {
            "id": "304db328-a19b-4d39-b814-10606c7e4b5c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 92,
            "second": 218
        },
        {
            "id": "2415ca25-345f-4519-b8d5-d3addcccb60e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 92,
            "second": 219
        },
        {
            "id": "efde036f-8bc4-4385-ac3d-74bcb4776ff0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 92,
            "second": 220
        },
        {
            "id": "347ccb11-1444-491e-b97e-e85e1a84e8be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -22,
            "first": 92,
            "second": 221
        },
        {
            "id": "3e641e83-c814-47f7-8687-bad68afb9e0f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 92,
            "second": 262
        },
        {
            "id": "779292c3-d4ee-46d1-aeec-9da28f707c9f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 92,
            "second": 338
        },
        {
            "id": "eaff7b12-b11d-458d-b6ac-eb4fe49d403d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -22,
            "first": 92,
            "second": 376
        },
        {
            "id": "029a58fd-1245-429b-a7c1-a7974bc316e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 92,
            "second": 8211
        },
        {
            "id": "df76b371-b487-4ec6-bbe9-8621b7849976",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 92,
            "second": 8212
        },
        {
            "id": "6f97784d-ae22-4e99-a33e-afb36dc112aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -24,
            "first": 92,
            "second": 8216
        },
        {
            "id": "b608094f-c3e9-4d6a-b7ea-32af0ddfe0ed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -24,
            "first": 92,
            "second": 8217
        },
        {
            "id": "cec5010b-e0ea-4816-a812-d2ed0c53c7dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -24,
            "first": 92,
            "second": 8220
        },
        {
            "id": "1ece249f-5ef8-4ab1-923d-3c731a4f6de6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -24,
            "first": 92,
            "second": 8221
        },
        {
            "id": "a1ef5754-db79-4bb9-917c-5b18e63cd16d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 92,
            "second": 8226
        },
        {
            "id": "b1e7230a-f290-4983-9b1a-bbaeed34a411",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 92,
            "second": 8249
        },
        {
            "id": "2a2475a3-c307-4f96-a28a-0f5f127b9ad2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 92,
            "second": 8250
        },
        {
            "id": "11639b72-87a0-4131-8447-5e31581f1e25",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -24,
            "first": 92,
            "second": 8482
        },
        {
            "id": "d68e757b-0524-409c-a3e0-fd96d0787e3c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -10,
            "first": 97,
            "second": 34
        },
        {
            "id": "a942b4b2-3cf9-4b07-b3f9-85d88b3cb000",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -10,
            "first": 97,
            "second": 39
        },
        {
            "id": "24c4cb3a-2f9c-4aad-b23c-514332d12b3e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -10,
            "first": 97,
            "second": 42
        },
        {
            "id": "392cc46c-5ef5-4882-b8a5-f76c98fac326",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 97,
            "second": 118
        },
        {
            "id": "d8ad2db3-6fa6-4ede-81cc-1dbe3d2dbaff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 97,
            "second": 119
        },
        {
            "id": "5461d76a-b4df-490d-b033-c9258ec33994",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 97,
            "second": 121
        },
        {
            "id": "07dbba00-7905-4e4f-a788-362e6c281c99",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -10,
            "first": 97,
            "second": 170
        },
        {
            "id": "b4510242-aa7b-4255-adf1-c95cae50e6d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -10,
            "first": 97,
            "second": 176
        },
        {
            "id": "6e0d4b5e-28e2-4d15-814f-fecf5cf253f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -10,
            "first": 97,
            "second": 178
        },
        {
            "id": "5cc3c4c9-cb40-45e5-b60e-1b1e74211ac9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -10,
            "first": 97,
            "second": 179
        },
        {
            "id": "43099556-40b9-4e4c-8dbc-f4dd8cde7050",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -10,
            "first": 97,
            "second": 185
        },
        {
            "id": "c628fb32-a88a-4a85-a98f-a374522ea676",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -10,
            "first": 97,
            "second": 186
        },
        {
            "id": "73189699-1b5d-4142-8e1f-908d988e3336",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -10,
            "first": 97,
            "second": 8216
        },
        {
            "id": "5699ea64-1e0a-41ef-abd0-07609809ebc6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -10,
            "first": 97,
            "second": 8217
        },
        {
            "id": "4d1fefec-d5fd-431e-aa88-3798f40374a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -10,
            "first": 97,
            "second": 8220
        },
        {
            "id": "545dcfe8-bf6f-4913-bd36-77222f7445cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -10,
            "first": 97,
            "second": 8221
        },
        {
            "id": "d535b67f-7257-4403-a914-4e82bcb36983",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -10,
            "first": 97,
            "second": 8482
        },
        {
            "id": "7024c221-d829-4172-807e-5f54f81229a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 98,
            "second": 34
        },
        {
            "id": "1aacf14a-7af9-46c6-8083-29732b5d58f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 98,
            "second": 39
        },
        {
            "id": "6b34ec30-c435-4594-bb62-4eb18233e52d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 98,
            "second": 41
        },
        {
            "id": "66dcc8b1-fef2-4eae-9c82-5a1a83951018",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 98,
            "second": 42
        },
        {
            "id": "21699882-0939-4e43-8486-3e7a8fed4e4f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -15,
            "first": 98,
            "second": 86
        },
        {
            "id": "078d97f6-e956-4115-8892-ca7f9b592d05",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 98,
            "second": 87
        },
        {
            "id": "cf662974-ffc5-4339-b759-e47227a603ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -15,
            "first": 98,
            "second": 92
        },
        {
            "id": "c3020d90-bd55-4a3c-a5d0-f56fea2fc1e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 98,
            "second": 93
        },
        {
            "id": "2e34e700-964a-41da-8a89-56271e4cb97c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 98,
            "second": 118
        },
        {
            "id": "2175dfba-af94-4e59-baac-f919fd578259",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 98,
            "second": 120
        },
        {
            "id": "54d03f0a-6ded-4395-833a-d513af24ede9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 98,
            "second": 121
        },
        {
            "id": "f57d2a58-b1f2-43a1-b791-17945019f3d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 98,
            "second": 125
        },
        {
            "id": "cc45f890-ee4b-40ef-9b98-e07143105347",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 98,
            "second": 170
        },
        {
            "id": "d96da0d5-7e32-4ce7-9e61-8a6dd018e227",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 98,
            "second": 176
        },
        {
            "id": "85d54bd8-344d-489b-8424-bafcc0cc6b08",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 98,
            "second": 186
        },
        {
            "id": "58ed4f6f-7f19-4a82-82e0-4018edcaadd5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 98,
            "second": 8216
        },
        {
            "id": "c28ff7d9-5ea0-45d3-bf0f-265bc479ef8c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 98,
            "second": 8217
        },
        {
            "id": "18c6dce9-11d2-4ca1-a1f6-10365fbdc902",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 98,
            "second": 8220
        },
        {
            "id": "d7943ac4-03f2-4b7e-b174-0f512cd472cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 98,
            "second": 8221
        },
        {
            "id": "00f2b821-72b8-4df0-a6b5-2a8281b59895",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 98,
            "second": 8482
        },
        {
            "id": "1d148875-6199-4914-87a2-c350bed0ed53",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 101,
            "second": 34
        },
        {
            "id": "c336a15f-69c5-42b5-8cbf-cd3bcf67b874",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 101,
            "second": 39
        },
        {
            "id": "8ebd7706-acf6-40fe-9a81-912116737884",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 101,
            "second": 41
        },
        {
            "id": "0f9c359d-b4af-43db-bd02-efcf6c1a771c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 101,
            "second": 42
        },
        {
            "id": "cfb17f16-7b17-45a1-b859-2e3351107a63",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -15,
            "first": 101,
            "second": 86
        },
        {
            "id": "8469c55d-91a6-4a04-ab60-6617faeec77c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 101,
            "second": 87
        },
        {
            "id": "d74c7ccb-6d81-4e57-93c2-2b2fa7fdd46f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -15,
            "first": 101,
            "second": 92
        },
        {
            "id": "71d2b39d-eb6b-4e34-a461-5abdb0ee18cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 101,
            "second": 93
        },
        {
            "id": "983c2654-9d85-4faf-9009-a606ab6a89b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 101,
            "second": 118
        },
        {
            "id": "ae8d467d-3032-469b-a2b7-ba284f2da2fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 101,
            "second": 120
        },
        {
            "id": "78d74981-858a-47a7-8c5c-7adb6e8d858c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 101,
            "second": 121
        },
        {
            "id": "1fee5c9c-d039-4c01-b151-62d174b4015e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 101,
            "second": 125
        },
        {
            "id": "defe0179-7e78-4ae3-b7cd-1d362ad21b13",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 101,
            "second": 170
        },
        {
            "id": "582d83ad-03a3-421b-a427-976d2c34d248",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 101,
            "second": 176
        },
        {
            "id": "a3706d38-a0b0-46bf-bb6c-4fe2bde5cd7f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 101,
            "second": 186
        },
        {
            "id": "81aa2e68-0ff5-4dcb-a0d8-fe209170ce23",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 101,
            "second": 8216
        },
        {
            "id": "52972927-d150-4dbc-8181-709c521a753b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 101,
            "second": 8217
        },
        {
            "id": "feca36f7-842b-40bc-aa24-7ab81694f6f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 101,
            "second": 8220
        },
        {
            "id": "da932118-78b9-4cab-a6a4-4f057c856124",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 101,
            "second": 8221
        },
        {
            "id": "689b81e7-36ea-4004-afda-5ee344b2a055",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 101,
            "second": 8482
        },
        {
            "id": "6b46a9f9-ee7f-4479-98c0-a606a3212fe6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 9,
            "first": 102,
            "second": 34
        },
        {
            "id": "689c91a6-ebfd-4476-949f-de3758621ecc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 9,
            "first": 102,
            "second": 39
        },
        {
            "id": "577939b4-fc2d-45b7-a366-5ee34420f8ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 9,
            "first": 102,
            "second": 42
        },
        {
            "id": "6e07a53e-2fd6-47a2-9c5f-3a7a1fbf139c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -17,
            "first": 102,
            "second": 44
        },
        {
            "id": "28473da4-f6f8-4918-bca7-7d2c2dff4cc9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -17,
            "first": 102,
            "second": 46
        },
        {
            "id": "11a0d544-e012-4587-9ee2-4f17545b95c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 9,
            "first": 102,
            "second": 170
        },
        {
            "id": "b5a82837-cf5e-4d88-b73f-69c9420861eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 9,
            "first": 102,
            "second": 176
        },
        {
            "id": "0dd81cac-dde2-4a39-930d-1b71a9b2a389",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 13,
            "first": 102,
            "second": 178
        },
        {
            "id": "59464aea-77f6-4494-b936-e2abc0e37740",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 13,
            "first": 102,
            "second": 179
        },
        {
            "id": "59624310-6801-4653-8ae1-af8e75f75e2d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 13,
            "first": 102,
            "second": 185
        },
        {
            "id": "862869f3-dca0-43a7-b475-ac9908ff7238",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 9,
            "first": 102,
            "second": 186
        },
        {
            "id": "91677143-58ca-4a48-93c7-6c7db72d2f59",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 9,
            "first": 102,
            "second": 8216
        },
        {
            "id": "0825ffbd-2c46-4f97-b127-a4c1f7a1d415",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 9,
            "first": 102,
            "second": 8217
        },
        {
            "id": "e073ef54-b1de-4247-9f7a-de100fd3d492",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -17,
            "first": 102,
            "second": 8218
        },
        {
            "id": "e3b49211-a0af-4003-b2cc-158943e6739f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 9,
            "first": 102,
            "second": 8220
        },
        {
            "id": "bd492c1b-43ea-4649-8606-08bf7fd87a97",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 9,
            "first": 102,
            "second": 8221
        },
        {
            "id": "b1a3d4c1-80aa-4fb0-8314-299ed0134924",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -17,
            "first": 102,
            "second": 8222
        },
        {
            "id": "9b711e6c-359d-4819-8708-1ce28cc9c56d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 9,
            "first": 102,
            "second": 8482
        },
        {
            "id": "9d6d182b-15a7-4be3-b8fe-b09864d75f4c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -10,
            "first": 104,
            "second": 34
        },
        {
            "id": "ea933b65-8079-4b8c-8545-df8627618913",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -10,
            "first": 104,
            "second": 39
        },
        {
            "id": "17154b27-fe1f-48bc-8738-205041e6e7b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -10,
            "first": 104,
            "second": 42
        },
        {
            "id": "54565008-5b93-4639-b853-8c8cd4a35e5c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 104,
            "second": 118
        },
        {
            "id": "a72b6e24-b38e-44ca-ae85-e8d57cf6ed4f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 104,
            "second": 119
        },
        {
            "id": "6a852eef-3862-4dbd-bfdc-3984ed5aa09c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 104,
            "second": 121
        },
        {
            "id": "658fad1c-fc95-481b-bac3-27ce6dfca6df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -10,
            "first": 104,
            "second": 170
        },
        {
            "id": "33618036-8567-42c1-846f-6ee6e1f5fb7f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -10,
            "first": 104,
            "second": 176
        },
        {
            "id": "1045405f-bc8f-4a7b-afa0-483012b5cb1a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -10,
            "first": 104,
            "second": 178
        },
        {
            "id": "02ac408a-3ae1-45d6-a040-9d925bee3d5f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -10,
            "first": 104,
            "second": 179
        },
        {
            "id": "a20710a4-92ff-4d20-a66d-908e0bfb04b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -10,
            "first": 104,
            "second": 185
        },
        {
            "id": "903eec96-fa95-491c-a2ee-aa05565119af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -10,
            "first": 104,
            "second": 186
        },
        {
            "id": "e676f1a7-c5c7-4aae-b469-e5c2c5ccc508",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -10,
            "first": 104,
            "second": 8216
        },
        {
            "id": "023f027c-e296-4274-a698-9b4a920a6de0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -10,
            "first": 104,
            "second": 8217
        },
        {
            "id": "89304ba1-eb93-4a0e-82ed-e3cdef34aeb8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -10,
            "first": 104,
            "second": 8220
        },
        {
            "id": "c127b0b9-beb5-4b35-a428-f819dd0c6d13",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -10,
            "first": 104,
            "second": 8221
        },
        {
            "id": "77bc01bb-78a4-4794-8e3c-78f4246f66c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -10,
            "first": 104,
            "second": 8482
        },
        {
            "id": "04b1a950-dac9-48b8-91a9-ae57fc275ccc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 107,
            "second": 99
        },
        {
            "id": "9221239f-181f-445d-9e7c-55df94049a5b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 107,
            "second": 100
        },
        {
            "id": "df2f87ab-4151-439d-aa2d-9e037965b6fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 107,
            "second": 101
        },
        {
            "id": "344162c5-8833-4ac9-a116-4d6b8e55cb5f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 107,
            "second": 111
        },
        {
            "id": "dbbef8c9-6199-4387-a15c-823cffc93e07",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 107,
            "second": 113
        },
        {
            "id": "c082a2ae-cd00-4563-99e3-75b9749355a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 107,
            "second": 231
        },
        {
            "id": "4e94327a-cd86-4f55-a64b-866b71d715a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 107,
            "second": 232
        },
        {
            "id": "661ed0f5-c408-4c45-8421-f54d32fa5ecf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 107,
            "second": 233
        },
        {
            "id": "04598699-a23d-4ad9-a4b8-6b168c683f16",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 107,
            "second": 234
        },
        {
            "id": "e24f3395-bc4f-49d1-917f-641a0d6f3021",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 107,
            "second": 235
        },
        {
            "id": "2ce1f8c2-619b-4004-a8c8-ee77fb53bc08",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 107,
            "second": 240
        },
        {
            "id": "27d95e21-9ce1-4e6c-a070-4899a1ba87f4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 107,
            "second": 242
        },
        {
            "id": "26437593-43ed-4607-98f7-fccf8c22a5df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 107,
            "second": 243
        },
        {
            "id": "1838f72a-5593-4197-93a3-173771ae427d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 107,
            "second": 244
        },
        {
            "id": "1ddd27b5-9a55-4227-9ceb-5b062a8ba0b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 107,
            "second": 245
        },
        {
            "id": "d76ff60f-ff77-4a8b-9da1-8382dc2077ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 107,
            "second": 246
        },
        {
            "id": "1626ae52-060f-43bf-a55e-9d637eb0735c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 107,
            "second": 248
        },
        {
            "id": "f29a7289-c351-4c42-935a-3f0ac3d1c026",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 107,
            "second": 263
        },
        {
            "id": "51f397e7-66c3-445e-bc12-ef6a238edd9e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 107,
            "second": 281
        },
        {
            "id": "729af329-309a-49c3-9c9f-9161dd99b16b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 107,
            "second": 339
        },
        {
            "id": "1bd77f7a-3b22-4a24-bfd9-b735d5672c7b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -10,
            "first": 109,
            "second": 34
        },
        {
            "id": "98c551ae-6db8-413f-a428-c17ed345bf45",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -10,
            "first": 109,
            "second": 39
        },
        {
            "id": "6c80f2f5-252d-4a19-ab31-80bf932662fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -10,
            "first": 109,
            "second": 42
        },
        {
            "id": "bc2e0f87-b496-4697-a1c5-187385a21a37",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 109,
            "second": 118
        },
        {
            "id": "1713c454-0ff9-49c4-b184-5502b03a6be3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 109,
            "second": 119
        },
        {
            "id": "982b48eb-85f2-457a-a43f-4071b2230553",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 109,
            "second": 121
        },
        {
            "id": "803a5519-ead6-40e1-8296-77a2e0d53143",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -10,
            "first": 109,
            "second": 170
        },
        {
            "id": "46005b5d-da6d-4c2d-9635-e95060a16378",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -10,
            "first": 109,
            "second": 176
        },
        {
            "id": "ecac835b-c535-4fdf-94a4-126f3c91a94d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -10,
            "first": 109,
            "second": 178
        },
        {
            "id": "f8e932d6-782f-4814-a74e-4b0c53117b14",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -10,
            "first": 109,
            "second": 179
        },
        {
            "id": "989d8d7d-2bb3-4bc5-8808-bdef918bdc86",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -10,
            "first": 109,
            "second": 185
        },
        {
            "id": "89858127-0df2-42cc-9b96-f9146c13aecc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -10,
            "first": 109,
            "second": 186
        },
        {
            "id": "696dc434-783f-4ad4-8ff9-75ab55fdd07a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -10,
            "first": 109,
            "second": 8216
        },
        {
            "id": "e3178b26-33a9-4abd-a3d8-62e3b87abefc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -10,
            "first": 109,
            "second": 8217
        },
        {
            "id": "674e9a9b-54fa-41fb-8e77-4db6dc336a22",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -10,
            "first": 109,
            "second": 8220
        },
        {
            "id": "fd6c1a9c-976f-43f2-b73f-ff8577c494d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -10,
            "first": 109,
            "second": 8221
        },
        {
            "id": "8bc4ddfc-9620-4830-b199-7ac8e6feddad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -10,
            "first": 109,
            "second": 8482
        },
        {
            "id": "0e42a06d-2e08-44e8-94af-feeb3f0b6e75",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -10,
            "first": 110,
            "second": 34
        },
        {
            "id": "f2c03ef9-7a10-4c61-bf7b-df029f3441bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -10,
            "first": 110,
            "second": 39
        },
        {
            "id": "2d10ddcd-3409-42c7-a424-c8cee31b9e81",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -10,
            "first": 110,
            "second": 42
        },
        {
            "id": "089609da-8795-42a3-9101-0fa9ba69bc22",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 110,
            "second": 118
        },
        {
            "id": "e9e82f52-f914-4fab-bbe3-913f86419815",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 110,
            "second": 119
        },
        {
            "id": "81efa52b-cdd3-45d5-b266-e9cfeb132291",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 110,
            "second": 121
        },
        {
            "id": "6eb933fe-7457-4e97-96a2-9da9ac65e4c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -10,
            "first": 110,
            "second": 170
        },
        {
            "id": "6700b476-fcbf-4a95-9d86-da198dc30f9a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -10,
            "first": 110,
            "second": 176
        },
        {
            "id": "6c2bae9f-cc29-422d-a66d-cff07019b194",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -10,
            "first": 110,
            "second": 178
        },
        {
            "id": "3f52d7f2-4414-47cc-a176-45d924ed426c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -10,
            "first": 110,
            "second": 179
        },
        {
            "id": "7f8038c4-ac01-40e2-b536-fecc8b3fabd4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -10,
            "first": 110,
            "second": 185
        },
        {
            "id": "1c1313d3-eaf6-4b9f-bf9c-a8beaea0db06",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -10,
            "first": 110,
            "second": 186
        },
        {
            "id": "936d7270-5d8a-45e2-9067-09ca0245f5e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -10,
            "first": 110,
            "second": 8216
        },
        {
            "id": "ca227b68-3ed2-4e75-9060-84fe84016e10",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -10,
            "first": 110,
            "second": 8217
        },
        {
            "id": "e3e509a6-4972-45b8-a95a-4189f756b27f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -10,
            "first": 110,
            "second": 8220
        },
        {
            "id": "a2169ee2-72bb-4422-bb23-b8c173bc4cbc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -10,
            "first": 110,
            "second": 8221
        },
        {
            "id": "73b035a4-6e31-443a-9bd8-f4b2755c6ed7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -10,
            "first": 110,
            "second": 8482
        },
        {
            "id": "a15c40d2-60a8-4373-8144-fccd44779840",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 111,
            "second": 34
        },
        {
            "id": "4268b00e-80fb-49d8-bbc6-b082db06f7bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 111,
            "second": 39
        },
        {
            "id": "cedd9279-de6e-40e1-8643-7a2fffc1602c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 111,
            "second": 41
        },
        {
            "id": "3e226904-e7fb-4535-b3fb-aa6e8fd7cf86",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 111,
            "second": 42
        },
        {
            "id": "1b6705dc-84d6-453c-b397-fd34c0104f4a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -15,
            "first": 111,
            "second": 86
        },
        {
            "id": "e58ff4eb-29a2-442d-86cf-ef06d1c58af2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 111,
            "second": 87
        },
        {
            "id": "57471fff-8797-4d3c-bca8-8a2eb803d754",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -15,
            "first": 111,
            "second": 92
        },
        {
            "id": "9bd30e27-92fa-4db1-9b32-45316e28efc0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 111,
            "second": 93
        },
        {
            "id": "0aeef8a8-e122-48ba-89fb-f082cb1279c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 111,
            "second": 118
        },
        {
            "id": "223b078b-e00e-4e29-a902-a1b63955b2bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 111,
            "second": 120
        },
        {
            "id": "8a29d82a-e12d-435e-96d5-4bb4d7b4953b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 111,
            "second": 121
        },
        {
            "id": "a90e5aed-7632-4b17-9655-9beecd316ad7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 111,
            "second": 125
        },
        {
            "id": "1624ebd7-8f82-4f9b-98dc-a88ff33ecca0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 111,
            "second": 170
        },
        {
            "id": "aba9a839-cc03-41b3-bfb2-40643edbd994",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 111,
            "second": 176
        },
        {
            "id": "d6017660-8c7c-4002-8d5f-04c96dca4b9e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 111,
            "second": 186
        },
        {
            "id": "d78b837b-b6ce-4c75-85fd-746af6db069b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 111,
            "second": 8216
        },
        {
            "id": "d4b714bb-4f60-4d8e-b759-d60e49d8ba74",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 111,
            "second": 8217
        },
        {
            "id": "e5f9ede4-de5d-4f2c-a9b1-6cdf11c90a15",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 111,
            "second": 8220
        },
        {
            "id": "e89de941-351a-47e8-8016-792fda14f677",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 111,
            "second": 8221
        },
        {
            "id": "f366cc9b-8233-49ce-ad2c-858f06f33fbb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 111,
            "second": 8482
        },
        {
            "id": "dd10c6f2-711c-4474-958a-de5165cc7567",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 112,
            "second": 34
        },
        {
            "id": "68585bc0-13a5-4b56-9e19-0b87882bdabd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 112,
            "second": 39
        },
        {
            "id": "11cbe212-cafd-442a-8b66-105d87a5a858",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 112,
            "second": 41
        },
        {
            "id": "1d7c3880-13a3-46ca-8080-a3c3fbd6eaa2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 112,
            "second": 42
        },
        {
            "id": "def3629e-8d5b-41a7-9786-fd42170cd480",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -15,
            "first": 112,
            "second": 86
        },
        {
            "id": "c873c600-14d5-4f59-9dea-ee25988682b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 112,
            "second": 87
        },
        {
            "id": "fb300126-ecb6-409e-a180-ef61eb6865d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -15,
            "first": 112,
            "second": 92
        },
        {
            "id": "f996c8b6-2f9e-4049-9468-149fcd76f860",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 112,
            "second": 93
        },
        {
            "id": "30886fd9-1959-438d-a3d6-97314422185c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 112,
            "second": 118
        },
        {
            "id": "a03c9895-756c-4183-bef0-ef407a8edbd6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 112,
            "second": 120
        },
        {
            "id": "54edb01b-8fb6-4772-b45a-e71f3d425486",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 112,
            "second": 121
        },
        {
            "id": "75e547b7-8b2e-4bd6-994f-648617f12f74",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 112,
            "second": 125
        },
        {
            "id": "e9f28cdd-8391-4a54-8338-754e2b402b5b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 112,
            "second": 170
        },
        {
            "id": "817c47de-39e6-49e5-a595-e076f10f845c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 112,
            "second": 176
        },
        {
            "id": "fcca2c8f-677f-4fd4-96c2-cea455c21f74",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 112,
            "second": 186
        },
        {
            "id": "f6f0a5dc-f756-42ff-90a2-0ae8f039ea8a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 112,
            "second": 8216
        },
        {
            "id": "52403aae-0e3f-4651-8ea3-1796bfeed496",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 112,
            "second": 8217
        },
        {
            "id": "b977c70c-e755-454c-993b-0ffd061eb3d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 112,
            "second": 8220
        },
        {
            "id": "c0626f33-8439-40d0-92aa-14be4ec77e4b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 112,
            "second": 8221
        },
        {
            "id": "cbdf72b0-c538-4d89-807e-72370a666190",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 112,
            "second": 8482
        },
        {
            "id": "15513846-15ad-4e7b-8ec8-c387b9b0d5e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -18,
            "first": 114,
            "second": 44
        },
        {
            "id": "46ad2b19-3622-4ae2-9fd4-edf2a2035934",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -18,
            "first": 114,
            "second": 46
        },
        {
            "id": "bfa6cfb5-0296-4050-8664-2d1fd480f025",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 114,
            "second": 97
        },
        {
            "id": "5307eaf1-1c23-46ab-96ef-682a8fe59030",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 114,
            "second": 224
        },
        {
            "id": "d5e8d764-7438-48b7-b75c-1b38a70def56",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 114,
            "second": 225
        },
        {
            "id": "bf185a81-a6d1-40eb-8eaf-e1bd96352e3d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 114,
            "second": 226
        },
        {
            "id": "a561dee0-272e-49fa-bfc4-9ededfaeb33f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 114,
            "second": 227
        },
        {
            "id": "24ec2104-94c9-4a32-b1d1-58f5b1ef489a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 114,
            "second": 228
        },
        {
            "id": "ccc80377-a0fc-49cd-9fdd-451f79068a3e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 114,
            "second": 229
        },
        {
            "id": "7b12b317-4d5d-4f0f-8419-342ee170f5db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 114,
            "second": 230
        },
        {
            "id": "5a77e2a7-9565-4c03-b90d-a673265fd827",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 114,
            "second": 261
        },
        {
            "id": "7bac63e8-d8ba-47ec-8c81-76e69a627442",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -18,
            "first": 114,
            "second": 8218
        },
        {
            "id": "4340c568-07f7-4ca4-b627-e51ad47d4f74",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -18,
            "first": 114,
            "second": 8222
        },
        {
            "id": "d858515c-afe5-487c-adb5-444a0534cbe3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 118,
            "second": 38
        },
        {
            "id": "0cc9b4f7-80e2-42a1-a141-7480a743b81c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -18,
            "first": 118,
            "second": 44
        },
        {
            "id": "dd9efdc5-4d53-449e-92cd-48341428cc45",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -18,
            "first": 118,
            "second": 46
        },
        {
            "id": "6e0e21c7-964c-4c44-82d2-c790ffb91fe9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 118,
            "second": 47
        },
        {
            "id": "ab229efc-58a9-46bd-baf7-bd4581931c54",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 118,
            "second": 65
        },
        {
            "id": "f6107f4f-3bce-4aaf-baab-229aee32b723",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 118,
            "second": 99
        },
        {
            "id": "5334fae9-768e-4f7e-8f17-3c6be38b57b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 118,
            "second": 100
        },
        {
            "id": "3943b283-23f6-462b-ae79-cbb46169679b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 118,
            "second": 101
        },
        {
            "id": "a3291c6a-4034-4742-994a-4b9a8bb0bec5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 118,
            "second": 111
        },
        {
            "id": "65ca485e-8f81-464f-ad50-328c226998e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 118,
            "second": 113
        },
        {
            "id": "afc4c394-80bb-4d6a-b3f3-07eec1b5a7c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 118,
            "second": 192
        },
        {
            "id": "1633a1d9-f301-4f0f-843c-fbec58257039",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 118,
            "second": 193
        },
        {
            "id": "66dd770d-3aae-4b6c-a82b-6c223bcce5c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 118,
            "second": 194
        },
        {
            "id": "203df2ed-f58d-4e05-9161-6949a9bc1126",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 118,
            "second": 195
        },
        {
            "id": "f012f35f-6ba4-45eb-a19a-9ee885b63dca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 118,
            "second": 196
        },
        {
            "id": "a3cc7c7c-ca54-4948-89d8-6bd7ae4b0c65",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 118,
            "second": 197
        },
        {
            "id": "921faafc-4547-480e-8aca-27927ca5918f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 118,
            "second": 198
        },
        {
            "id": "d6721e56-1b2f-460c-8ecf-8e70e3a643a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 118,
            "second": 231
        },
        {
            "id": "6f2aa290-0d5a-48aa-a85f-12b7bffb4a08",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 118,
            "second": 232
        },
        {
            "id": "e47179cc-4fd6-45f4-8be1-e237dd21cf80",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 118,
            "second": 233
        },
        {
            "id": "edb47146-44c0-4f30-9eaf-a7d56029de20",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 118,
            "second": 234
        },
        {
            "id": "a63bf91a-de8e-4437-8c16-33c278e1d04a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 118,
            "second": 235
        },
        {
            "id": "452407b4-5eea-45a6-8d48-32ece881e119",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 118,
            "second": 240
        },
        {
            "id": "013e63e7-c46a-4f94-9a0c-1b4cd12be3b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 118,
            "second": 242
        },
        {
            "id": "cb455e34-5fef-4b8c-9118-261744fad0cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 118,
            "second": 243
        },
        {
            "id": "e0a731fe-6c40-4a14-8097-8e4e59448d2b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 118,
            "second": 244
        },
        {
            "id": "e7e3b52d-38f6-4cc5-804b-6f6759495766",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 118,
            "second": 245
        },
        {
            "id": "5bed7d56-7cb1-42c1-bef2-15893f0b7bb4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 118,
            "second": 246
        },
        {
            "id": "f5cc9316-43e4-4fa1-b414-f9be4f7521b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 118,
            "second": 248
        },
        {
            "id": "eb109e05-9d04-465b-a72c-194390323e17",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 118,
            "second": 260
        },
        {
            "id": "6854ebe2-7102-4e82-b2a6-9306169bda76",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 118,
            "second": 263
        },
        {
            "id": "062505cc-7de3-4cb2-aef1-6616cf78a908",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 118,
            "second": 281
        },
        {
            "id": "ce0660fb-8e1c-448a-a3a8-fcedd7f2413d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 118,
            "second": 339
        },
        {
            "id": "88bd80a4-fefa-4658-adaf-416785185568",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -18,
            "first": 118,
            "second": 8218
        },
        {
            "id": "bbcb6f77-da79-4249-947b-3152d101b45d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -18,
            "first": 118,
            "second": 8222
        },
        {
            "id": "0a13cc70-058b-4966-ba18-7c50c40ee27b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 118,
            "second": 8710
        },
        {
            "id": "eb16681d-f614-4f12-bd0e-bff17c722f91",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 119,
            "second": 44
        },
        {
            "id": "64f81018-6760-4f4a-8a88-28d0f6ddda0d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 119,
            "second": 46
        },
        {
            "id": "11cbdce2-9a04-41a0-b446-74ee657aba4b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 119,
            "second": 8218
        },
        {
            "id": "3d2247e2-c926-4e16-bc4b-e2d82049bf4d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 119,
            "second": 8222
        },
        {
            "id": "0f44ff1f-22fe-4282-a699-7c3d53750a39",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 120,
            "second": 99
        },
        {
            "id": "51353bb2-4f7e-4784-9467-82525f8a31c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 120,
            "second": 100
        },
        {
            "id": "12b60255-8370-4672-9a41-a8251ef92cc1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 120,
            "second": 101
        },
        {
            "id": "019ba47c-b74f-49cd-80d9-bd5d0a199e35",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 120,
            "second": 111
        },
        {
            "id": "b92b1a90-4125-48f5-a23d-53496b5a70cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 120,
            "second": 113
        },
        {
            "id": "3f41f833-5664-41ed-bd7e-f82bdcb99a39",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 120,
            "second": 231
        },
        {
            "id": "fcea005d-06aa-483e-9841-91aa43426f45",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 120,
            "second": 232
        },
        {
            "id": "094134a7-6ec9-40e4-bc80-c808ff4d926a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 120,
            "second": 233
        },
        {
            "id": "20cbe5e9-d755-417c-9ab6-d2ac2b7fb988",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 120,
            "second": 234
        },
        {
            "id": "b649584a-0bdc-4681-83c2-49c9755c5a6d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 120,
            "second": 235
        },
        {
            "id": "380176d9-0680-4bed-a7ed-f9d69802963b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 120,
            "second": 240
        },
        {
            "id": "3e51896b-85cc-4beb-92e3-e592f9dd4b10",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 120,
            "second": 242
        },
        {
            "id": "5edba214-17f7-4512-850e-53ff6f7c3f52",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 120,
            "second": 243
        },
        {
            "id": "a937bd6f-1dbc-4a73-9b2d-c4e10a7fcf4d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 120,
            "second": 244
        },
        {
            "id": "5f597411-00e1-48f6-9d4e-9a7527c916d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 120,
            "second": 245
        },
        {
            "id": "e46b73c7-7ead-4fa7-8a16-9cb892248378",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 120,
            "second": 246
        },
        {
            "id": "df046e25-d21b-4bd2-b988-907e4dfee639",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 120,
            "second": 248
        },
        {
            "id": "d7de79ed-a469-47e4-8b9a-025b817162bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 120,
            "second": 263
        },
        {
            "id": "e20c1e28-efef-4401-b82e-327977105a9d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 120,
            "second": 281
        },
        {
            "id": "fa651999-1954-4b5b-aa7d-9d18591e2ea2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 120,
            "second": 339
        },
        {
            "id": "a7c8a1ec-b076-4f63-8d23-0dc81c846fc8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 121,
            "second": 38
        },
        {
            "id": "c71f278a-ac40-41a5-aa8a-a2392c9f3547",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -18,
            "first": 121,
            "second": 44
        },
        {
            "id": "8a9f45c3-4534-4fcb-af48-a8a7c44353f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -18,
            "first": 121,
            "second": 46
        },
        {
            "id": "56a6093e-fdeb-4ba2-82ca-3157706fc5a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 121,
            "second": 47
        },
        {
            "id": "5fda20fd-9599-4a2b-a41e-37db0bcc3447",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 121,
            "second": 65
        },
        {
            "id": "1ee97dbb-451d-4ee5-acb0-abc8409ea2fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 121,
            "second": 99
        },
        {
            "id": "c7d336c7-377f-4002-8585-53827ef3e960",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 121,
            "second": 100
        },
        {
            "id": "5da4fb35-97d8-4bfb-a98c-3dac37f663e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 121,
            "second": 101
        },
        {
            "id": "b6d2d90d-a898-4e42-8851-d50d85d53263",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 121,
            "second": 111
        },
        {
            "id": "77c68553-3c28-4073-89b0-4d83cfc9d453",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 121,
            "second": 113
        },
        {
            "id": "6c3df97e-e62d-4955-ab5f-a974f12765b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 121,
            "second": 192
        },
        {
            "id": "14622d69-b1e6-4e4a-b1bd-f86ef7b7d19f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 121,
            "second": 193
        },
        {
            "id": "98e100b8-228d-4e7b-82ea-713cd3693218",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 121,
            "second": 194
        },
        {
            "id": "86d1ac2e-2b86-4352-9ae8-e2112d2c27c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 121,
            "second": 195
        },
        {
            "id": "e2a2588c-82fa-4ea2-8d2d-0d0e47020cc2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 121,
            "second": 196
        },
        {
            "id": "6ba29d53-81ed-4273-ae77-436bf50c7782",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 121,
            "second": 197
        },
        {
            "id": "444fa6e0-76b7-42df-be31-3aa43a60865f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 121,
            "second": 198
        },
        {
            "id": "8d2c789e-4f2b-4a42-873a-b98d71c79603",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 121,
            "second": 231
        },
        {
            "id": "d6dc2a95-0651-4a16-ba58-d653e657e285",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 121,
            "second": 232
        },
        {
            "id": "4b18f134-6931-43c9-95c0-b8e90bf22d16",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 121,
            "second": 233
        },
        {
            "id": "31568156-2a70-4c06-a6a6-8d0cdca50e94",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 121,
            "second": 234
        },
        {
            "id": "0fa388cb-4b72-4b93-b760-f8f867da84fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 121,
            "second": 235
        },
        {
            "id": "ef1c2d88-6834-45b8-b81b-65199c465905",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 121,
            "second": 240
        },
        {
            "id": "9282a123-b394-4bc4-9c10-c6f5d1cd12a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 121,
            "second": 242
        },
        {
            "id": "cd97abf3-0ea5-49fb-a6ec-e3a2497faf59",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 121,
            "second": 243
        },
        {
            "id": "ecbf478e-11af-4733-9750-adeb464cf68e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 121,
            "second": 244
        },
        {
            "id": "c0701888-e9fb-427c-9898-6e97c0539800",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 121,
            "second": 245
        },
        {
            "id": "2b4e7d54-7028-4d63-b9bb-f01aa4a69e73",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 121,
            "second": 246
        },
        {
            "id": "2833e5f7-9701-45d5-9c71-1824109f9f6e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 121,
            "second": 248
        },
        {
            "id": "df2c271b-2ec2-4bfe-a827-995ac776f317",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 121,
            "second": 260
        },
        {
            "id": "8d0712c5-b569-4fc7-91df-35d8ec9ecfce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 121,
            "second": 263
        },
        {
            "id": "9754b53c-b322-4cdf-aa89-ef9ea8ffb970",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 121,
            "second": 281
        },
        {
            "id": "03ee2759-038d-4bf8-88e0-151ea0ec859b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 121,
            "second": 339
        },
        {
            "id": "92b2a4bb-306e-488f-8cb0-197176023e08",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -18,
            "first": 121,
            "second": 8218
        },
        {
            "id": "cf2cf561-4d34-48cb-86d0-fcc245545f21",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -18,
            "first": 121,
            "second": 8222
        },
        {
            "id": "ac94ba47-320c-4cc1-b68c-ea9bf82bd25c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 121,
            "second": 8710
        },
        {
            "id": "be348853-d634-43fd-83b6-0c7496241e08",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 123,
            "second": 64
        },
        {
            "id": "b5183715-6b49-4b9d-9cf3-6be48005e2d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 123,
            "second": 67
        },
        {
            "id": "cb3bf6fb-3d24-46f6-b5a2-b468ff18cd12",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 123,
            "second": 71
        },
        {
            "id": "28015e75-53a3-46f3-8b8a-8937744c9bee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 123,
            "second": 79
        },
        {
            "id": "1513b011-b180-46bb-ad79-cf87d6b62fb3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 123,
            "second": 81
        },
        {
            "id": "d4d53702-f5bc-4d37-8484-e32a00903909",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 123,
            "second": 99
        },
        {
            "id": "e72f783e-cb6a-4afb-9453-c12aaea559de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 123,
            "second": 100
        },
        {
            "id": "9dcd0c00-d850-4787-8e58-9846eb182cc8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 123,
            "second": 101
        },
        {
            "id": "618800c8-2a9c-4889-997e-2da53926a782",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 123,
            "second": 111
        },
        {
            "id": "10f00017-0104-4c2a-8bf8-4bc736c8198e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 123,
            "second": 113
        },
        {
            "id": "3daddb73-acf5-45b0-83bf-94686ceb71b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 123,
            "second": 199
        },
        {
            "id": "f036b32f-73f8-4afc-b5fb-41830a2fc602",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 123,
            "second": 210
        },
        {
            "id": "c40fc942-a979-446c-9b9b-c5c425aca2f4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 123,
            "second": 211
        },
        {
            "id": "fe42a465-20f6-402d-b207-ba328a37f6e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 123,
            "second": 212
        },
        {
            "id": "5e3f98eb-19d7-435b-af1d-5756294c8579",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 123,
            "second": 213
        },
        {
            "id": "ff94bff1-c323-48ec-a99a-e5128ae2e418",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 123,
            "second": 214
        },
        {
            "id": "8a695e21-1dc4-490c-a633-ced774e52e65",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 123,
            "second": 216
        },
        {
            "id": "24155e8b-fb74-4d46-8021-1edd1d832f7c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 123,
            "second": 231
        },
        {
            "id": "63c50994-a768-4c2a-aa85-b3d32679dd29",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 123,
            "second": 232
        },
        {
            "id": "31ea04b6-936c-4fc1-a27f-b30d035c3f97",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 123,
            "second": 233
        },
        {
            "id": "e0f56bab-1d74-4741-aa10-390e6e8f86a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 123,
            "second": 234
        },
        {
            "id": "1fb0babf-de63-48fd-a322-251d3038f65d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 123,
            "second": 235
        },
        {
            "id": "bedefa42-afa9-4686-b574-f4ac93278a2a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 123,
            "second": 240
        },
        {
            "id": "81dd9d1c-d557-43d6-9d06-5c5fec012989",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 123,
            "second": 242
        },
        {
            "id": "ee71f1d6-e1ff-4691-8ad1-7efa58238648",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 123,
            "second": 243
        },
        {
            "id": "8c2900a1-4929-4d19-a1ee-8efffca598ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 123,
            "second": 244
        },
        {
            "id": "ef15f736-f018-4c33-bfc0-cdae6037afca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 123,
            "second": 245
        },
        {
            "id": "056287ff-6368-468f-bfa5-848cdc23aa29",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 123,
            "second": 246
        },
        {
            "id": "354c8291-69aa-41f4-a831-19954a3a285f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 123,
            "second": 248
        },
        {
            "id": "68539a5e-e8b3-44c9-b37d-8432104e141c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 123,
            "second": 262
        },
        {
            "id": "5be94e8e-5d78-485e-aaa1-3a74da319e8e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 123,
            "second": 263
        },
        {
            "id": "fea5757c-b251-4e2d-aaa2-7ce291175f6f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 123,
            "second": 281
        },
        {
            "id": "f701c55a-05e4-4210-8030-14acf621e4a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 123,
            "second": 338
        },
        {
            "id": "962b70d2-4df6-455c-be1d-65a6ac2484e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 123,
            "second": 339
        }
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 200,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}