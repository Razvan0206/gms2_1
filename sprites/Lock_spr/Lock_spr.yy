{
    "id": "1c246292-4f4f-4987-829c-d454a3fa907b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Lock_spr",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 315,
    "bbox_left": -41,
    "bbox_right": 241,
    "bbox_top": 13,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bf3c7557-1d18-4f8e-a6e0-887a76052065",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1c246292-4f4f-4987-829c-d454a3fa907b",
            "compositeImage": {
                "id": "49e1f1b4-ad68-483a-8790-b3e0d44b7695",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bf3c7557-1d18-4f8e-a6e0-887a76052065",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a1dcedd5-4a85-4302-94ca-f58794f29b9c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bf3c7557-1d18-4f8e-a6e0-887a76052065",
                    "LayerId": "83d5c20c-9c0a-4e5f-ac35-30e230815ad0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 294,
    "layers": [
        {
            "id": "83d5c20c-9c0a-4e5f-ac35-30e230815ad0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1c246292-4f4f-4987-829c-d454a3fa907b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 85,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 200,
    "xorig": 511,
    "yorig": 396
}