{
    "id": "f7900f3f-50c3-42ed-9cb1-fe23541f98bd",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "Restart2_b_obj1",
    "eventList": [
        {
            "id": "9f4514e4-69ca-4eb8-a6fb-bed1150476c7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 6,
            "m_owner": "f7900f3f-50c3-42ed-9cb1-fe23541f98bd"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "2fba6c38-8d43-48bf-afc1-f1e9bbc31b73",
    "visible": true
}