{
    "id": "9183f7de-bde6-4af3-a56a-dcae1177bf48",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "StarS_obj",
    "eventList": [
        {
            "id": "4e0103cc-be2c-4ed0-9cb4-9ac471063851",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "f445a2af-c3b6-4bb1-b37b-bd21f1ccd76a",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "9183f7de-bde6-4af3-a56a-dcae1177bf48"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "f7322c9c-7c5e-43fa-8049-ba50f0f4a5f5",
    "visible": true
}