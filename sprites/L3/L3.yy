{
    "id": "d8417d9d-9e39-46b9-8313-91378e3abade",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "L3",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 60,
    "bbox_left": 9,
    "bbox_right": 55,
    "bbox_top": 10,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "29b506e8-4d8d-488d-babd-e85133f5ab87",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d8417d9d-9e39-46b9-8313-91378e3abade",
            "compositeImage": {
                "id": "e6ef874c-10ac-4f5c-bf42-0370ee222078",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "29b506e8-4d8d-488d-babd-e85133f5ab87",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e139f900-ba19-41cc-950c-88e894178e76",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "29b506e8-4d8d-488d-babd-e85133f5ab87",
                    "LayerId": "df028e2b-c92c-4920-9b5f-3d18f50f557b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "df028e2b-c92c-4920-9b5f-3d18f50f557b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d8417d9d-9e39-46b9-8313-91378e3abade",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}