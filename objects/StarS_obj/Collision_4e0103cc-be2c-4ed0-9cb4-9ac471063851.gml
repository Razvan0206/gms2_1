/// @DnDAction : YoYo Games.Common.Variable
/// @DnDVersion : 1
/// @DnDHash : 0668D032
/// @DnDArgument : "expr" "-10"
/// @DnDArgument : "expr_relative" "1"
/// @DnDArgument : "var" "global.playerscore"
global.playerscore += -10;

/// @DnDAction : YoYo Games.Instances.Destroy_Instance
/// @DnDVersion : 1
/// @DnDHash : 13ED206C
instance_destroy();

/// @DnDAction : YoYo Games.Common.If_Variable
/// @DnDVersion : 1
/// @DnDHash : 114A9DBE
/// @DnDArgument : "var" "global.playerscore"
/// @DnDArgument : "op" "1"
if(global.playerscore < 0)
{
	/// @DnDAction : YoYo Games.Rooms.Go_To_Room
	/// @DnDVersion : 1
	/// @DnDHash : 348F83AC
	/// @DnDParent : 114A9DBE
	/// @DnDArgument : "room" "Restart"
	/// @DnDSaveInfo : "room" "1913c30a-09cf-4afb-aefd-c31d5629e005"
	room_goto(Restart);
}