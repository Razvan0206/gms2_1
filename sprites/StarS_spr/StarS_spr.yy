{
    "id": "f7322c9c-7c5e-43fa-8049-ba50f0f4a5f5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "StarS_spr",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 164,
    "bbox_left": 0,
    "bbox_right": 164,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cbee3605-ecf7-4aef-a040-db7ab60aa47b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f7322c9c-7c5e-43fa-8049-ba50f0f4a5f5",
            "compositeImage": {
                "id": "02100565-874c-466e-8edb-ee94448465fc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cbee3605-ecf7-4aef-a040-db7ab60aa47b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "57b30045-730a-47c8-9ef9-2d99d4ee4b48",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cbee3605-ecf7-4aef-a040-db7ab60aa47b",
                    "LayerId": "92b660aa-84c0-46a6-99ff-16206066909a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 168,
    "layers": [
        {
            "id": "92b660aa-84c0-46a6-99ff-16206066909a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f7322c9c-7c5e-43fa-8049-ba50f0f4a5f5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 168,
    "xorig": 0,
    "yorig": 0
}